# -----------------------------------------------------------------------------
#  File        : dighelpers.py
#  Brief       : Helper functions for digitial low-level data
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2023-09-27
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import math

def float_to_fixed(number, bits_int, bits_frac, signed=True):
    """
    Converts a floating point number fixed-point number in Q notation.

    An integer is used to represent the result. The lowest bits_int+bits_frac
    bits are used to represent the fixed-point number.

    Args:
        number (float): Floating-point number to be converted.
        bits_int (int): Number of bits used for the integer part of the Q value.
        bits_frac (int): Number of bits used for the fractional part of the
            Q value.
        signed : Selector if fixed point bumber is signed.

    Returns:
        int: Converted number in fixed-point Q notation.
    """

    num_int = int(math.floor(number*(2**bits_frac)))
    if signed and (num_int < 0):
        neg_correct = 2**(bits_int+bits_frac)
        num_int += neg_correct
    return num_int


def fixed_to_float(number, bits_int, bits_frac, signed=True):
    """
    Converts fixed-point number in Q notation to a floating point number.

    The input fixed-point number is represent by an integer. Only the lowest
    bits_int+bits_frac bits are used for the conversion.

    Args:
        number (int): Fixed-point number in Q format to be converted.
        bits_int (int): Number of bits used for the integer part of the Q value.
        bits_frac (int): Number of bits used for the fractional part of the
            Q value.
        signed : Selector if fixed point bumber is signed.

    Returns:
        int: Converted number in fixed-point Q notation.
    """

    if signed and (number >= 2**(bits_int+bits_frac-1)):
        number -= 2**(bits_int+bits_frac)
    number = number / (2**bits_frac)
    return number


def quantize(number, bits_int, bits_frac=0, signed=True):
    quantized_fixed = float_to_fixed(number, bits_int, bits_frac, signed)
    quantized_float = fixed_to_float(quantized_fixed, bits_int, bits_frac, signed)
    return quantized_float


def bytearray_to_hexstr(array, width=1, byteorder='little'):
    """
    Converts a bytearray into a string of hex values.

    For that the bytearray is split into elements of width bytes. These are then
    converted into hex string and returned as a string seperated by spaces.

    Args:
        array (bytearray):
        width (int): Number of bytes one element in the array consists of.
        byteorder ('little'/'big'): Endianess of elements in the array.

    Returns:
        str: Printable representation of the bytearray in hex.
    """

    hex_string = ""
    for i in range(len(array)//width):
        j = i * width
        part = array[j:j+width]
        if byteorder == "little":
            part = part[::-1]
        part_string = "".join("{:02X}".format(x) for x in part)
        hex_string += "0x{} ".format(part_string)
    hex_string = hex_string[:-1]
    return hex_string


def mask_bits(width: int) -> int:
    return int(2**width-1)


def extract_part(value: int, offset: int, width: int) -> int:
    part_mask = mask_bits(width) << offset
    return (value & part_mask) >> offset
