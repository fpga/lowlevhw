#------------------------------------------------------------------------------
#  File        : register_bank.py
#  Brief       : Class representing a register bank of a hardware device.
#------------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2018-03-13
#------------------------------------------------------------------------------
#  Description :
#------------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
#------------------------------------------------------------------------------

import math
import string
import typing

from abc import ABCMeta, abstractmethod

import lowlevhw


REGISTER_NAME_LENGTH = 24
FIELD_NAME_WIDTH = REGISTER_NAME_LENGTH
FULL_RESET_WIDTH = (32-1)/4 + 1 + 4

DEFAULT_RESET_VALUE = 0

MODE_NAMES = { "R" : "READ_LOGIC",
               "B" : "READ_BACK_REGISTER",
               "S" : "SET",
               "C" : "CLEAR",
               "W" : "WRITE",
               "T" : "STRB",
               "X" : "EXLCUDE_TESTING"
             }
FIELD_MODES       = set(["R", "B", "W", "S", "C", "X", "T"])
READABLE_MODES    = set(["R", "B"])
READ_LOGIC_MODES  = set(["R"])
WRITEABLE_MODES   = set(["W", "S", "C", "T"])
WRITE_LOGIC_MODES = set(["W", "S", "C", "T"])
DEFAULT_MODES = "BW"


def get_field_mode_name(abbreviation) -> str:
    return MODE_NAMES[abbreviation]


def get_mode_string(modes) -> str:

    return

#------------------------------------------------------------------------------
class Field:
    __metaclass__ = ABCMeta

    def __init__(self, name, offset, mode, description=""):
        self.name = name
        self.offset = offset
        self.width: int
        self.modes = set()
        self.set_modes(mode)
        self.reset_value: typing.Optional[int]
        self.description = description
        self.linked_fields = []

    def __str__(self) -> str:
        mode_list = sorted(self.modes)
        if mode_list:
            mode_string = ''.join(mode_list)
            mode_string = f'- {mode_string:<3}'
        else:
            mode_string = '     '
        reset_value_width = (self.width-1)//4 + 1
        if self.reset_value is not None:
            reset_value_string = f"(0x{self.reset_value:0{reset_value_width}X})"
        else:
            reset_value_string = ' ' * reset_value_width
        field_string = f"{self.name:<{FIELD_NAME_WIDTH}} : {self.width:2} bit(s) @ {self.offset:>2}           {reset_value_string:>{FULL_RESET_WIDTH}} {mode_string}"
        return field_string

    @abstractmethod
    def is_implementable(self) -> bool:
        pass

    @abstractmethod
    def is_filler(self) -> bool:
        pass

    def is_writable(self) -> bool:
        if self.modes & WRITEABLE_MODES:
            return True
        for field in self.linked_fields:
            if field.modes.isdisjoint(WRITEABLE_MODES):
                return True
        return False

    def is_readable(self) -> bool:
        if self.modes & READABLE_MODES:
            return True
        for field in self.linked_fields:
            if field.modes & READABLE_MODES:
                return True
        return False

    def set_modes(self, modes) -> None:
        if modes:
            for mode in modes:
                if set([mode]) & FIELD_MODES:
                    self.modes.update(mode)
                else:
                    print(f"Not supported register mode: {mode}")

    def get_write_modes(self):
        return self.modes & WRITEABLE_MODES

    def get_read_modes(self):
        return self.modes & READABLE_MODES

    def add_linked_field(self, field):
        self.linked_fields.append(field)

    def get_linked_fields(self):
        linked_fields = self.linked_fields
        all_linked_fields = list(linked_fields)
        for linked_field in linked_fields:
            all_linked_fields.extend(linked_field.get_linked_fields())
        return all_linked_fields


class FillerField(Field):

    def __init__(self, offset, width, name: str = '--FILLER--', description="") -> None:
        super().__init__(name, offset, "", description=description)
        self.width: int = width
        self.reset_value = None

    def is_implementable(self) -> bool:
        return False

    def is_filler(self) -> bool:
        return True


class HWField(Field):

    def __init__(self, name, offset, width, modes=DEFAULT_MODES, reset_value=0x0000, description=""):
        super().__init__(name, offset, modes, description=description)
        self.width: int = width
        self.reset_value = reset_value
        self.shadow_group = None

    @property
    def hw_field(self):
        return self

    @property
    def parent_field(self):
        return None

    def is_shadowed(self) -> bool:
        if self.shadow_group is None:
            return False
        return True

    def is_implementable(self) -> bool:
        return True

    def is_filler(self) -> bool:
        return False

    def is_writable(self) -> bool:
        if self.modes & WRITEABLE_MODES:
            return True
        return False

    def is_readable(self) -> bool:
        if self.modes & READABLE_MODES:
            return True
        return False

    def writes_logic(self) -> bool:
        if self.modes & WRITE_LOGIC_MODES:
            return True
        for field in self.linked_fields:
            if field.modes & WRITE_LOGIC_MODES:
                return True
        return False

    def reads_logic(self) -> bool:
        if self.modes & READ_LOGIC_MODES:
            return True
        for field in self.linked_fields:
            if field.modes & READ_LOGIC_MODES:
                return True
        return False


class VirtualField(Field):

    def __init__(self, name, offset, parent_field: Field, modes=DEFAULT_MODES):
        super().__init__(name, offset, modes)
        self.parent_field = parent_field
        parent_field.add_linked_field(self)

    def __str__(self) -> str:
        field_string = super().__str__()
        field_string += f"  --> {self.hw_field.name}"
        return field_string

    @property
    def width(self) -> int:
        return self.parent_field.width

    @property
    def reset_value(self) -> int:
        return self.hw_field.reset_value

    @property
    def hw_field(self) -> HWField:
        return self.parent_field.hw_field

    def is_implementable(self) -> bool:
        return False

    def is_filler(self) -> bool:
        return False

#-------------------------------------------------------------------------------
class Register:

    FREE_POSITION_CHAR = '-'
    FIELD_REPR_CHARS = list(string.ascii_uppercase) + list(string.ascii_lowercase)

    def __init__(self, name: str, width: int,
                 modes: str = DEFAULT_MODES,
                 reset_value: int = DEFAULT_RESET_VALUE,
                 description: str = "") -> None:
        self.name = name
        self.width = width
        self.description = description
        self.default_modes = modes
        default_field = HWField(self.name, 0, self.width, modes, reset_value)
        self.fields: typing.Dict[str, Field] = {self.name: default_field}

    def get_string(self, field_shift: int) -> str:
        reset_value_width = (self.width-1)//4 + 1
        output = f"{self.name:<{REGISTER_NAME_LENGTH}} : {self.graphic_string()} (0x{self.reset_value:0{reset_value_width}X})"
        if self.has_fields():
            for nr, field in enumerate(self.get_fields()):
                field_char = self.FIELD_REPR_CHARS[nr]
                shift_spaces = ' ' * field_shift
                output += f"\n{shift_spaces}{field_char} - {field}"
        else:
            field = list(self.fields.values())[0]
            modes_string = ''.join(sorted(field.modes))
            output += f" - {modes_string}"
            if not field.is_implementable():
                output += f"  --> {field.hw_field.name}"
        return output

    def __str__(self) -> str:
        return self.get_string(field_shift=4)

    def graphic_string(self) -> str:
        output = ''
        for bit in self.get_occupation():
            if bit is None:
                output += self.FREE_POSITION_CHAR
            else:
                output += self.FIELD_REPR_CHARS[bit]
        # Return MSB first
        return output[::-1]

    def __getitem__(self, field_name: str) -> Field:
        if field_name == '':
            return self
        try:
            return self.fields[field_name.lower()]
        except KeyError:
            raise KeyError(f"Field <{field_name}> not found in register <{self.name}>")

    def get_fields(self) -> typing.List[Field]:
        """Returns all fields ordered by their offset."""
        sorted_field_names = sorted(self.fields, key = lambda x: self.fields[x].offset)
        return [self.fields[field_name] for field_name in sorted_field_names]

    def get_fields_incl_reserved(self) -> typing.List[Field]:
        fields = self.get_fields()
        last_field_nr = -1
        output_fields = []
        reserved_offset = 0
        reserved_width = 0
        for bit_nr, field_nr in enumerate(self.get_occupation()):
            if field_nr is None:
                if reserved_width == 0:
                    reserved_offset = bit_nr
                reserved_width += 1
            elif field_nr > last_field_nr:
                if reserved_width > 0:
                    output_fields.append(FillerField(reserved_offset, reserved_width, f'__reserved_{reserved_offset}'))
                output_fields.append(fields[field_nr])
                last_field_nr = field_nr
                reserved_width = 0
        if reserved_width > 0:
            output_fields.append(FillerField(reserved_offset, reserved_width, f'__reserved_{reserved_offset}'))
        return output_fields

    def add_field(self, name: str, offset, width, modes=None, reset_value=DEFAULT_RESET_VALUE, description: str = "") -> HWField:
        if modes is None:
            modes = self.default_modes
        if not self.has_fields():
            del self.fields[self.name]
        # TODO: Check if space available
        field = HWField(name, offset, width, modes, reset_value, description)
        self.fields[name.lower()] = field
        return field

    def add_linked_field(self, name: str, existing_field, offset, modes=None) -> VirtualField:
        if modes is None:
            modes = self.default_modes
        if not self.has_fields():
            del self.fields[self.name]
        # TODO: Check if space available
        new_field = VirtualField(name, offset, existing_field, modes)
        self.fields[name] = new_field
        return new_field

    def link_to_register(self, dest_register, name_postfix=""):
        if dest_register.get_fields():
            modes = self.default_modes
            self.fields = {}
            for field in dest_register.get_fields():
                if name_postfix != '':
                    name_postfix = '_' + name_postfix
                linking_field_name = field.name + name_postfix
                self.add_linked_field(linking_field_name, field, field.offset, modes)

    def is_multiple_fields(self) -> bool:
        print("DEPRECATED: the function is_multiple_fields() has been renamed to has_fields()")
        return self.has_fields()

    def has_fields(self) -> bool:
        more_than_one_field = len(self.fields) > 1
        field_not_equal_register = list(self.fields.keys()) != [self.name]
        return more_than_one_field or field_not_equal_register

    def get_occupation(self) -> typing.List[typing.Optional[int]]:
        occupation: typing.List[typing.Optional[int]] = [None] * self.width
        field_nr = 0
        for field in self.get_fields():
            offset = field.offset
            width = field.width
            occupation[offset:offset+width] = [field_nr] * width
            field_nr += 1
        return occupation

    def rename(self, new_name: str) -> None:
        if self.name in self.fields:
            self.fields[new_name] = self.fields.pop(self.name)
        self.name = new_name

    def __set_reset_value(self, reset_value) -> None:
        for field in self.get_fields():
            field_reset_value = reset_value >> field.offset
            field_reset_value = field_reset_value & ((2**field.width)-1)
            field.reset_value = field_reset_value

    def __get_reset_value(self) -> int:
        reset_value = 0
        for field in self.get_fields():
            field_reset_value = field.reset_value << field.offset
            reset_value = reset_value | field_reset_value
        return reset_value

    reset_value = property(__get_reset_value, __set_reset_value)

    def __set_modes(self, modes) -> None:
        for field in self.get_fields():
            field.set_modes(modes)

    def __get_modes(self):
        modes = set()
        for field in self.get_fields():
            modes.update(field.modes)
        return modes

    modes = property(__get_modes, __set_modes)

    def is_readable(self) -> bool:
        if self.modes & READABLE_MODES:
            return True
        return False

    def is_writable(self) -> bool:
        if self.modes & WRITEABLE_MODES:
            return True
        return False

#-------------------------------------------------------------------------------
class ShadowGroup:

    def __init__(self, name: str) -> None:
        self.name = name
        self.hw_fields = []

    def __str__(self) -> str:
        output = ""
        output += f"{self.name}: "
        for field in self.hw_fields:
            output += f"{field.name}, "
        output += '\n'
        return output

    def add_element(self, element) -> None:
        if hasattr(element, 'get_fields'):
            fields = element.get_fields()
        else:
            fields = [element]
        for field in fields:
            hw_field = field.hw_field
            self.hw_fields.append(hw_field)
            hw_field.shadow_group = self


#------------------------------------------------------------------------------
class RegisterBank:

    def __init__(self, name: str, address_width: int, data_width: int, element_addressing: bool = False, error_reporting: bool = False) -> None:
        self.name = name
        self._base_address = 0
        self.address_width = address_width
        self.data_width: int = data_width
        self.wstrb_width = math.ceil(data_width / 8)
        self.element_addressing = element_addressing
        self.enable_error_decoding = error_reporting
        self.register_map = {}
        self.shadow_groups = []

    @property
    def base_address(self) -> int:
        print("DEPRECATED: .base_address of RegisterBank should not be used anymore")
        return self._base_address

    @base_address.setter
    def base_address(self, value: int) -> None:
        print("DEPRECATED: .base_address of RegisterBank should not be used anymore")
        self._base_address = value

    def get_string(self) -> str:
        return lowlevhw.generators.RegbankTextDocGen.generate_string(self)

    def __str__(self) -> str:
        return self.get_string()

    def __getitem__(self, key: typing.Union[str, int]):
        if isinstance(key, str):
            if key == '':
                return self
            (location, sub_location) = lowlevhw.split_element_location(key)
            register = self.get_register_by_name(location)
            if register is None:
                raise KeyError(f"Register <{key}> is not in register bank <{self.name}>")
            if sub_location != '':
                return register[sub_location]
            return register
        if isinstance(key, int):
            return self.get_register_at_address(key)
        raise KeyError("RegisterBank: wrong type of access key")

    def get_element(self, name):
        register_name, field_name = lowlevhw.split_element_location(name)
        register = self.get_register_by_name(register_name)
        if not field_name:
            return register
        return register[field_name]

    def get_path(self, location: str) -> typing.List[str]:
        _ = self[location]
        return [location]

    def get_register_by_name(self, name) -> typing.Optional[Register]:
        for _, register in self.register_map.items():
            if register.name.lower() == name.lower():
                return register
        return None

    def get_register_at_address(self, address):
        for reg_address, register in self.register_map.items():
            if (self._base_address + reg_address) == address:
                return register
        return None

    def __address_new_register(self, address: typing.Optional[int]) -> int:
        if address in self.register_map:
            raise Exception(f"Register at address 0x{address:0>3X} exists already.")
        if address is None:
            if len(self.register_map) == 0:
                address = 0
            else:
                highest_address = max(k for k in self.register_map)
                if self.element_addressing:
                    address = highest_address + 1
                else:
                    address_diff = math.ceil(self.data_width/8)
                    address = highest_address + address_diff
        # TODO: In case of Byte addressing don't allow generation of mis-aligned registers
        return address

    def add_register(self, name: str, address=None, modes=DEFAULT_MODES, reset_value=DEFAULT_RESET_VALUE, description="") -> Register:
        try:
            new_address = self.__address_new_register(address)
        except Exception as e:
            raise type(e)(str(e) + f" - register <{name}> ignored.")
        if self.get_register_by_name(name) is None:
            new_register = Register(name, self.data_width, modes, reset_value, description)
            self.register_map[new_address] = new_register
            return new_register
        raise Exception(f"Register with name <{name}> already exists - ignored.")

    def add_existing_register(self, register_definition: Register, address: typing.Optional[int] = None) -> Register:
        try:
            new_address = self.__address_new_register(address)
        except Exception as e:
            raise type(e)(str(e) + f" - register <{register_definition.name}> ignored.")
        self.register_map[new_address] = register_definition
        return register_definition

    def append_register_bank(self, regbank, offset=0x0, prefix=""):
        new_registers = regbank.get_registers()
        for reg in new_registers:
            address = regbank.get_address(reg.name)
            new_name = prefix + reg.name
            if (address + offset) in self.register_map:
                raise Exception(f"Register already exists at 0x{address:X} in register bank {self.name}.")
            elif self.get_register_by_name(new_name) is not None:
                raise Exception(f"Register named <{new_name}> already exists in register bank {self.name}.")
            else:
                reg.rename(new_name)
                self.register_map[address + offset] = reg

    def get_registers(self) -> typing.List[Register]:
        addresses = self.get_address_list()
        sorted_registers = [self.register_map[address] for address in addresses]
        return sorted_registers

    def get_fields(self) -> typing.List[Field]:
        all_fields = [field
                      for register in self.get_registers()
                      for field in register.get_fields()]
        return all_fields

    def get_unique_hw_fields(self) -> typing.List[Field]:
        unique_hw_fields = []
        for field in self.get_fields():
            if field.is_implementable() and (field not in unique_hw_fields):
                unique_hw_fields.append(field)
        return unique_hw_fields

    def get_register_of_field(self, field):
        for register in self.get_registers():
            if field in register.get_fields():
                return register
        return None

    def get_address_list(self) -> typing.List[int]:
        addresses = list(self.register_map.keys())
        addresses.sort()
        return addresses

    def get_address(self, register_name: str) -> int:
        register_name, _ = lowlevhw.split_element_location(register_name)
        for (address, register) in self.register_map.items():
            if register.name.lower() == register_name.lower():
                return self._base_address + address
        raise KeyError(f"Register <{register_name}> not found in register bank <{self.name}>")

    def add_shadow_group(self, name) -> ShadowGroup:
        new_group = ShadowGroup(name)
        self.shadow_groups.append(new_group)
        return new_group

    def check_validity(self) -> bool:
        print(f"Check validity of register bank: {self.name}")
        print("--------------------------------------------------------")
        valid = True

        # # Check for multiple fields with the same name
        # field_names = set()
        # for field in self.get_fields():
        #     if field.is_implementable():
        #         if field.name in field_names:
        #             print("ERROR: Field with name {} exists multiple times.".format(field.name))
        #             valid = False
        #         else:
        #             field_names.add(field.name)

        if valid:
            print(f"INFO: Register bank {self.name} satisfies rules.")

        return valid
