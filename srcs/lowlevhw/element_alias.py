# -----------------------------------------------------------------------------
#  File        : element_alias.py
#  Brief       : Class representing a hardware device.
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2019-01-25
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2020 European Spallation Source ERIC
# -----------------------------------------------------------------------------


class ElementAlias:

    def __init__(self, name, element_name, organization=None):
        self.name = name
        self.element_name = element_name
        self.organization = organization
