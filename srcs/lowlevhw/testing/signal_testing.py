# -----------------------------------------------------------------------------
#  File        : test_helpers.py
#  Brief       :
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2020-07-01
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2020 - 2021 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import pytest

# import matplotlib.pyplot as mplot
import numpy as np
import scipy.fftpack


def assert_waveform_equal(signal_measured, signal_expected, offset=10, shifttol=0):
    offset = min(offset, len(signal_measured)-1)
    shift = offset+1

    for i in range(-offset, 0):
        assert_data_measured = signal_measured[-i:]
        assert_data_expected = signal_expected[:i]
        if np.array_equal(assert_data_measured, assert_data_expected):
            if abs(i) < shift:
                shift = i

    for i in range(1, offset+1):
        assert_data_measured = signal_measured[:-i]
        assert_data_expected = signal_expected[i:]
        if np.array_equal(assert_data_measured, assert_data_expected):
            if abs(i) < abs(shift):
                shift = i


    # Check for no shift
    if np.array_equal(signal_measured, signal_expected):
        return

    # if no shift worked
    if shift == offset+1:
        np.testing.assert_array_equal(signal_measured, signal_expected)
    else:
        if abs(shift) <= shifttol:
            return
        else:
            raise AssertionError(f"The tested signal is shifted by {-shift} samples")


def assert_waveform_allclose(signal_measured, signal_expected, offset=10, rtol=1e-07, atol=0.0, shifttol=0):

    offset = min(offset, len(signal_measured)-1)
    shift = offset+1

    for i in range(-offset, 0):
        assert_data_measured = signal_measured[-i:]
        assert_data_expected = signal_expected[:i]
        if np.allclose(assert_data_measured, assert_data_expected, rtol, atol):
            if abs(i) < shift:
                shift = i

    for i in range(1, offset+1):
        assert_data_measured = signal_measured[:-i]
        assert_data_expected = signal_expected[i:]
        if np.allclose(assert_data_measured, assert_data_expected, rtol, atol):
            if abs(i) < abs(shift):
                shift = i

    # Check for no shift
    if np.allclose(signal_measured, signal_expected, rtol, atol):
        return

    # if no shift worked
    if shift == offset+1:
        np.testing.assert_allclose(signal_measured, signal_expected, rtol, atol)
    else:
        if abs(shift) <= shifttol:
            return
        else:
            raise AssertionError(f"The tested signal is shifted by {-shift} samples")


def assert_test_noise(signal):
    if len(signal) > 1:
        for i in range(len(signal)-1):
            if signal[0] != signal[i+1]:
                return
        raise AssertionError("No noise present in the tested signal, i.e. value is not changing")


def assert_frequency(signal, frequency, f_sampling, atol=0, rtol=1e-07):
    T = 1/f_sampling
    nr_samples = len(signal)
    xf = np.linspace(0.0, 1.0/(2.0*T), nr_samples//2)
    yf = scipy.fftpack.fft(signal)
    yf = yf[0:len(yf)//2]
    yf = np.abs(yf)

    # Debug
    # mplot.figure()
    # mplot.plot(xf[0:100], yf[0:100])
    # mplot.draw()
    # mplot.pause(0.001)
    # input("Press enter")

    max_f_peak = max(yf)
    frequency_pos = np.where(yf == max_f_peak)
    frequency_peak = xf[frequency_pos[0]]

    atol_min = 1.5*(f_sampling/len(signal))
    atol = max([atol, atol_min])
    assert frequency_peak[0] == pytest.approx(frequency, abs=atol, rel=rtol), f"Expected frequency {frequency}, but is {frequency_peak[0]}"


def digital_tolerance(tolerance_bits, resolution, max_value):
    return max_value * tolerance_bits*(1/2**resolution)


def get_error_blocks(error_pos, max_pos):
    block_extension = 3
    off_tolerance_blocks = []
    off_tolerance_pos_idx = 0
    while off_tolerance_pos_idx < len(error_pos)-1:
        block_start = max(0, error_pos[off_tolerance_pos_idx] - block_extension)
        while error_pos[off_tolerance_pos_idx+1] - error_pos[off_tolerance_pos_idx] == 1:
            off_tolerance_pos_idx += 1
            if off_tolerance_pos_idx == len(error_pos)-1:
                break
        block_end = min(max_pos, error_pos[off_tolerance_pos_idx] + block_extension)
        block = (block_start, block_end)
        off_tolerance_blocks.append(block)
        off_tolerance_pos_idx += 1
    return off_tolerance_blocks


def assert_waveform_digital(measured, exact, tolerance_bits=2, resolution=16, max_value=1.0):
    max_blocks_reported = 4
    diff = np.asarray(measured) - np.asarray(exact)
    diff_bits = diff / (max_value * 1/2**resolution)
    diff_bits = np.round(diff_bits, 3)
    tolerance = digital_tolerance(tolerance_bits, resolution, max_value)
    off_tolerance = np.greater(np.abs(diff), [tolerance] * len(diff))
    if np.any(off_tolerance):
        off_tolerance_pos = np.array(np.where(off_tolerance)[0])
        error_blocks = get_error_blocks(off_tolerance_pos, len(diff)-1)
        error_values = [measured[block[0]:block[1]] for block in error_blocks]
        exact_values = [exact[block[0]:block[1]] for block in error_blocks]
        bit_error = [diff_bits[block[0]:block[1]] for block in error_blocks]
        error_string =  f"Out of tolerance ({tolerance_bits} bits): maximum {np.max(np.abs(diff_bits))} bits\n"
        for block_id, values in enumerate(list(zip(error_values, exact_values, bit_error))):
            error_string += f"Error block {block_id} ({error_blocks[block_id][0]} --> {error_blocks[block_id][1]}):\n"
            error_string += f"  measured:     {np.asarray(values[0])}\n"
            error_string += f"  exact:        {np.asarray(values[1])}\n"
            error_string += f"  diff in bits: {np.asarray(values[2])}\n"
            if block_id > max_blocks_reported-2:
                more_error_blocks = len(error_values) - block_id - 1
                error_string += f"... {more_error_blocks} more block(s) with errors"
                break
        raise AssertionError(error_string)
