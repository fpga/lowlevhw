#-------------------------------------------------------------------------------
#  File        : device_state.py
#  Brief       : Class representing the state of a hardware device.
#-------------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2019-06-04
#-------------------------------------------------------------------------------
#  Description :
#-------------------------------------------------------------------------------
#  Copyright (C) 2018 - 2019 European Spallation Source ERIC
#-------------------------------------------------------------------------------

import lowlevhw.register_bank as register_bank

class DeviceState:
    ELEMENT_STATE_SEPARATOR = ":"

    def __init__(self, element_states, register_banks=None):
        self.element_states = element_states
        self.register_banks = register_banks

    @classmethod
    def fromfile(cls, filename, register_banks=None):
        element_states = {}
        with open(filename, "r") as f:
            for line in f:
                separator_idx = line.rindex(cls.ELEMENT_STATE_SEPARATOR)
                element_name = line[:separator_idx]
                value_string = line[separator_idx+1:]
                if register_banks == None:
                    state_register = register_bank.Register(element_name, 256)
                else:
                    for reg_bank in register_banks:
                        state_register = reg_bank[element_name]
                element_states[state_register] = int(value_string)
        return cls(element_states, register_banks)

    def __getitem__(self, element_name):
        for (element, state) in self.element_states.items():
            if element.name == element_name:
                return (element, state)
        return (element, None)

    def __str__(self):
        state_string = ""
        for (element, state) in self.element_states.items():
            state_string += element.name
            state_string += self.ELEMENT_STATE_SEPARATOR
            state_string += str(hex(state))
            state_string += "\n\r"
        return state_string

    def diff(self, compare_state):
        diff_string = ""
        for (element, state) in self.element_states.items():
            compare_element_state = compare_state[element.name][1]
            if state != compare_element_state:
                diff_string += "{} : {} | {}".format(element.name, hex(state), hex(compare_element_state))
                diff_string += "\n\r"
        return diff_string

    def store(self, filename):
        with open(filename, "w") as f:
            for (element, state) in self.element_states.items():
                element_name = element.name
                f.write("{}{}{}".format(element_name, self.ELEMENT_STATE_SEPARATOR, state))
                f.write("\r\n")
