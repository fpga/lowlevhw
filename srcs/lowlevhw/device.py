# -----------------------------------------------------------------------------
#  File        : device.py
#  Brief       : Class representing a hardware device.
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2019-01-25
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import time
import typing

from abc import ABCMeta, abstractmethod

import lowlevhw
import lowlevhw.register_bank as register_bank
import lowlevhw.memory_map as memory_map
import lowlevhw.device_state as device_state
from lowlevhw.helpers import mask_bits
from lowlevhw.helpers import extract_part


class DeviceRegElement:
    __metaclass__ = ABCMeta

    def __init__(self, element_definition, type_string) -> None:
        self.definition = element_definition
        self.type_string = type_string
        self.width = self.definition.width

    @property
    def raw(self) -> int:
        return self.read()

    @raw.setter
    def raw(self, value: int) -> None:
        self.write(value)

    #TODO: This will implement type conversion when registers/fields have a type property"
    @property
    def value(self):
        return self.read()

    #TODO: This will implement type conversion when registers/fields have a type property"
    @value.setter
    def value(self, value) -> None:
        self.write(value)

    @abstractmethod
    def read(self) -> int:
        pass

    def assert_value_range(self, value: int, width: int, element_string=None) -> None:
        if value >= 2**width:
            select_element_str = element_string if element_string else self.type_string
            raise ValueError(f"Value (0x{value:X}) too large to be written to {select_element_str} of width {self.width}")

    def assert_out_of_bound(self, begin_bit: int, end_bit: int) -> None:
        if begin_bit < 0:
            raise IndexError(f"Bit index cannot be negative.")
        if end_bit < begin_bit:
            raise IndexError(f"End bit of a bit range must be larger then the start bit")
        if end_bit > self.width-1:
            raise IndexError(f"Out-of-bound access to {self.type_string}")

    def read_part(self, begin_bit: int, end_bit: int) -> int:
        self.assert_out_of_bound(begin_bit, end_bit)
        part_width = end_bit - begin_bit + 1
        element_value = self.read()
        part_value = extract_part(element_value, begin_bit, part_width)
        return part_value

    def write(self, value: int) -> None:
        self.assert_value_range(value, self.width)
        self._hw_write(value)

    @abstractmethod
    def _hw_write(self, value: int) -> None:
        """ Writes a value to the register/field without any checks"""
        pass

    def write_part(self, value: int, begin_bit: int, end_bit: int) -> None:
        self.assert_out_of_bound(begin_bit, end_bit)
        part_width = end_bit - begin_bit + 1
        self.assert_value_range(value, part_width, "part")
        length_mask = mask_bits(part_width)
        set_mask = value << begin_bit
        value_inverted = value ^ length_mask
        mask_shifted = value_inverted << begin_bit
        clear_mask = mask_shifted ^ mask_bits(self.width)
        if self.definition.is_readable():
            register_value = self.read()
        else:
            register_value = 0
        register_with_set = register_value | set_mask
        new_value = register_with_set & clear_mask
        self._hw_write(new_value)

    def set(self) -> None:
        self._hw_write(mask_bits(self.width))

    def clear(self) -> None:
        self._hw_write(0)

    def toggle(self) -> None:
        old_value = self.read()
        new_value = ~old_value & mask_bits(self.width)
        self._hw_write(new_value)

    def write_check(self, test_value: int) -> bool:
        self._hw_write(test_value)
        result = self.read()
        return result == test_value

    def check_bit(self, position: int) -> bool:
        return bool(self.read_part(position, position))

    def write_bit(self, value: int, position: int) -> None:
        self.write_part(value, position, position)

    def set_bit(self, position: int) -> None:
        self.write_bit(1, position)

    def clear_bit(self, position: int) -> None:
        self.write_bit(0, position)

    def toggle_bit(self, position: int) -> None:
        toggled_value = int(not self.check_bit(position))
        self.write_part(toggled_value, position, position)


class DeviceField(DeviceRegElement):

    def __init__(self, device, register, field_definition) -> None:
        super().__init__(field_definition, "field")
        self.device = device
        self.register = register
        self.begin_bit = self.definition.offset
        self.end_bit = self.definition.offset + self.definition.width - 1

    def read(self) -> int:
        field_value = self.register.read_part(self.begin_bit, self.end_bit)
        return field_value

    # Temporary solution, proper handling of all functions for asynchronous designs is necessary
    async def aread(self) -> int:
        field_value = await self.register.aread_part(self.begin_bit, self.end_bit)
        return field_value

    def _hw_write(self, value: int) -> None:
        self.register.write_part(value, self.begin_bit, self.end_bit)

    # Temporary solution, proper handling of all functions for asynchronous designs is necessary
    async def awrite(self, value: int) -> None:
        await self.register.awrite_part(value, self.begin_bit, self.end_bit)


class DeviceRegister(DeviceRegElement):

    FULL_FIELD_NAME = '--FULL--'

    def __init__(self, device, address: int, register_definition: lowlevhw.Register) -> None:
        super().__init__(register_definition, "register")
        self.device = device
        self.address = address

    def __getitem__(self, location: str) -> DeviceField:
        field = self.definition[location]
        return DeviceField(self.device, self, field)

    def read(self) -> int:
        if not self.device.ready:
            raise IOError("Device is not ready")
        return self.device.driver.read_register(self.address)

    # Temporary solution, proper handling of all functions for asynchronous designs is necessary
    async def aread(self) -> int:
        if not self.device.ready:
            raise IOError("Device is not ready")
        return await self.device.driver.read_register_async(self.address)

    # Temporary solution, proper handling of all functions for asynchronous designs is necessary
    async def aread_part(self, begin_bit: int, end_bit: int) -> int:
        register_value = await self.aread()
        width = end_bit - begin_bit + 1
        part_value = extract_part(register_value, begin_bit, width)
        return part_value

    def read_all_fields(self) -> typing.Dict[str, int]:
        register_value = self.read()
        fields = self.definition.get_fields()
        field_values = dict()
        field_values[self.FULL_FIELD_NAME] = register_value
        for field in fields:
            field_value = extract_part(register_value, field.offset, field.width)
            field_values[field.name] = field_value
        return field_values

    # Temporary solution, proper handling of all functions for asynchronous designs is necessary
    async def aread_all_fields(self) -> typing.Dict[str, int]:
        register_value = await self.aread()
        fields = self.definition.get_fields()
        field_values = dict()
        field_values[self.FULL_FIELD_NAME] = register_value
        for field in fields:
            field_value = extract_part(register_value, field.offset, field.width)
            field_values[field.name] = field_value
        return field_values

    def _hw_write(self, value: int) -> None:
        if not self.device.ready:
            raise IOError("Device is not ready")
        self.device.driver.write_register(self.address, value)

    # Temporary solution, proper handling of all functions for asynchronous designs is necessary
    async def awrite(self, value: int) -> None:
        if not self.device.ready:
            raise IOError("Device is not ready")
        await self.device.driver.write_register_async(self.address, value)

    # Temporary solution, proper handling of all functions for asynchronous designs is necessary
    async def awrite_part(self, value: int, begin_bit: int, end_bit: int) -> None:
        set_mask = value << begin_bit
        part_width = end_bit - begin_bit + 1
        length_mask = mask_bits(part_width)
        mask_inv = value ^ length_mask
        mask_shifted = mask_inv << begin_bit
        clear_mask = mask_shifted ^ mask_bits(self.definition.width)
        if self.definition.is_readable():
            register_value = await self.aread()
        else:
            register_value = 0
        register_with_set = register_value | set_mask
        new_value = register_with_set & clear_mask
        await self.awrite(new_value)

class DeviceMemoryArea:

    def __init__(self, device, area_definition: lowlevhw.MemoryArea, offset_address: int) -> None:
        self.device = device
        self.definition = area_definition
        self.offset_address = offset_address

    def get_base_address(self) -> int:
        """ in Bytes """
        area_base = self.definition.base
        if self.definition.base_is_register:
            base_address = self.device[area_base].read()
        else:
            base_address = area_base
        base_address = base_address * self.definition.base_block_bytes
        # Support for Struck legacy memory block format
        if self.definition.legacy == "STRUCK":
            # area_size = self.definition.size
            read_length = self.get_size()
            base_address = base_address - read_length - self.definition.base_block_bytes
        base_address += self.offset_address
        return base_address

    def set_base_address(self, address: int, strict: bool=True) -> None:
        """relative to memory area, in Bytes """
        correction = 0
        if (address % self.definition.base_block_bytes) != 0:
            if strict:
                raise ValueError("Base address is not aligned with block size {} of memory area".format(
                    self.definition.base_block_bytes
                ))
            else:
                correction = 1
        base_address_blocks = address // self.definition.base_block_bytes + correction
        if self.definition.base_is_register:
            self.device[self.definition.base].write(base_address_blocks)
        else:
            self.definition.base = base_address_blocks

    def get_size(self) -> int:
        """ in Bytes """
        area_size = self.definition.size
        if self.definition.size_is_register:
            read_length = self.device[area_size].read()
        else:
            read_length = area_size
        read_length = read_length * self.definition.size_block_bytes
        return read_length

    def read(self, read_len=None):
        raw_data = self.read_raw(read_len)
        format_data = self.definition.organization.from_raw(raw_data)
        return format_data

    def read_raw(self, read_len=None):
        if not self.device.ready:
            raise IOError("Device is not ready")
        if read_len is None:
            read_len = self.get_size()
        else:
            read_len = read_len * self.definition.size_block_bytes
        mem_content = self.device.driver.read_ram(self.get_base_address(), read_len)
        return mem_content

    def write(self, data) -> None:
        raw_data = self.definition.organization.to_raw(data)
        # TODO: Check for writing out of area
        self.write_raw(raw_data)

    def write_raw(self, raw_data) -> None:
        if not self.device.ready:
            print("WARNING: Device is not ready.")
            return None
        if len(raw_data) > self.get_size():
            print("ERROR: Data longer than memory area")
            return
        self.device.driver.write_ram(self.get_base_address(), len(raw_data), raw_data)
        # update register with new size


class DeviceRegisterBank:

    def __init__(self, device, definition: lowlevhw.RegisterBank, base_address: int) -> None:
        self.device = device
        self.base_address: int = base_address
        self.definition: lowlevhw.RegisterBank = definition

    def __getitem__(self, location) -> typing.Union[DeviceRegister, DeviceField]:
        register_location, sub_location = lowlevhw.split_element_location(location)
        register_address = self.definition.get_address(register_location)
        address = self.base_address + register_address
        register = DeviceRegister(self.device, address, self.definition[register_location])
        if sub_location != '':
            return register[sub_location]
        return register


class DeviceMemoryMap:

    def __init__(self, device, definition: lowlevhw.MemoryMap, offset_address: int):
        self.device = device
        self.offset_address = offset_address
        self.definition = definition

    def __getitem__(self, location) -> DeviceMemoryArea:
        if isinstance(location, tuple):
            if len(location) == 2:
                base = location[0]
                size = location[1]
                temp_memory_area = lowlevhw.MemoryAreaFixed("____temp____", base, size)
                return DeviceMemoryArea(self.device, temp_memory_area, self.offset_address)
            else:
                raise KeyError("Tupple access only allowed for tuples of two integers")
        elif isinstance(location, int):
            temp_memory_area = lowlevhw.MemoryAreaFixed("____temp____", location, 1)
            return DeviceMemoryArea(self.device, temp_memory_area, self.offset_address)
        return DeviceMemoryArea(self.device, self.definition[location], self.offset_address)


class DeviceAddressSpace:

    def __init__(self, device, definition: lowlevhw.AddressSpace, base_address: int) -> None:
        self.device = device
        self.base_address: int = base_address
        self.definition: lowlevhw.AddressSpace = definition

    def __getitem__(self, location):
        local_location, sub_location = lowlevhw.split_element_location(location)
        space_element = self.definition[local_location]
        base_address = self.base_address + self.definition.get_address(local_location)
        if isinstance(space_element, lowlevhw.Register):
            device_element = DeviceRegister(self.device, base_address, space_element)
        elif isinstance(space_element, lowlevhw.RegisterBank):
            device_element = DeviceRegisterBank(self.device, space_element, base_address)
        elif isinstance(space_element, lowlevhw.MemoryMap):
            device_element = DeviceMemoryMap(self.device, space_element, base_address)
        elif isinstance(space_element, lowlevhw.AddressSpace):
            device_element = DeviceAddressSpace(self.device, space_element, base_address)
        else:
            raise TypeError(f"Unsupported type {type(space_element)} returned when accessing device elements")
        if sub_location == '':
            return device_element
        return device_element[sub_location]


class DeviceElementAlias:

    def __init__(self, device_element, definition) -> None:
        self.definition = definition
        self.device_element = device_element

    def read(self, read_len=None):
        if self.definition.organization is None:
            data = self.device_element.read(read_len)
        else:
            raw_data = self.device_element.read_raw(read_len)
            data = self.definition.organization.from_raw(raw_data)
        return data

    def read_raw(self, read_len=None):
        return self.device_element.read_raw(read_len)

    def write(self, data) -> None:
        if self.definition.organization is None:
            self.device_element.write(data)
        else:
            raw_data = self.definition.organization.to_raw(data)
            self.device_element.write_raw(raw_data)

    def write_raw(self, raw_data) -> None:
        self.device_element.write_raw(raw_data)


class Device:

    def __init__(self, device_definition: lowlevhw.DeviceDefinition, driver: lowlevhw.DeviceDriver) -> None:
        self.definition: lowlevhw.DeviceDefinition = device_definition
        self.driver: lowlevhw.DeviceDriver = driver
        self.ready: bool = False

    def connect(self, dev_node) -> None:
        if self.ready:
            print("INFO: Device already connected.")
            return
        self.driver.open_device(dev_node)
        self.ready = True

    def disconnect(self) -> None:
        self.ready = False
        self.driver.close_device()

    def get_alias(self, location: str):
        return self.definition.get_alias(location)

    def __getitem__(self, location) -> typing.Any:
        if isinstance(location, tuple):
            if len(location) == 2:
                return self.new_memory_area(location[0], location[1])
            raise KeyError("Tupple access only allowed for tuples of two integers")
        elif isinstance(location, int):
            return DeviceRegister(self, location, lowlevhw.Register('--TEMP_ADDRESS_ACCESS--', 32))
        else:
            if self.definition.has_legacy_defintion():
                device_element_definition = self.definition[location]
                if device_element_definition is None:
                    raise KeyError(f"Device element <{location}> not found.")
                elif isinstance(device_element_definition, lowlevhw.ElementAlias):
                    real_element_definition = self.definition[device_element_definition.element_name]
                else:
                    real_element_definition = device_element_definition
                real_element = None
                if isinstance(real_element_definition, lowlevhw.Register):
                    register_banks = [i.definition for i in self.definition.get_address_space('--LEGACY-REGISTERS--').get_elements()]
                    register_address = None
                    for register_bank in register_banks:
                        try:
                            register_address = register_bank.get_address(real_element_definition.name)
                        except KeyError:
                            pass
                    if register_address is None:
                        raise KeyError(f"Register <{real_element_definition.name}> not found")
                    real_element = DeviceRegister(self, register_address, real_element_definition)
                elif issubclass(type(real_element_definition), lowlevhw.Field):
                    register_name = location.split(':')[0]
                    register_definition = self.definition[register_name]
                    register_banks = [i.definition for i in self.definition.get_address_space('--LEGACY-REGISTERS--').get_elements()]
                    register_address = None
                    for register_bank in register_banks:
                        try:
                            register_address = register_bank.get_address(register_name)
                        except KeyError:
                            pass
                    if register_address is None:
                        raise KeyError(f"Register <{register_name}> not found")
                    register = DeviceRegister(self, register_address, register_definition)
                    real_element = DeviceField(self, register, real_element_definition)
                elif isinstance(real_element_definition, lowlevhw.MemoryArea):
                    real_element = DeviceMemoryArea(self, real_element_definition, 0x0000)
                else:
                    raise TypeError("Unsupported element type returned by the device definition")
                if isinstance(device_element_definition, lowlevhw.ElementAlias):
                    return DeviceElementAlias(real_element, device_element_definition)
                return real_element
            else:
                local_location, rest_location = lowlevhw.split_element_location(location)
                element_definition = self.definition[local_location]
                if element_definition is None:
                    raise KeyError(f"Element <{local_location}> not found in address spaces")
                if isinstance(element_definition, lowlevhw.ElementAlias):
                    aliased_element_name = element_definition.element_name
                    device_element = self[aliased_element_name]
                    return DeviceElementAlias(device_element, element_definition)
                else:
                    if isinstance(element_definition, lowlevhw.Register):
                        base_address = self.definition.get_address(local_location)
                        device_element = DeviceRegister(self, base_address, element_definition)
                    elif isinstance(element_definition, lowlevhw.RegisterBank):
                        base_address = self.definition.get_address(local_location)
                        device_element = DeviceRegisterBank(self, element_definition, base_address)
                    elif isinstance(element_definition, lowlevhw.MemoryMap):
                        base_address = self.definition.get_address(local_location)
                        device_element = DeviceMemoryMap(self, element_definition, base_address)
                    elif isinstance(element_definition, lowlevhw.MemoryArea):
                        if (element_definition.base_is_register):
                            base_address = 0
                        else:
                            base_address = self.definition.get_address(local_location)
                        device_element = DeviceMemoryArea(self, element_definition, base_address)
                    elif isinstance(element_definition, lowlevhw.AddressSpace):
                        base_address = self.definition.get_address(local_location)
                        device_element = DeviceAddressSpace(self, element_definition, base_address)
                    else:
                        raise TypeError(f"Unsupported type {type(element_definition)} returned when accessing device elements")
                    if rest_location == '':
                        return device_element
                    return device_element[rest_location]

    def new_memory_area(self, base, size) -> DeviceMemoryArea:
        # NOTE: Legacy function
        memory_area_def = memory_map.MemoryAreaFixed("____temp____", base, size)
        return DeviceMemoryArea(self, memory_area_def, 0x0000)

    def get_state(self):
        register_values = dict()
        for (_, regbank) in self._register_banks.items():
            for register in regbank.get_registers():
                if register.modes & register_bank.READABLE_MODES:
                    register_values[register] = self[register].read()
        return device_state.DeviceState(register_values, self._register_banks)

    def restore_state(self, state)-> None:
        pass

    def write(self, location, value) -> None:
        print("DEPRECATED: Function Device:write() should not be used anymore.")
        self[location].write(value)

    def read(self, location):
        print("DEPRECATED: Function Device:read() should not be used anymore.")
        return self[location].read()

    def register_state_string(self) -> str:
        REGISTER_NAME_LENGTH = 22
        FIELD_NAME_LENGTH = REGISTER_NAME_LENGTH + 5
        state_string = ""
        for (_, regbank) in self.definition.  _register_banks.items():
            for register in regbank.get_registers():
                if register.modes & register_bank.READABLE_MODES:
                    register_name = register.name
                    address = regbank.get_address(register_name)
                    register_value = self[register].read()
                    state_string += "0x{:03X} - {:<{name_width}}: 0x{:08X}\n".format(
                            address,
                            register_name,
                            register_value,
                            name_width=REGISTER_NAME_LENGTH
                    )
                    if len(register.get_fields()) > 1:
                        for field in register.get_fields():
                            field_name = field.name
                            field_value = self[register][field_name].read()
                            state_string += "   {:<{field_width}}: 0x{:08X}\n".format(
                                    field_name,
                                    field_value,
                                    field_width=FIELD_NAME_LENGTH
                            )
        return state_string

    def export_md(self, md_path):
        ## TODO Include Mapping structure here
        ## TODO Store an overview markdown
        for (address, regbank) in sorted(self._register_banks.items()):
            generator = lowlevhw.generators.RegbankMDDocGen(regbank, md_path)
            generator.generate(md_path)

    @staticmethod
    def remove_transparent_elements(path) -> str:
        cleaned_path = ''
        path_elements = path.split(':')
        for element in path_elements:
            if element[0] != lowlevhw.DeviceDefinition.TRANSPARENCY_MARKER and \
               element[-1] != lowlevhw.DeviceDefinition.TRANSPARENCY_MARKER:
                cleaned_path += f':{element}'
        return cleaned_path[1:]

    def test_registers_after_reset(self) -> bool:
        print()
        print("Register values after reset:")
        print("----------------------------")
        test_passed = True
        for register_path in self.definition.get_registers():
            register = self.definition.get_element(register_path)
            if register.modes & register_bank.READABLE_MODES:
                clean_path = self.remove_transparent_elements(register_path)
                register_value = self[clean_path].read()
                if register.reset_value is None:
                    status = ''
                elif register_value == register.reset_value:
                    status = 'ok'
                else:
                    status = '\033[91m' + 'fail' + '\033[0m' + f" (0x{register.reset_value:0>8x})"
                    test_passed = False
                register_address = self.definition.get_address(clean_path)
                print(f"  0x{register_address:0>8x} - {clean_path:<40}: 0x{register_value:0>8x} - {status}")
        return test_passed

    def test_register_write(self) -> bool:
        print()
        print("Register write test:  write all 0x00000000, then write all 0xFFFFFFFF")
        print("---------------------------------------------------------------------")
        test_passed = True
        for register_path in self.definition.get_registers():
            register = self.definition.get_element(register_path)
            if ('B' in register.modes) and ('X' not in register.modes):
                clean_path = self.remove_transparent_elements(register_path)
                device_register = self[clean_path]
                # Initialize with 0xFFFFFFFF to check if 0s are written correctly
                device_register.write(0xFFFFFFFF)
                if device_register.write_check(0x00000000):
                    status_all_0 = 'ok'
                else:
                    status_all_0 = '\033[91m' + 'fail' + '\033[0m'
                    test_passed = False
                if device_register.write_check(0xFFFFFFFF):
                    status_all_f = 'ok'
                else:
                    status_all_f = '\033[91m' + 'fail' + '\033[0m'
                    test_passed = False
                register_address = self.definition.get_address(clean_path)
                print(f"   {clean_path:<22} (0x{register_address:0>3x}): {status_all_0:>4} - {status_all_f:>4}")
        return test_passed

    def test_memory_block(self, memory_size, dma_block_size, value):
        print("Writing 0x{0:x} to memory".format(value))
        start = time.time()
        for offset in range(0, memory_size, dma_block_size):
            test_block = self.new_memory_area(offset, dma_block_size)
            test_block.write([value]*dma_block_size)
        end = time.time()
        print("Writing done in {0} s".format(end-start))
        # --------------------------------------
        print("Read back data from memory")
        transfer_time = 0
        errors = 0
        counter = 0
        for offset in range(0, memory_size, dma_block_size):
            test_block = self.new_memory_area(offset, dma_block_size)
            counter += 1
            start = time.time()
            result = test_block.read()
            end = time.time()
            transfer_time += end-start
            for read_value in result:
                if read_value != value:
                    errors += 1
        nr_bytes = dma_block_size * counter
        mbit_per_s = nr_bytes * 8 / transfer_time / 1000000
        print("Read {0} Bytes done with {1} errors in {2} s --> {3} Mbit/s".format(nr_bytes, errors, transfer_time, mbit_per_s))
        return errors
