#------------------------------------------------------------------------------
#  File        : generator.py
#  Brief       : Abstract class to represent an output generator for LowLevHW
#------------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2021-10-21
#------------------------------------------------------------------------------
#  Description :
#------------------------------------------------------------------------------
#  Copyright (C) 2021 European Spallation Source ERIC
#------------------------------------------------------------------------------

import abc
import os

from string import Template


class Generator():

    def __init__(self, output_folder):
        self.output_folder = output_folder

    @abc.abstractmethod
    def generate(self):
        pass


class GeneratorTemplate(Generator):

    def __init__(self, output_folder, template_folder, files='*all*') -> None:
        super().__init__(output_folder)
        self.template_snippets = dict()
        self.template_folder = template_folder
        if files == '*all*':
            template_files = os.listdir(self.template_folder)
        else:
            template_files = files
        self.template_files = [os.path.join(self.template_folder, f) for f in template_files]
        self.regbank = None

    @abc.abstractmethod
    def fill_template_snippets(self) -> None:
        pass

    def generate(self):

        if not self.regbank.check_validity():
            return None

        self.fill_template_snippets()

        output_path = self.output_folder + '/' + self.regbank.name
        for template_name in self.template_files:
            with open(template_name, 'r') as f:
                template_file = f.read()
            file_template = Template(template_file)
            file_content = file_template.safe_substitute(self.template_snippets)

            if not os.path.exists(output_path):
                os.makedirs(output_path)
            file_name = os.path.basename(template_name)
            file_base = os.path.splitext(file_name)[0]
            file_extension = os.path.splitext(file_name)[-1]
            if file_extension == '.vho':
                dest_file_name = output_path + '/' + file_base + '.vhdl'
            else:
                dest_file_name = output_path + '/' + file_base + file_extension
            with open(dest_file_name, 'w') as f:
                f.write(file_content)
