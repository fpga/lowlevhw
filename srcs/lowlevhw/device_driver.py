#------------------------------------------------------------------------------
#  File        : device_driver.py
#  Brief       : Abstract class to represent a device driver for LowLevHW.
#------------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2019-02-07
#------------------------------------------------------------------------------
#  Description :
#------------------------------------------------------------------------------
#  Copyright (C) 2018 - 2024 European Spallation Source ERIC
#------------------------------------------------------------------------------

import abc
import getpass
import signal

# Package paramiko is only necessary if SSH access should be possible
try:
    paramiko = None
    import paramiko
except ImportError:
    pass
except Exception as e:
    print("ERROR: Something went wrong while importing module 'paramiko'. Can be ignored if no external access is performed.")
    print(e)


class DeviceDriver(metaclass=abc.ABCMeta):

    @classmethod
    @abc.abstractmethod
    def find_devices(cls):
        pass

    @abc.abstractmethod
    def open_device(self, dev_context):
        pass

    @abc.abstractmethod
    def close_device(self):
        pass

    @abc.abstractmethod
    def read_register(self, address):
        pass

    @abc.abstractmethod
    def write_register(self, address, data):
        pass

    @abc.abstractmethod
    def read_ram(self, offset, size):
        pass

    @abc.abstractmethod
    def write_ram(self, offset, size, data):
        pass

    # def wait_irq(self):
    #    pass
    #
    # def release_irq(self):
    #    pass


class SSHConfig():

    def __init__(self, host=None, port=22, user=None, password=None, keyfile=None):
        self.host = host
        self.port = port
        self.user = user
        self.password = password
        self.keyfile = keyfile

    def apply_config_file(self, configfile_object, section):
        section = configfile_object[section]
        self.host = section.get('host', self.host)
        self.port = section.getint('port', self.port)
        self.user = section.get('user', self.user)
        self.password = section.get('password', self.password)
        self.keyfile = section.get('keyfile', self.keyfile)


class DeviceDriverSSH(metaclass=abc.ABCMeta):

    def __init__(self, target, jumpbox=None):
        self.target = target
        self._target_password = None
        self._client = None
        self.jumpbox = jumpbox
        self._jumpbox_password = None
        self._jumpclient = None

    def __connect_passwd_fallback(self, client, config, password, sock=None):

        def handle_alarm(signum, frame):
            raise Exception

        if password is None:
            password = config.password
        try:
            client.connect(config.host, config.port, config.user, password, key_filename=config.keyfile, sock=sock)
        except paramiko.ssh_exception.PasswordRequiredException:
            if config.user == None:
                user = getpass.getuser()
            else:
                user = config.user
            signal.signal(signal.SIGALRM, handle_alarm)
            signal.alarm(30)
            try:
                print()
                password = getpass.getpass(prompt=f"Password to connect {user}@{config.host}: ")
                signal.alarm(0)
                print()
            except:
                print()
                raise paramiko.AuthenticationException("Timeout on password prompt")
            client.connect(config.host, config.port, user, password, sock=sock)
            return password

    def connect_remote(self):
        self._client = paramiko.SSHClient()
        self._client.set_missing_host_key_policy(paramiko.AutoAddPolicy())                           # Warning: This causes a risk of man-in-the-middle attacks but fails with first connection
        #self._client.load_system_host_keys()                                                        # better
        if (self.jumpbox is not None) and (self.jumpbox.host is not None):
            self._jumpclient = paramiko.SSHClient()
            self._jumpclient.set_missing_host_key_policy(paramiko.AutoAddPolicy())                   # Warning: This causes a risk of man-in-the-middle attacks but fails with first connection
            #self._client.load_system_host_keys()                                                    # better
            self._jumpbox_password = self.__connect_passwd_fallback(self._jumpclient, self.jumpbox, self._jumpbox_password)
            jump_transport = self._jumpclient.get_transport()
            jump_src = (self.jumpbox.host, self.jumpbox.port)
            jump_dest = (self.target.host, self.target.port)
            jump_channel = jump_transport.open_channel("direct-tcpip", jump_dest, jump_src)
            self._target_password = self.__connect_passwd_fallback(self._client, self.target, self._target_password, sock=jump_channel)
        else:
            self._target_password = self.__connect_passwd_fallback(self._client, self.target, self._target_password)

    def close_remote(self):
        self._client.close()
        if self._jumpclient is not None:
            self._jumpclient.close()
            self._jumpclient = None

    def execute_remote(self, command, mode='text'):
        _, stdout, _ = self._client.exec_command(command)
        if mode == 'binary':
            return_data = stdout.read()
        elif mode == 'text':
            lines = stdout.readlines()
            return_data = [x.replace('\n', '') for x in lines]
            if len(return_data) == 1:
                return_data = return_data[0]
        return return_data
