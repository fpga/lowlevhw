#------------------------------------------------------------------------------
#  File        : memorymap_text_doc.py
#  Brief       : Generates a text documentation of a memory map
#------------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2021-10-21
#------------------------------------------------------------------------------
#  Description :
#------------------------------------------------------------------------------
#  Copyright (C) 2021 European Spallation Source ERIC
#------------------------------------------------------------------------------

import lowlevhw


class MemoryMapTextDocGen(lowlevhw.Generator):

    def __init__(self, memory_map: lowlevhw.MemoryMap) -> None:
        self.memory_map = memory_map

    def generate_string(self) -> str:
        return self.memory_map.get_string()

    def generate_console(self) -> None:
        doc_string = self.generate_string()
        print(doc_string)

    def generate(self, folder) -> None:
        doc_string = self.generate_string()
        file_name = self.memory_map.name
        file = f"{folder}/{file_name}.txt"
        f = open(file, 'w')
        f.write(doc_string)
        f.close()
