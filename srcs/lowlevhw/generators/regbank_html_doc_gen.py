#------------------------------------------------------------------------------
#  File        : regbank_html_doc_gen.py
#  Brief       : Generates html documentation of regbank
#------------------------------------------------------------------------------
#  Authors     : Kaj Rosengren
#  Date        : 2021-11-11
#------------------------------------------------------------------------------
#  Description :
#------------------------------------------------------------------------------
#  Copyright (C) 2021 European Spallation Source ERIC
#------------------------------------------------------------------------------
import os
from datetime import datetime
from string import Template
import lowlevhw

class RegbankHtmlDocGen(lowlevhw.GeneratorTemplate):

    def __init__(self, regbank, output_folder) -> None:
        script_location = os.path.dirname(__file__)
        self.template_folder = script_location + '/regbank_html_templates'
        register_bank_files = [
            'overview.html'
            ]
        super().__init__(output_folder, self.template_folder, register_bank_files)
        self.regbank = regbank

    def get_templated(self, filename, snippets):
        template_file = f'{self.template_folder}/{filename}'
        with open(template_file, 'r') as f:
            template_file = f.read()
        return Template(template_file).safe_substitute(snippets)

    def generate_string(self, split_on_id):

        if (split_on_id):
            return self.get_functional_id_split_html()
        else:
            return self.get_regbank_html()

    def __mode_formatting(self, modes):
        output = ""
        for mode in sorted(modes):
            if mode == 'B':
                output += 'R'
            else:
                output += mode
        return output

    def __description_formatting(self, description):
        output = description.replace('\n', '<br />')
        return output

    def __field_description_formatting(self, description):
        output = description.replace('\n', '<br />')
        if (output != ""):
            output = "- " + output
        return output

    def __width_formatting(self, width):
        if (width == 1):
            return "1 Bit"
        else:
            return "{} Bits".format(width)

    def __reset_formatting(self, reset, width):
        return "{0:#0{1}x}".format(reset, (width)//4 + 2)

    def __format_id(self, reset):
        return "{0}, 0x{0:x}".format(reset)

    def __format_register(self, register):
        template_snippets = dict()
        template_snippets['REG_NAME'] = register.name
        template_snippets['REG_ADDR'] = hex(self.regbank.get_address(register.name))
        template_snippets['REG_MODE'] = self.__mode_formatting(register.modes)
        template_snippets['REG_DESCR'] = self.__description_formatting(register.description)
        template_snippets['REG_RESET'] = self.__reset_formatting(register.reset_value, register.width)
        if register.has_fields():
            template_snippets['REG_FIELDS'] = self.get_overview_fields(register)
        else:
            template_snippets['REG_FIELDS'] = ""

        return self.get_templated('overview_reg_entry.htmo', template_snippets)

    def get_overview_fields(self, register):
        html_output = "<ul>\n"
        for field in register.get_fields():
            template_snippets = dict()
            template_snippets['FIELD_NAME']   = field.name
            template_snippets['FIELD_WIDTH']  = self.__width_formatting(field.width)
            template_snippets['FIELD_OFFSET'] = field.offset
            template_snippets['FIELD_RESET']  = self.__reset_formatting(field.reset_value, field.width)
            template_snippets['FIELD_DESCR']  = self.__field_description_formatting(field.description)
            html_output += self.get_templated('overview_field_entry.htmo', template_snippets)
        html_output += "</ul>\n"
        return html_output

    def get_overview_registers(self):
        html_output = ""
        for register in self.regbank.get_registers():
            html_output += self.__format_register(register)
        return html_output

    def get_functional_id_split_html(self):
        func_id_snippets = dict()
        func_id_snippets['REG_INSTANCES'] = ""
        html_output   = ""
        in_id_block   = False # We write the ID blocks on the end
        for address in self.regbank.get_address_list():

            register = self.regbank.get_register_by_address(address)

            # We found a new ID block in the register map
            if (address % 4096 == 0):
                if in_id_block:
                    html_output += self.get_templated('overview_func_id_entry.htmo', func_id_snippets)
                    func_id_snippets = dict()
                    func_id_snippets['REG_INSTANCES'] = ""
                in_id_block = True
                func_id_snippets['FUNC_ID_NAME'] = register.name
                func_id_snippets['FUNC_ADDR']    = hex(address)
                func_id_snippets['FUNC_ID']      = self.__format_id((register.reset_value & 0xFFFFFF00) >> 8)

            func_id_snippets['REG_INSTANCES'] += self.__format_register(register)

        html_output += self.get_templated('overview_func_id_entry.htmo', func_id_snippets)
        return html_output

    def get_regbank_html(self):
        template_snippets = dict()
        template_snippets['REGBANK_NAME'] = self.regbank.name
        template_snippets['REGBANK_ADDR'] = hex(self.regbank.base_address)
        template_snippets['REG_INSTANCES'] = ""
        for register in self.regbank.get_registers():
            template_snippets['REG_INSTANCES'] += self.__format_register(register)

        return self.get_templated('overview_regbank_entry.htmo', template_snippets)

    def fill_template_snippets(self):
        self.template_snippets['REGBANK_NAME'] = self.regbank.name
        self.template_snippets['REG_INSTANCES'] = self.get_overview_registers()
        self.template_snippets['DATE_TIME'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
