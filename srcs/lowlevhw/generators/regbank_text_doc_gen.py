#------------------------------------------------------------------------------
#  File        : regbank_text_doc.py
#  Brief       : Generates a text documentation of the
#------------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2021-10-21
#------------------------------------------------------------------------------
#  Description :
#------------------------------------------------------------------------------
#  Copyright (C) 2021 - 2023 European Spallation Source ERIC
#------------------------------------------------------------------------------

import os

import lowlevhw


class RegbankTextDocGen(lowlevhw.Generator):

    @classmethod
    def generate_string(cls, register_bank: lowlevhw.RegisterBank, base_address=None) -> str:
        if not base_address:
            # DEPRECATION: This code can be romved when base_address doesn't exist anymore.
            base_address = register_bank._base_address
        string = f"{register_bank.name} @ 0x{base_address:X}\n"
        addresses = register_bank.get_address_list()
        for address in addresses:
            register = register_bank.register_map[address]
            full_address = base_address + address
            register_string = register.get_string(field_shift=6)
            string += f"  0x{full_address:0>3X} - {register_string}\n"
        if len(register_bank.shadow_groups) > 0:
            string += "Shadow groups:\n"
            for shadow_grp in register_bank.shadow_groups:
                string += "  "
                string += str(shadow_grp)
        return string

    @classmethod
    def generate_console(self, register_bank: lowlevhw.RegisterBank):
        doc_string = self.generate_string(register_bank)
        print(doc_string)

    @classmethod
    def generate(cls, register_bank: lowlevhw.RegisterBank, output_folder: str):
        doc_string = cls.generate_string(register_bank)
        file_name = register_bank.name
        file = f"{output_folder}/{file_name}.txt"
        try:
            os.makedirs(output_folder)
        except FileExistsError:
            pass
        f = open(file, 'w+')
        f.write(doc_string)
        f.close()
