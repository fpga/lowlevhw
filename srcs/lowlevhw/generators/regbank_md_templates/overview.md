# ${REGBANK_NAME}

## Register map

Base address: ${REGBANK_ADDR}

| Address | Name | Access | Description                 |
| :------ | :--- | -      | :-------------------------- |
${REG_OVERVIEW_LIST}

## Registers

${REG_ENTRIES}


Generated: ${DATE_TIME}
