
## ${REG_NAME}

${REG_DESCR}

Address: ${REG_ADDR}

![](md_img/${REG_NAME}.svg)

| Bits | Value | Name    | Access | Description                  |
| :--- | :---- | :------ | -      | :--------------------------  |
${FIELD_ENTRIES}

Register reset: ${REG_RESET}

Back to [Register map](#register-map). <br>
