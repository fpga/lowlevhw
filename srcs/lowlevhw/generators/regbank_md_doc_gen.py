#------------------------------------------------------------------------------
#  File        : regbank_md_doc_gen.py
#  Brief       : Generates markdown documentation of regbank
#------------------------------------------------------------------------------
#  Authors     : Kaj Rosengren
#  Date        : 2023-10-06
#------------------------------------------------------------------------------
#  Description :
#------------------------------------------------------------------------------
#  Copyright (C) 2023 European Spallation Source ERIC
#------------------------------------------------------------------------------

import os
from datetime import datetime
from string import Template
import lowlevhw
import json
try:
    import wavedrom
except ModuleNotFoundError:
    pass


class RegbankMDDocGen(lowlevhw.GeneratorTemplate):

    def __init__(self, regbank, output_folder):
        script_location = os.path.dirname(__file__)
        self.template_folder = script_location + '/regbank_md_templates'
        register_bank_files = [
            'overview.md'
            ]
        super().__init__(output_folder, self.template_folder, register_bank_files)
        self.regbank = regbank
        self.output_folder = output_folder

    def get_templated(self, filename, snippets):
        template_file = f'{self.template_folder}/{filename}'
        with open(template_file, 'r') as f:
            template_file = f.read()
        return Template(template_file).safe_substitute(snippets)

    def __mode_formatting(self, modes):
        output = ""
        for mode in sorted(modes):
            if mode == 'B':
                output += 'R'
            else:
                output += mode
        return output

    def __description_formatting(self, description):
        output = description.replace('\n', '<br>')
        return output

    ## Dumb function trying to figure out if the field name will fit
    ## in the graphics
    def __abbrv_field(self, name, width):
        if (len(name) > 4 + width):
            abbrv = name[0]

            for i in range(1, len(name)):
                if name[i-1] == ' ' or name[i-1] == '_':
                    abbrv += name[i]

            return abbrv.upper()
        else:
            return name

    def __range_formatting(self, offset, width):
        if (width == 1):
            return "{}".format(offset)
        else:
            return "{}:{}".format(offset+width-1, offset)

    def __reset_formatting(self, reset, width):
        return "{0:#0{1}x}".format(reset, (width)//4 + 2)

    def __format_list_register(self, register):
        template_snippets = dict()
        template_snippets['REG_NAME'] = register.name
        template_snippets['REG_LINK'] = register.name.lower()
        template_snippets['REG_ADDR'] = hex(self.regbank.get_address(register.name))
        template_snippets['REG_MODE'] = self.__mode_formatting(register.modes)
        template_snippets['REG_DESCR'] = self.__description_formatting(register.description)
        return self.get_templated('overview_list_entry.md', template_snippets)

    def __insert_reserved_field(self, upper_offs, lower_offs):
        width = upper_offs - lower_offs
        template_snippets = dict()
        template_snippets['FIELD_RANGE']  = self.__range_formatting(lower_offs, width)
        template_snippets['FIELD_RESET']  = self.__reset_formatting(0, width)
        template_snippets['FIELD_NAME']   = "-"
        template_snippets['FIELD_MODE']  = 'R'
        template_snippets['FIELD_DESCR']  = 'Reserved'
        return self.get_templated('overview_field_entry.md', template_snippets)

    def __get_reg_entry_fields(self, register):
        md_output = ""
        if register.has_fields():
            upper_offset = self.regbank.data_width
            for field in reversed(register.get_fields()):
                template_snippets = dict()
                template_snippets['FIELD_RANGE']  = self.__range_formatting(field.offset, field.width)
                template_snippets['FIELD_RESET']  = self.__reset_formatting(field.reset_value, field.width)
                template_snippets['FIELD_NAME']   = field.name
                template_snippets['FIELD_MODE']  = self.__mode_formatting(field.modes)
                template_snippets['FIELD_DESCR']  = self.__description_formatting(field.description)
                # Figure out if we need to insert a reserved field
                if (upper_offset > (field.offset + field.width)):
                    md_output += self.__insert_reserved_field(upper_offset, field.offset + field.width)
                upper_offset = field.offset
                md_output += self.get_templated('overview_field_entry.md', template_snippets)
        else:
            # Full 32-bit registe without field definition
             template_snippets = dict()
             template_snippets['FIELD_RANGE']  = self.__range_formatting(0,  self.regbank.data_width)
             template_snippets['FIELD_RESET']  = self.__reset_formatting(register.reset_value,  self.regbank.data_width)
             template_snippets['FIELD_NAME']   = "-"
             template_snippets['FIELD_MODE']  = self.__mode_formatting(register.modes)
             template_snippets['FIELD_DESCR']  = "See register description"
             md_output += self.get_templated('overview_field_entry.md', template_snippets)

        return md_output

    def __format_reg_entry(self, register):
        template_snippets = dict()
        template_snippets['REG_NAME'] = register.name
        template_snippets['REG_DESCR'] = self.__description_formatting(register.description)
        template_snippets['REG_ADDR'] = hex(self.regbank.get_address(register.name))
        template_snippets['FIELD_ENTRIES'] = self.__get_reg_entry_fields(register)
        template_snippets['REG_RESET'] = self.__reset_formatting(register.reset_value, self.regbank.data_width)

        return self.get_templated('overview_reg_entry.md', template_snippets)

    def get_overview_registers(self, regbank, start_addr=0x0, end_addr=0xffffffff):
        md_output = ""
        # TODO: remove with deprecated .base_address functionality
        if regbank._base_address != 0:
            base_address = regbank.base_address
        else:
            base_address = 0
        for address, register in regbank.register_map.items():
            if ((base_address + address) >= start_addr) and ((base_address + address) <= end_addr):
                md_output += self.__format_list_register(register)
        return md_output

    def get_reg_entries(self, regbank, start_addr=0x0, end_addr=0xffffffff):
        md_output = ""
        # TODO: remove with deprecated .base_address functionality
        if regbank._base_address != 0:
            base_address = regbank.base_address
        else:
            base_address = 0
        for address, register in regbank.register_map.items():
            if ((base_address + address) >= start_addr) and ((base_address + address) <= end_addr):
                md_output += self.__format_reg_entry(register)
        return md_output

    def gen_images(self, regbank):
        output_path = self.output_folder + "/" + regbank.name
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        img_path = output_path + "/md_img/"
        if not os.path.exists(img_path):
            os.makedirs(img_path)

        for register in regbank.get_registers():
            img_filename = img_path + register.name + ".svg"
            json_reg = {"reg": [],
                        "config": {"bits": self.regbank.data_width, "fontsize": 10}}

            if register.has_fields():
                last_offset = 0
                for field in register.get_fields():

                    # Insert reserved space
                    if field.offset > last_offset:
                        json_reg["reg"].append({"bits": (field.offset - last_offset)})

                    json_reg["reg"].append({"name": self.__abbrv_field(field.name, field.width),
                                            "bits": field.width,
                                            "attr": self.__mode_formatting(field.modes)})

                    last_offset = field.offset + field.width

                if (last_offset < self.regbank.data_width):
                    json_reg["reg"].append({"bits": (self.regbank.data_width - last_offset)})
            else:
                json_reg["reg"].append({"name": register.name,
                                        "bits":  self.regbank.data_width,
                                        "attr": self.__mode_formatting(register.modes)})
            try:
                wavedrom.render(json.dumps(json_reg)).saveas(img_filename)
            except NameError:
                raise ModuleNotFoundError("Package wavedrom might not availbale, probably because of running Python < 3.7")

    def fill_template_snippets(self):
        self.template_snippets['REGBANK_NAME'] = self.regbank.name
        # TODO: remove with deprecated .base_address functionality
        if self.regbank._base_address != 0:
            base_address = self.regbank.base_address
        else:
            base_address = 0
        self.template_snippets['REGBANK_ADDR'] = hex(base_address)
        self.template_snippets['REG_OVERVIEW_LIST'] = self.get_overview_registers(self.regbank)
        self.template_snippets['DATE_TIME'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.template_snippets['REG_ENTRIES'] = self.get_reg_entries(self.regbank)
        self.gen_images(self.regbank)
