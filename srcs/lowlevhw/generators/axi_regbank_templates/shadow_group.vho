  ${SHADOW_GRP_NAME}_shadow_group : process (clock_i)
  begin
    if rising_edge(clock_i) then
      if reset_n_i = '0' then
${SHADOW_SIGNAL_ASSIGNMENTS}
      else
        if transfer_shadow_group_i.${SHADOW_GRP_NAME} = '1' then
${SHADOW_SIGNAL_ASSIGNMENTS}
        end if;
      end if;
    end if;
  end process;
