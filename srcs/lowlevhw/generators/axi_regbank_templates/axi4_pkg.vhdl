--------------------------------------------------------------------------------
-- Project     : LowLevHW
--------------------------------------------------------------------------------
-- File        : axi4_pkg.vhdl
-- Authors     : Christian Amstutz
-- Created     : 2020-10-09
-- Platform    :
-- Standard    : VHDL'93
--------------------------------------------------------------------------------
-- Description : Package containing definitions about the AXI4 standard.
-- Problems    :
--------------------------------------------------------------------------------
-- Copyright (c) 2018 - 2020 European Spallation Source ERIC
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

--------------------------------------------------------------------------------
--! Package containing definitions about the AXI4 standard and records to model
--! different types of AXI4 buses.
--------------------------------------------------------------------------------
package axi4 is

  --! Width of burst length signals (AWLEN, ARLEN)
  constant AXI4_LEN_WIDTH     : integer := 8;

  --! Width of burst size signals (AWSIZE, ARSIZE)
  constant AXI4_SIZE_WIDTH    : integer := 3;

  --! Width of burst type signals (AWBURST, ARBURST)
  constant AXI4_BURST_WIDTH   : integer := 2;

  --! Width of memory type signals (AWCACHE, ARCACHE)
  constant AXI4_CACHE_WIDTH   : integer := 4;

  --! Default value for AXI4 CACHE signals as recomended by Xilinx
  constant AXI4_CACHE_DEFAULT : std_logic_vector(AXI4_CACHE_WIDTH-1 downto 0) := "0011";

  --! Width of lock signals (AWCLOCK, ARLOCK)
  constant AXI4_LOCK_WIDTH    : integer := 1;

  --! Default value for AXI4 lock signals
  constant AXI4_LOCK_DEFAULT  : std_logic_vector(AXI4_LOCK_WIDTH-1 downto 0) := "0";

  --! Width of access permission signals (AWPROT, ARPROT)
  constant AXI4_PROT_WIDTH    : integer := 3;

  --! Width of quality of service (QOS) signals (AWQOS, ARQOS)
  constant AXI4_QOS_WIDTH     : integer := 4;

  --! Default value for AXI4 QOS signals
  constant AXI4_QOS_DEFAULT   : std_logic_vector(AXI4_QOS_WIDTH-1 downto 0) := (others => '0');

  --! Width of region signals (AWREGION, ARREGION)
  constant AXI4_REGION_WIDTH  : integer := 4;

  --! Width of response signals (BRESP, RRESP)
  constant AXI4_RESP_WIDTH    : integer := 2;

  -- Constants defining different burst types
  constant AXI4_BURST_FIXED : std_logic_vector(AXI4_BURST_WIDTH-1 downto 0) := "00";
  constant AXI4_BURST_INCR  : std_logic_vector(AXI4_BURST_WIDTH-1 downto 0) := "01";
  constant AXI4_BURST_WRAP  : std_logic_vector(AXI4_BURST_WIDTH-1 downto 0) := "10";

  constant AXI4_PROT_NONE   : std_logic_vector(AXI4_PROT_WIDTH-1 downto 0) := "000";

  constant AXI4_RESP_OKAY   : std_logic_vector(AXI4_RESP_WIDTH-1 downto 0) := "00";
  constant AXI4_RESP_EXOKAY : std_logic_vector(AXI4_RESP_WIDTH-1 downto 0) := "01";
  constant AXI4_RESP_SLVERR : std_logic_vector(AXI4_RESP_WIDTH-1 downto 0) := "10";
  constant AXI4_RESP_DECERR : std_logic_vector(AXI4_RESP_WIDTH-1 downto 0) := "11";

  --! Width of data signal for Xilinx AXI4-Lite interfaces
  constant AXI4L_DATA_WIDTH   : integer := 32;

  --! Width of strobe signal for Xilinx AXI4-Lite interfaces
  constant AXI4L_STRB_WIDTH   : integer := 4;

  --! Calculate the number of full bytes needed to represent a number of bits.
  --! This function can be used to calculate the needed TDATA width of an AXI4-S
  --! port.
  function byte_aligned_width(nr_bits : in positive) return natural;

end package axi4;

package body axi4 is

  function byte_aligned_width(nr_bits : in positive) return natural is
    variable nr_bytes : positive;
  begin
    nr_bytes := (nr_bits-1)/8 + 1;
    return nr_bytes * 8;
  end function;

end package body;
