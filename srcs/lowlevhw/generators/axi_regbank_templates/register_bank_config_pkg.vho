--------------------------------------------------------------------------------
-- Project     : LowLevHW
--------------------------------------------------------------------------------
-- File        : register_bank_config_pkg.vhdl
-- Authors     : Christian Amstutz
-- Created     : 2018-03-14
-- Platform    :
-- Standard    : VHDL'93
--------------------------------------------------------------------------------
-- Description :
--------------------------------------------------------------------------------
-- Copyright (C) 2018 - 2019 European Spallation Source ERIC
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library ${REGBANK_LIBRARY};
use ${REGBANK_LIBRARY}.register_types.all;


package register_bank_config is

  constant ADDRESS_WIDTH         : integer   := ${ADDRESS_WIDTH};
  constant REGISTER_WIDTH        : integer   := ${REGISTER_WIDTH};
  constant WSTRB_WIDTH           : integer   := ${WSTRB_WIDTH};
  constant ENABLE_ERROR_DECODING : boolean   := ${ENABLE_ERROR_DECODING};
  constant UNDEFINED_FIELD_VALUE : std_logic := '0';

  type register_write_en_t is record
${REGISTER_WRITE_EN_RECORD}
  end record;

  type field_write_en_t is record
${FIELD_WRITE_EN_RECORD}
  end record;

  type field_data_t is record
${FIELD_DATA_RECORD}
  end record;

  type register_bus_read_t is record
${BUS_READ_RECORD}
  end record;

  type logic_read_data_t is record
${LOGIC_READ_DATA_RECORD}
  end record;

  type logic_return_t is record
${LOGIC_RETURN_RECORD}
  end record;

  type transfer_shadow_group_t is record
${TRANSFER_SHADOW_GROUP_RECORD}
  end record;

  ${SHADOW_GROUP_RECORDS}

end register_bank_config;
