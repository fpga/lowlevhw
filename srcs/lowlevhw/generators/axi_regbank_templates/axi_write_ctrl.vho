--------------------------------------------------------------------------------
-- Project     : LowLevHW
--------------------------------------------------------------------------------
-- File        : axi_write_ctrl.vhdl
-- Authors     : Christian Amstutz
-- Created     : 2018-03-13
-- Platform    : Xilinx Ultrascale
-- Standard    : VHDL'93
--------------------------------------------------------------------------------
-- Description : AXI4-Lite write controller to access the register bank.
--------------------------------------------------------------------------------
-- Copyright (C) 2018 - 2019 European Spallation Source ERIC
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;

library ${REGBANK_LIBRARY};
use ${REGBANK_LIBRARY}.axi4.all;

--------------------------------------------------------------------------------
--!
--------------------------------------------------------------------------------
entity axi_write_ctrl  is
  generic (
    ELEMENT_ADDRESSING : boolean := false;
    REG_ADDR_WIDTH     : integer := 8;
    AXI_ADDR_WIDTH     : integer := 10;
    AXI_WSTRB_WIDTH    : integer := 4;                                          --! Width of the AXI wstrb signal, may be determined by DATA_WIDTH
    AXI_DATA_WIDTH     : integer := 32;                                         --! Width of the AXI data signals
    REGISTER_WIDTH     : integer := 32                                          --! Width of a register
  );
  port (
    s_axi_aclk    : in  std_logic;                                              --! Clock of the whole block and the AXI interface
    s_axi_aresetn : in  std_logic;                                              --! Synchronous reset, active-low

    ----------------------------------------------------------------------------
    --! @name AXI4-Lite write interface
    --!@{
    ----------------------------------------------------------------------------
    s_axi_awaddr  : in  std_logic_vector(AXI_ADDR_WIDTH-1 downto 0);
    s_axi_awprot  : in  std_logic_vector(AXI4_PROT_WIDTH-1 downto 0);
    s_axi_awvalid : in  std_logic;
    s_axi_awready : out std_logic;
    s_axi_wdata   : in  std_logic_vector(AXI_DATA_WIDTH-1 downto 0);
    s_axi_wstrb   : in  std_logic_vector(AXI_WSTRB_WIDTH-1 downto 0);
    s_axi_wvalid  : in  std_logic;
    s_axi_wready  : out std_logic;
    s_axi_bresp   : out std_logic_vector(AXI4_RESP_WIDTH-1 downto 0);
    s_axi_bvalid  : out std_logic;
    s_axi_bready  : in  std_logic;
    --!@}

    write_en_o    : out std_logic;                                              --! Write enable towards the register bank
    write_strb_o  : out std_logic_vector(AXI_WSTRB_WIDTH-1 downto 0);           --! Write strobes
    address_o     : out std_logic_vector(REG_ADDR_WIDTH-1 downto 0);            --! Address of the register to be written
    bus_data_o    : out std_logic_vector(REGISTER_WIDTH-1 downto 0);            --! Data that is written to the register
    error_i       : in  std_logic                                               --! Error signal from register bank
  );
end entity axi_write_ctrl;

--------------------------------------------------------------------------------
--!
--------------------------------------------------------------------------------
architecture rtl of axi_write_ctrl is

  constant LOW_BITS_ADDR_REMOVE : natural := integer(ceil(log2(real(REGISTER_WIDTH)/8.0)));
  constant HIGH_ADDR_ZEROES     : std_logic_vector(LOW_BITS_ADDR_REMOVE-1 downto 0) := (others =>'0');

  type   axi_write_state_t is (READY, WAIT_AWVALID, WAIT_WVALID, WRITE_DATA, RESPONSE);     --! States of the data write FSM
  signal axi_write_state   : axi_write_state_t := READY;                                    --! The next state of the FSM
  signal axi_write_state_r : axi_write_state_t := READY;                                    --! Registered state of the FSM, current state

  signal reg_address_in : std_logic_vector(REG_ADDR_WIDTH-1 downto 0);
  signal reg_address    : std_logic_vector(REG_ADDR_WIDTH-1 downto 0) := (others => '0');
  signal reg_address_r  : std_logic_vector(REG_ADDR_WIDTH-1 downto 0) := (others => '0');
  signal data           : std_logic_vector(REGISTER_WIDTH-1 downto 0) := (others => '0');
  signal data_r         : std_logic_vector(REGISTER_WIDTH-1 downto 0) := (others => '0');
  signal wstrb          : std_logic_vector(AXI_WSTRB_WIDTH-1 downto 0) := (others => '0');
  signal wstrb_r        : std_logic_vector(AXI_WSTRB_WIDTH-1 downto 0) := (others => '0');

begin

  reg_address_gen : if ELEMENT_ADDRESSING generate
    reg_address_in <= HIGH_ADDR_ZEROES & s_axi_awaddr(REG_ADDR_WIDTH-1 downto LOW_BITS_ADDR_REMOVE);
  else generate
    reg_address_in <= s_axi_awaddr(REG_ADDR_WIDTH-1 downto 0);
  end generate;

  -- Write data control FSM
  axi_write_fsm_reg : process(all)
  begin
    if rising_edge(s_axi_aclk) then
      if s_axi_aresetn = '0' then
        axi_write_state_r <= READY;
        wstrb_r           <= (others => '0');
      else
        axi_write_state_r <= axi_write_state;
        reg_address_r     <= reg_address;
        data_r            <= data;
        wstrb_r           <= wstrb;
      end if;
    end if;
  end process axi_write_fsm_reg;

  axi_write_fsm_comb : process(all)
  begin

    s_axi_awready <= '0';
    s_axi_wready <= '0';
    s_axi_bvalid <= '0';
    s_axi_bresp <= AXI4_RESP_OKAY;
    write_en_o <= '0';
    reg_address <= reg_address_r;
    data  <= data_r;
    wstrb <= wstrb_r;
    axi_write_state <= axi_write_state_r;

    case axi_write_state_r is

      when READY =>
        s_axi_awready <= '1';
        s_axi_wready <= '1';
        if (s_axi_awvalid = '1') and (s_axi_wvalid = '0') then
          reg_address <= reg_address_in;
          axi_write_state <= WAIT_WVALID;
        elsif (s_axi_awvalid = '0') and (s_axi_wvalid = '1') then
          data  <= s_axi_wdata;
          wstrb <= s_axi_wstrb;
          axi_write_state <= WAIT_AWVALID;
        elsif (s_axi_awvalid = '1') and (s_axi_wvalid = '1') then
          reg_address <= reg_address_in;
          data  <= s_axi_wdata;
          wstrb <= s_axi_wstrb;
          axi_write_state <= WRITE_DATA;
        end if;

      when WAIT_AWVALID =>
        s_axi_awready <= '1';
        if (s_axi_awvalid = '1') then
          reg_address <= reg_address_in;
          axi_write_state <= WRITE_DATA;
        end if;

      when WAIT_WVALID =>
        s_axi_wready <= '1';
        if (s_axi_wvalid = '1') then
          data  <= s_axi_wdata;
          wstrb <= s_axi_wstrb;
          axi_write_state <= WRITE_DATA;
        end if;

      when WRITE_DATA =>
        write_en_o <= '1';
        if (s_axi_awvalid = '0') and (s_axi_wvalid = '0') then
          axi_write_state <= RESPONSE;
        end if;

      when RESPONSE =>
        s_axi_bvalid <= '1';
        if error_i = '0' then
          s_axi_bresp <= AXI4_RESP_OKAY;
        else
          s_axi_bresp <= AXI4_RESP_SLVERR;
        end if;

        if (s_axi_bready = '1') then
          axi_write_state <= READY;
        end if;

      when others =>
        axi_write_state <= READY;

    end case;

  end process;

  address_o    <= reg_address_r;
  bus_data_o   <= data_r;
  write_strb_o <= wstrb_r;

end architecture rtl;
