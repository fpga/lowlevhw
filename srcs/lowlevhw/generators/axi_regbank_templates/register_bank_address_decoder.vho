--------------------------------------------------------------------------------
-- Project     : LowLevHW
--------------------------------------------------------------------------------
-- File        : register_bank_address_decoder.vhdl
-- Authors     : Christian Amstutz
-- Created     : 2018-03-12
-- Platform    : Xilinx Ultrascale
-- Standard    : VHDL'93
--------------------------------------------------------------------------------
-- Description : Template for the address decoder of the AXI-4 register bank
--               generator. Generates the according write enables for the
--               different registers.
--------------------------------------------------------------------------------
-- Copyright (C) 2018 - 2023 European Spallation Source ERIC
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ${REGBANK_LIBRARY};
use ${REGBANK_LIBRARY}.register_bank_config.all;
use ${REGBANK_LIBRARY}.register_types.all;

--------------------------------------------------------------------------------
--!
--------------------------------------------------------------------------------
entity register_bank_address_decoder  is
  generic (
    ELEMENT_ADDRESSING    : boolean := false;
    ENABLE_ERROR_DECODING : boolean := false;
    ADDRESS_WIDTH         : integer := ADDRESS_WIDTH;
    WSTRB_WIDTH           : integer := 4;
    REGISTER_WIDTH        : integer := REGISTER_WIDTH
  );
  port (
    clock_i             : in  std_logic;
    reset_n_i           : in  std_logic;

    decode_error_o      : out std_logic;                                         --! Status signal that indicates that a non-existing address should be accessed

    write_en_i          : in  std_logic;                                         --! Global write enable signal
    wstrb_i             : in  std_logic_vector(WSTRB_WIDTH-1 downto 0);
    address_i           : in  std_logic_vector(ADDRESS_WIDTH-1 downto 0);        --! Address of the register
    bus_data_i          : in  std_logic_vector(REGISTER_WIDTH-1 downto 0);       --! Data coming from the bus

    register_write_en_o : out register_write_en_t;                               --! Record with an enable signal for each register
    register_wstrb_o    : out std_logic_vector(WSTRB_WIDTH-1 downto 0);
    register_data_o     : out std_logic_vector(REGISTER_WIDTH-1 downto 0)        --! Output data to the registers
  );
end entity register_bank_address_decoder;

--------------------------------------------------------------------------------
--! @brief RTL description of the register bank write controller.
--------------------------------------------------------------------------------
architecture rtl of register_bank_address_decoder is

  constant LOW_ADDR_BLANK_MASK : std_logic_vector(ADDRESS_WIDTH-1 downto 0) := std_logic_vector(to_unsigned(integer(real(REGISTER_WIDTH)/8.0-1.0), ADDRESS_WIDTH));

  signal address_int : integer;                                                  --! Integer representation of the register address

begin

  address_gen : if ELEMENT_ADDRESSING generate
    address_int <= to_integer(unsigned(address_i));
  else generate
    address_int <= to_integer(unsigned(address_i and not(LOW_ADDR_BLANK_MASK)));
  end generate;

  write_select : process(clock_i)
  begin
    if rising_edge(clock_i) then
      if reset_n_i = '0' then
          decode_error_o <= '0';
      else
        -- Put '0' on all outputs per default
        register_write_en_o <= (others => '0');
        decode_error_o <= decode_error_o;

        if (write_en_i = '1') then
          decode_error_o <= '0';
            case address_int is

${REGISTER_WRITE_EN_SELECT}

              when others =>
                if ENABLE_ERROR_DECODING then
                  decode_error_o <= '1';
                end if;
            end case;
        end if;
      end if;
    end if;
  end process write_select;

  -- Forward bus data to the registers
  register_data_o  <= bus_data_i;
  register_wstrb_o <= wstrb_i;

end architecture rtl;
