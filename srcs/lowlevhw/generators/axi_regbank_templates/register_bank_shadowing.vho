--------------------------------------------------------------------------------
-- Project     : LowLevHW
--------------------------------------------------------------------------------
-- File        : register_bank_shadowing.vhdl
-- Authors     : Christian Amstutz
-- Created     : 2018-05-28
-- Platform    : Xilinx Ultrascale
-- Standard    : VHDL'93
--------------------------------------------------------------------------------
-- Description :
--               Combinatorial block.
--------------------------------------------------------------------------------
-- Copyright (C) 2018 - 2019 European Spallation Source ERIC
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library ${REGBANK_LIBRARY};
use ${REGBANK_LIBRARY}.register_bank_config.all;
use ${REGBANK_LIBRARY}.register_bank_functions.all;

--------------------------------------------------------------------------------
--!
--------------------------------------------------------------------------------
entity register_bank_shadowing is
  port (
    clock_i                 : in  std_logic;
    reset_n_i               : in  std_logic;

    transfer_shadow_group_i : in  transfer_shadow_group_t;

    logic_data_i            : in  logic_read_data_t;
    logic_data_o            : out logic_read_data_t
  );
end entity register_bank_shadowing;

--------------------------------------------------------------------------------
--! @brief
--------------------------------------------------------------------------------
architecture rtl of register_bank_shadowing is
begin

${SHADOW_GROUP_INSTANCES}

${NON_SHADOWED_SIGNALS}

end architecture rtl;
