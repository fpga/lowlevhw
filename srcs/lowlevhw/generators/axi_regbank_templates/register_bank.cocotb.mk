#-------------------------------------------------------------------------------
# Project     : LowLevHW
#-------------------------------------------------------------------------------
# File        : register_bank.cocotb.mk
# Authors     : Christian Amstutz
# Created     : 2024-12-12
# Platform    : Xilinx Vivado 2023.1
# Standard    :
#-------------------------------------------------------------------------------
# Description : Files used for cocotb simulations
#-------------------------------------------------------------------------------
# Copyright (C) 2018 - 2024 European Spallation Source ERIC
#-------------------------------------------------------------------------------

# Path to this makefile ###################################################
MKFILE_DIR_${REGBANK_LIBRARY} := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

# Add VHDL sources #############################################################
VHDL_SOURCES_${REGBANK_LIBRARY} += $(MKFILE_DIR_${REGBANK_LIBRARY})/axi4_pkg.vhdl
VHDL_SOURCES_${REGBANK_LIBRARY} += $(MKFILE_DIR_${REGBANK_LIBRARY})/register_types_pkg.vhdl
VHDL_SOURCES_${REGBANK_LIBRARY} += $(MKFILE_DIR_${REGBANK_LIBRARY})/register_bank_config_pkg.vhdl
VHDL_SOURCES_${REGBANK_LIBRARY} += $(MKFILE_DIR_${REGBANK_LIBRARY})/register_bank_functions_pkg.vhdl

VHDL_SOURCES_${REGBANK_LIBRARY} += $(MKFILE_DIR_${REGBANK_LIBRARY})/field_core.vhdl
VHDL_SOURCES_${REGBANK_LIBRARY} += $(MKFILE_DIR_${REGBANK_LIBRARY})/register_bank_address_decoder.vhdl
VHDL_SOURCES_${REGBANK_LIBRARY} += $(MKFILE_DIR_${REGBANK_LIBRARY})/register_bank_rd_interface.vhdl
VHDL_SOURCES_${REGBANK_LIBRARY} += $(MKFILE_DIR_${REGBANK_LIBRARY})/register_bank_rd_encoder.vhdl
VHDL_SOURCES_${REGBANK_LIBRARY} += $(MKFILE_DIR_${REGBANK_LIBRARY})/register_bank_wr_interface.vhdl
VHDL_SOURCES_${REGBANK_LIBRARY} += $(MKFILE_DIR_${REGBANK_LIBRARY})/register_bank_shadowing.vhdl
VHDL_SOURCES_${REGBANK_LIBRARY} += $(MKFILE_DIR_${REGBANK_LIBRARY})/register_bank_core.vhdl
VHDL_SOURCES_${REGBANK_LIBRARY} += $(MKFILE_DIR_${REGBANK_LIBRARY})/register_bank.vhdl

VHDL_SOURCES_${REGBANK_LIBRARY} += $(MKFILE_DIR_${REGBANK_LIBRARY})/axi_read_ctrl.vhdl
VHDL_SOURCES_${REGBANK_LIBRARY} += $(MKFILE_DIR_${REGBANK_LIBRARY})/axi_write_ctrl.vhdl
VHDL_SOURCES_${REGBANK_LIBRARY} += $(MKFILE_DIR_${REGBANK_LIBRARY})/register_bank_axi.vhdl

# libraries to add to VHDL_LIB_ORDER: ${REGBANK_LIBRARY}
