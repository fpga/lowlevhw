--------------------------------------------------------------------------------
-- Project     : LowLevHW
--------------------------------------------------------------------------------
-- File        : register_bank_rd_interface.vhdl
-- Authors     : Christian Amstutz
-- Created     : 2018-05-21
-- Platform    : Xilinx Ultrascale
-- Standard    : VHDL'93
--------------------------------------------------------------------------------
-- Description :
--               Combinatorial block.
--------------------------------------------------------------------------------
-- Copyright (C) 2018 - 2019 European Spallation Source ERIC
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library ${REGBANK_LIBRARY};
use ${REGBANK_LIBRARY}.register_bank_config.all;
use ${REGBANK_LIBRARY}.register_bank_functions.all;

--------------------------------------------------------------------------------
--!
--------------------------------------------------------------------------------
entity register_bank_rd_interface is
  port (
    field_value_i      : in  field_data_t;
    logic_value_i      : in  field_data_t;

    register_data_o    : out register_bus_read_t
  );
end entity register_bank_rd_interface;

--------------------------------------------------------------------------------
--! @brief
--------------------------------------------------------------------------------
architecture rtl of register_bank_rd_interface is
begin

  read_interface_cores : process (field_value_i, logic_value_i)
  begin

    -- Make all undefined registers/fields read as UNDEFINED_FIELD_VALUE
    register_data_o <= (others => (others => UNDEFINED_FIELD_VALUE));

${READ_INTERFACE_CORES}

  end process read_interface_cores;

end architecture rtl;
