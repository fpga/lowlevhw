--------------------------------------------------------------------------------
-- Project     : LowLevHW
--------------------------------------------------------------------------------
-- File        : register_bank_wr_interface.vhdl
-- Authors     : Christian Amstutz
-- Created     : 2018-05-21
-- Platform    : Xilinx Ultrascale
-- Standard    : VHDL'93
--------------------------------------------------------------------------------
-- Description :
--               Combinatorial block.
--------------------------------------------------------------------------------
-- Copyright (C) 2018 - 2019 European Spallation Source ERIC
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library ${REGBANK_LIBRARY};
use ${REGBANK_LIBRARY}.register_bank_config.all;
use ${REGBANK_LIBRARY}.register_bank_functions.all;

--------------------------------------------------------------------------------
--!
--------------------------------------------------------------------------------
entity register_bank_wr_interface  is
  port (
    register_write_en_i   : in  register_write_en_t;
    register_write_strb_i : in  std_logic_vector(WSTRB_WIDTH-1 downto 0);
    data_bus_i            : in  std_logic_vector(REGISTER_WIDTH-1 downto 0);

    current_field_value_i : in  field_data_t;

    field_write_en_o      : out field_write_en_t;
    field_data_o          : out field_data_t
  );
end entity register_bank_wr_interface;

--------------------------------------------------------------------------------
--! @brief
--------------------------------------------------------------------------------
architecture rtl of register_bank_wr_interface is

${WRITE_INTERFACE_EN_SIGS}

begin

  write_interface_cores : process (all)
  begin
${WRITE_INTERFACE_CORES}

  end process write_interface_cores;

end architecture rtl;
