
  -- Field: ${FIELD_NAME}
  field_${FIELD_NAME} : entity ${VHDL_LIBRARY}.field_core
    generic map (
      WIDTH => ${FIELD_WIDTH},
      RESET_VALUE => ${RESET_VALUE}
    )
    port map (
      clock_i           => clock_i,
      reset_n_i         => reset_n_i,
      write_en_i        => ${WRITE_EN},
      bus_write_data_i  => bus_write_data_i.${FIELD_NAME},
      field_value_o     => current_data_o.${FIELD_NAME},
      logic_to_bus_o    => logic_to_bus_o.${FIELD_NAME},
      logic_data_o${LOGIC_READ},
      logic_return_i${LOGIC_RETURN}
    );
