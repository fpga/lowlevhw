--------------------------------------------------------------------------------
-- Project     : LowLevHW
--------------------------------------------------------------------------------
-- File        : register_bank_register_pkg.vhdl
-- Authors     : Christian Amstutz
-- Created     : 2021-10-22
-- Platform    :
-- Standard    : VHDL'93
--------------------------------------------------------------------------------
-- Description :
--------------------------------------------------------------------------------
-- Copyright (C) 2018 - 2021 European Spallation Source ERIC
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

package register_types is

${REGISTER_CONSTANTS}

${REGISTER_TYPE_RECORDS}

${REGISTER_TYPE_FUNC_DECLARATIONS}

end package;

package body register_types is

${REGISTER_TYPE_FUNCTIONS}

end package body register_types;