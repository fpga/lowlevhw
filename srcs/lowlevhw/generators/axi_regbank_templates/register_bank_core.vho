--------------------------------------------------------------------------------
-- Project     : LowLevHW
--------------------------------------------------------------------------------
-- File        : register_bank_core.vhdl
-- Authors     : Christian Amstutz
-- Created     : 2018-03-12
-- Platform    : Xilinx Ultrascale
-- Standard    : VHDL'93
--------------------------------------------------------------------------------
-- Description : Collection of the register fields belonging to the register
--               bank.
--------------------------------------------------------------------------------
-- Copyright (C) 2018 - 2019 European Spallation Source ERIC
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library ${REGBANK_LIBRARY};
use ${REGBANK_LIBRARY}.register_bank_config.all;
use ${REGBANK_LIBRARY}.register_bank_functions.all;

--------------------------------------------------------------------------------
--!
--------------------------------------------------------------------------------
entity register_bank_core  is
  port (
    clock_i          : in  std_logic;                                           --! The system clock that is a multiple of the ADC clock
    reset_n_i        : in  std_logic;                                           --! Low active reset signal

    write_en_i       : in  field_write_en_t;                                    --! Record of write enable signals, one enable for each field
    bus_write_data_i : in  field_data_t;                                        --! Write data from the data bus
    current_data_o   : out field_data_t;                                        --! Output of all the register values that can be read
    logic_to_bus_o   : out field_data_t;

    logic_data_o     : out logic_read_data_t;                                   --! Output values of the registers and register parts towards the logic
    logic_return_i   : in  logic_return_t                                       --! Return values of the registers and register parts from the logic
  );

end entity register_bank_core;

--------------------------------------------------------------------------------
--!
--------------------------------------------------------------------------------
architecture rtl of register_bank_core is
begin

${FIELD_INSTANCES}

end architecture rtl;
