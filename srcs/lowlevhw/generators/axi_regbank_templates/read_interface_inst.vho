
    read_interface_${TYPE}(
      width            => ${WIDTH},
      offset           => ${OFFSET},
      register_value   => field_value_i.${FIELD_NAME},
      logic_value      => logic_value_i.${FIELD_NAME},
      register_rd_data => register_data_o.${REGISTER_NAME}
    );
