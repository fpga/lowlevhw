--------------------------------------------------------------------------------
-- Project     : LowLevHW
--------------------------------------------------------------------------------
-- File        : register_bank_rd_encoder.vhdl
-- Authors     : Christian Amstutz
-- Created     : 2018-03-12
-- Platform    : Xilinx Ultrascale
-- Standard    : VHDL'93
--------------------------------------------------------------------------------
-- Description :
--------------------------------------------------------------------------------
-- Copyright (C) 2018 - 2019 European Spallation Source ERIC
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ${REGBANK_LIBRARY};
use ${REGBANK_LIBRARY}.register_bank_config.all;
use ${REGBANK_LIBRARY}.register_types.all;

--------------------------------------------------------------------------------
--!
--------------------------------------------------------------------------------
entity register_bank_rd_encoder  is
  generic (
    ELEMENT_ADDRESSING    : boolean := false;
    ENABLE_ERROR_DECODING : boolean := false
  );
  port (
    clock_i         : in  std_logic;                                    --! The output data is registered according to this clock
    reset_n_i       : in  std_logic;                                    --! Asynchronous low-active reset

    decode_error_o  : out std_logic;                                    --! Read access to address which does not allow read

    read_en_i       : in  std_logic;                                    --! Read enable
    address_i       : in  std_logic_vector(ADDRESS_WIDTH-1 downto 0);   --! Register address
    register_data_i : in  register_bus_read_t;                          --! Record of values stored in the registers

    bus_data_o      : out std_logic_vector(REGISTER_WIDTH-1 downto 0)   --! Value of selected register (registered)
  );
end entity register_bank_rd_encoder;

--------------------------------------------------------------------------------
--! @brief
--------------------------------------------------------------------------------
architecture rtl of register_bank_rd_encoder is

  constant LOW_ADDR_BLANK_MASK : std_logic_vector(ADDRESS_WIDTH-1 downto 0) := std_logic_vector(to_unsigned(integer(real(REGISTER_WIDTH)/8.0-1.0), ADDRESS_WIDTH));

  signal address_int     : integer;                                           --! Integer representation of the register address
  signal illegal_address : std_logic;

begin

  address_gen : if ELEMENT_ADDRESSING generate
    address_int <= to_integer(unsigned(address_i));
  else generate
    address_int <= to_integer(unsigned(address_i and not(LOW_ADDR_BLANK_MASK)));
  end generate;

  output_select : process (clock_i)
  begin
    if rising_edge(clock_i) then
      if (reset_n_i = '0') then
        decode_error_o <= '0';
        bus_data_o <= (others => '0');
      else
        decode_error_o <= decode_error_o;
        if (read_en_i = '1') then

          decode_error_o <= '0';

            case address_int is

${REGISTER_TO_BUS_SELECT}

              when others =>
                if ENABLE_ERROR_DECODING then
                  decode_error_o <= '1';
                else
                  bus_data_o <= (others => '0');
                end if;
            end case;
        end if;
      end if;
    end if;
  end process output_select;

end architecture rtl;
