
    ${FIELD_NAME}_write_en <= ${CONCAT_WRITE_ENS};
    write_interface_core(
      offsets        => (${OFFSETS}),
      interface_mode => (${INTERFACE_MODES}),
      write_en       => ${FIELD_NAME}_write_en,
      wstrb          => register_write_strb_i,
      bus_data       => data_bus_i,
      field_wr_en    => field_write_en_o.${FIELD_NAME},
      field_wr_data  => field_data_o.${FIELD_NAME},
      field_value    => current_field_value_i.${FIELD_NAME}
    );
