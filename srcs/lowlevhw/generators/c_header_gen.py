#------------------------------------------------------------------------------
#  File        : c_header_gen.py
#  Brief       : Generates the struct based c-header files
#------------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2023-10-16
#------------------------------------------------------------------------------
#  Description :
#------------------------------------------------------------------------------
#  Copyright (C) 2021 - 2023 European Spallation Source ERIC
#------------------------------------------------------------------------------

import math
import os

import lowlevhw


class RegisterBankCHeaderGen(lowlevhw.Generator):

    HEADER="""\
// Automatically generated register bank file

#include <stdint.h>

"""
    indention_spaces = 4

    def __init__(self, regbank: lowlevhw.RegisterBank, output_folder: str) -> None:
        super().__init__(output_folder)
        self.regbank = regbank
        self.register_width: int = regbank.data_width // 8
        self._register_c_type: str = 'uint32_t'

    @classmethod
    def indent_code(cls, code: str, levels: int, spaces_per_level: int = -1) -> str:
        if spaces_per_level == -1:
            spaces_per_level = cls.indention_spaces
        indent_spaces = ' ' * (levels * spaces_per_level)
        return_string = ""
        for line in code.splitlines():
            return_string += f"{indent_spaces}{line}\n"
        return return_string

    def _generate_address_comment(self, start_address: int, end_address: int = 0) -> str:
        if (end_address == 0) or (end_address - start_address == 0):
            return f"// 0x{start_address:04X}\n"
        else:
            word_width = (math.ceil(self.regbank.data_width / 8))
            return f"// 0x{start_address:04X} - 0x{end_address-word_width:04X}\n"

    def _generate_reserved_string(self, start_address: int, end_address: int) -> str:
        output_string = self._generate_address_comment(start_address, end_address)
        block_size = end_address - start_address
        c_type = self._register_c_type
        output_string += f"{c_type} __res_{start_address:04X}"
        if block_size > 0:
            block_size_words = block_size // (math.ceil(self.regbank.data_width / 8))
            output_string += f"[{block_size_words}]"
        output_string += ";\n"
        return output_string

    def _generate_field_string(self, field: lowlevhw.Field, register_name : str) -> str:
        c_type = self._register_c_type
        if field.is_filler():
            return f"{c_type} __res_{register_name}_{field.name[6:]}:{field.width};"
        else:
            return f"{c_type} {register_name}_{field.name}:{field.width};"

    def _generate_split_register_string(self, register: lowlevhw.Register) -> str:
        return_string = "union {\n"
        return_string += self.indent_code(self._generate_simple_register_string(register), 1)
        return_string += self.indent_code("struct {", 1)
        field_string = ""
        for field in register.get_fields_incl_reserved():
            field_string += self._generate_field_string(field, register.name)
            field_string += '\n'
        return_string += self.indent_code(field_string, 2)
        return_string += self.indent_code("};", 1)
        return_string += "};\n"
        return return_string

    def _generate_simple_register_string(self, register: lowlevhw.Register) -> str:
        c_type = self._register_c_type
        name = register.name
        return f'{c_type} {name};\n'

    def _generate_register_string(self, address: int, register: lowlevhw.Register) -> str:
        return_string = self._generate_address_comment(address)
        if register.has_fields():
            return_string += self._generate_split_register_string(register)
        else:
            return_string += self._generate_simple_register_string(register)
        return return_string

    def generate_string(self) -> str:
        output_string = f"typedef struct {self.regbank.name} {{\n"
        next_address = 0x0
        for address in self.regbank.get_address_list():
            if address != next_address:
                reserved_string = self._generate_reserved_string(next_address, address)
                output_string += self.indent_code(reserved_string, 1)
            register = self.regbank.get_register_at_address(address)
            register_string = self._generate_register_string(address, register)
            output_string += self.indent_code(register_string, 1)
            next_address = address + self.register_width
        output_string += f"}} {self.regbank.name}_t;"
        return output_string

    def generate(self) -> None:
        # Check if register map does not use element addressing
        doc_string = self.HEADER
        guard_name = f"{self.regbank.name.upper()}_H_"
        doc_string += f"#ifndef {guard_name}\n"
        doc_string += f"#define {guard_name}\n\n"
        doc_string += self.generate_string()
        doc_string += "\n\n#endif\n"
        print(doc_string)

        file_name = self.regbank.name
        file = f"{self.output_folder}/{file_name}.h"
        try:
            os.makedirs(self.output_folder)
        except FileExistsError:
            pass
        f = open(file, 'w+')
        f.write(doc_string)
        f.close()


class RegisterSpaceCHeaderGen(lowlevhw.Generator):

    HEADER="""\
// Automatically generated register space file

#include <stdint.h>

"""

    def __init__(self, address_space: lowlevhw.AddressSpace, output_folder: str) -> None:
        super().__init__(output_folder)
        self.address_space = address_space

    def generate_string(self) -> str:
        return_string = ""
        for regmap in self.address_space.get_elements():
            base_address = regmap.base_address
            name = regmap.name
            return_string += f"static const uint32_t {name}__base=0x{base_address:08X};\n"
        return return_string

    def generate(self) -> None:
        doc_string = self.HEADER
        guard_name = f"{self.address_space.name.upper()}_H_"
        doc_string += f"#ifndef {guard_name}\n"
        doc_string += f"#define {guard_name}\n\n"
        doc_string += self.generate_string()
        doc_string += "\n#endif\n"
        print(doc_string)
        file = f'{self.output_folder}/{self.address_space.name}_map.h'
        try:
            os.makedirs(self.output_folder)
        except FileExistsError:
            pass
        f = open(file, 'w+')
        f.write(doc_string)
        f.close()


class DeviceCHeaderGen(lowlevhw.Generator):

    def __init__(self, device: lowlevhw.DeviceDefinition, output_folder: str,
                 bank_generator_class : type = RegisterBankCHeaderGen,
                 map_generator_class  : type = RegisterSpaceCHeaderGen) -> None:
        super().__init__(output_folder)

        self.device = device

        self.bank_generator_class = bank_generator_class
        self.map_generator_class = map_generator_class


    def __generate_recursive(self, element, generated_register_banks):

        if isinstance(element.definition, lowlevhw.RegisterBank):
            register_bank = element.definition

            if (register_bank.name not in generated_register_banks):
                bank_generator = self.bank_generator_class(register_bank, self.output_folder)
                bank_generator.generate()
                generated_register_banks.append(register_bank.name)

        elif isinstance(element.definition, lowlevhw.AddressSpace):
            for sub_element in element.definition.get_elements():
                self.__generate_recursive(sub_element, generated_register_banks)

            map_generator = self.map_generator_class(element.definition, self.output_folder)
            map_generator.generate()

    def generate(self) -> None:
        generated_register_banks = []
        for address_space in self.device.get_address_spaces():
            register_map_generated = False
            for element in address_space.get_elements():
                self.__generate_recursive(element, generated_register_banks)

            map_generator = self.map_generator_class(address_space, self.output_folder)
            map_generator.generate()
