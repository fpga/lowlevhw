#------------------------------------------------------------------------------
#  File        : memorymap_text_doc.py
#  Brief       : Generates a text documentation of a memory map
#------------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2021-10-21
#------------------------------------------------------------------------------
#  Description :
#------------------------------------------------------------------------------
#  Copyright (C) 2021 European Spallation Source ERIC
#------------------------------------------------------------------------------

import os

from string import Template

import lowlevhw


class AXIRegbankVHDLslvGen(lowlevhw.GeneratorTemplate):

    REGISTER_NAME_LENGTH = 20
    FIELD_NAME_LENGTH = 20

    def __init__(self, regbank, output_folder) -> None:
        script_location = os.path.dirname(__file__)
        self.template_folder = script_location + '/axi_regbank_templates'
        register_bank_files = [
            'axi4_pkg.vhdl',
            'axi_read_ctrl.vho',
            'axi_write_ctrl.vho',
            'field_core.vhdl',
            'register_bank_address_decoder.vho',
            'register_bank_axi.vho',
            'register_bank.cfg',
            'register_bank.cocotb.mk',
            'register_bank_config_pkg.vho',
            'register_bank_core.vho',
            'register_bank_functions_pkg.vhdl',
            'register_bank_rd_encoder.vho',
            'register_bank_rd_interface.vho',
            'register_types_pkg.vho',
            'register_bank_shadowing.vho',
            'register_bank.vho',
            'register_bank_wr_interface.vho'
            ]
        super().__init__(output_folder, self.template_folder, register_bank_files)
        self.regbank = regbank

    def get_full_field_name(self, field):
        return field.name

    def get_field_rec_access(self, field):
        return field.name

    def get_register_types(self):
        return ""

    def get_register_convert_functions(self):
        return ""

    def get_register_convert_headers(self):
        return ""

    def get_register_constants(self):
        if self.regbank._base_address != 0:
            base_address = self.regbank.base_address
        else:
            base_address = 0
        addresses = list(self.regbank.register_map.keys())
        ordered_addresses = sorted(addresses)
        vhdl_code = ""
        for address in ordered_addresses:
            register = self.regbank.register_map[address]
            register_const_name = f"{register.name}_ADDR"
            register_const_name = register_const_name.upper()
            register_name_str = f"{register_const_name:<{AXIRegbankVHDLslvGen.REGISTER_NAME_LENGTH}}"
            address_hex = f"{base_address+address:0>8X}"
            vhdl_code += f"  constant {register_name_str} : integer := 16#{address_hex}#;\n"
        return vhdl_code

    def get_templated(self, filename, snippets):
        template_file = f'{self.template_folder}/{filename}'
        with open(template_file, 'r') as f:
            template_file = f.read()
        return Template(template_file).safe_substitute(snippets)

    def get_field_implementation(self, field, vhdl_library):
        template_snippets = dict()
        template_snippets['VHDL_LIBRARY'] = vhdl_library
        template_snippets['FIELD_WIDTH'] = field.width
        template_snippets['FIELD_NAME'] = self.get_full_field_name(field)
        if field.is_writable():
            template_snippets['WRITE_EN'] = f"write_en_i.{template_snippets['FIELD_NAME']}"
        else:
            template_snippets['WRITE_EN'] = "(others => '0')"
        field_rec_access = self.get_field_rec_access(field)
        if field.writes_logic():
            template_snippets['LOGIC_READ'] = f"      => logic_data_o.{field_rec_access}"
        else:
            template_snippets['LOGIC_READ'] = "      => open"
        if field.reads_logic():
            template_snippets['LOGIC_RETURN'] = f"    => logic_return_i.{field_rec_access}"
        else:
            template_snippets['LOGIC_RETURN'] = "    => (others => '0')"
        # Vivado has problems with unsigned width 32 and sign bit set it seems (integer overflow error)
        if field.width == 32:
            template_snippets['RESET_VALUE'] = f'x"{field.reset_value:08X}\"'
        else:
            template_snippets['RESET_VALUE'] = f"std_logic_vector(to_unsigned(16#{field.reset_value:X}#, {field.width}))"
        return self.get_templated('register_field.vho', template_snippets)

    def get_register_write_en(self, register):
        if register.is_writable():
            vhdl_code = f"register_write_en_o.{register.name:<{AXIRegbankVHDLslvGen.REGISTER_NAME_LENGTH}} <= '1';\n"
        else:
            vhdl_code = ""
        return vhdl_code

    def get_shdwgrp_register_record(self, shadow_group):
        vhdl_code = f"type {shadow_group.name}_shadow_group_t is record\n"
        if len(shadow_group.hw_fields) > 1:
            for hw_field in shadow_group.hw_fields:
                vhdl_code += f"     {hw_field.name:<{AXIRegbankVHDLslvGen.REGISTER_NAME_LENGTH}} : std_logic_vector({hw_field.width-1} downto 0);\n"
        else:
            vhdl_code += f"     {'none':<{AXIRegbankVHDLslvGen.REGISTER_NAME_LENGTH}} : std_logic_vector(0 downto 0);\n"
        vhdl_code += "  end record;"
        return vhdl_code

    def get_shdwgrp_instance(self, shadow_group):
        template_snippets = dict()
        template_snippets['SHADOW_GRP_NAME'] = shadow_group.name
        shdwgrp_sig_assignments = ""
        for hw_field in shadow_group.hw_fields:
            field_rec_element = self.get_field_rec_access(hw_field)
            shdwgrp_sig_assignments += f"          logic_data_o.{field_rec_element} <= logic_data_i.{field_rec_element};\n"
        shdwgrp_sig_assignments = shdwgrp_sig_assignments[:-1]
        template_snippets['SHADOW_SIGNAL_ASSIGNMENTS'] = shdwgrp_sig_assignments
        return self.get_templated('shadow_group.vho', template_snippets)

    def get_vhdl_register_en_record(self):
        vhdl_code = ""
        for register in self.regbank.get_registers():
            if register.is_writable():
                vhdl_code += f"     {register.name:<{AXIRegbankVHDLslvGen.REGISTER_NAME_LENGTH}} : std_logic;\n"
        if vhdl_code != "":
            return vhdl_code
        else:
            return "    none : std_logic;\n"

    def get_vhdl_field_en_record(self):
        vhdl_code = ""
        for hw_field in self.regbank.get_unique_hw_fields():
            if hw_field.is_writable():
                field_name = self.get_full_field_name(hw_field)
                vhdl_code += f"     {field_name:<{AXIRegbankVHDLslvGen.REGISTER_NAME_LENGTH+AXIRegbankVHDLslvGen.FIELD_NAME_LENGTH}} : std_logic_vector({hw_field.width-1} downto 0);\n"
        if vhdl_code != "":
            return vhdl_code
        else:
            return "    none : std_logic;\n"

    def get_vhdl_field_data_record(self):
        vhdl_code = ""
        for hw_field in self.regbank.get_unique_hw_fields():
            field_name = self.get_full_field_name(hw_field)
            field_name_str = f"{field_name:<{AXIRegbankVHDLslvGen.REGISTER_NAME_LENGTH+AXIRegbankVHDLslvGen.FIELD_NAME_LENGTH}}"
            vhdl_code += f"     {field_name_str} : std_logic_vector({hw_field.width-1} downto 0);\n"
        return vhdl_code

    def get_vhdl_transfer_shadow_group_record(self):
        if len(self.regbank.shadow_groups) > 0:
            vhdl_code = ""
            for shadow_group in self.regbank.shadow_groups:
                vhdl_code += f"    {shadow_group.name} : std_logic;\n"
            return vhdl_code
        else:
            return "    none : std_logic;\n"

    def get_vhdl_shadow_group_records(self):
        vhdl_code = ""
        for shadow_group in self.regbank.shadow_groups:
            vhdl_code += self.get_shdwgrp_register_record(shadow_group)
            vhdl_code += "\n"
        return vhdl_code

    def get_vhdl_bus_read_record(self):
        addresses = list(self.regbank.register_map.keys())
        ordered_addresses = sorted(addresses)
        vhdl_code = ""
        for address in ordered_addresses:
            register = self.regbank.register_map[address]
            if register.is_readable():
                vhdl_code += f"     {register.name:<{AXIRegbankVHDLslvGen.REGISTER_NAME_LENGTH}} : std_logic_vector(REGISTER_WIDTH-1 downto 0);\n"
        return vhdl_code

    def get_vhdl_logic_record(self, selector):
        vhdl_code = ""
        implemented_registers = set()
        for field in self.regbank.get_unique_hw_fields():
            selector_method = getattr(field, selector)
            if selector_method():
              field_name_str = f"{field.name:<{AXIRegbankVHDLslvGen.FIELD_NAME_LENGTH}}"
              vhdl_code += f"     {field_name_str} : std_logic_vector({field.width-1} downto 0);\n"
        if vhdl_code == "":
            vhdl_code = "    none : std_logic;\n"
        return vhdl_code

    def get_vhdl_logic_read_data_record(self):
        return self.get_vhdl_logic_record('writes_logic')

    def get_vhdl_logic_return_record(self):
        return self.get_vhdl_logic_record('reads_logic')

    def get_vhdl_reg_to_bus_select(self):
        vhdl_code = ""
        addresses = list(self.regbank.register_map.keys())
        ordered_addresses = sorted(addresses)
        for address in ordered_addresses:
            register = self.regbank.register_map[address]
            if register.is_readable():
                register_addr_str = f"{register.name.upper()}_ADDR"
                vhdl_code += f"              when {register_addr_str} =>\n"
                vhdl_code += f"                bus_data_o <= register_data_i.{register.name};\n"
        return vhdl_code

    def get_vhdl_reg_write_en_select(self):
        vhdl_code = ""
        addresses = list(self.regbank.register_map.keys())
        ordered_addresses = sorted(addresses)
        for address in ordered_addresses:
            register = self.regbank.register_map[address]
            register_specific_vhdl = ""
            register_specific_vhdl += self.get_register_write_en(register)
            if register_specific_vhdl != "":
                register_addr_str = f"{register.name.upper()}_ADDR"
                vhdl_code += f"              when {register_addr_str} =>\n"
                vhdl_code += f"                {register_specific_vhdl}"
        return vhdl_code

    def get_vhdl_write_if_en_sigs(self):
        vhdl_code = ""
        for hw_field in self.regbank.get_unique_hw_fields():
            elements = len(hw_field.get_linked_fields()) + 1
            field_name = self.get_full_field_name(hw_field)
            vhdl_code += f"    signal {field_name}_write_en : std_logic_vector({elements-1} downto 0);\n"
        return vhdl_code

    def get_vhdl_write_if_cores(self):
        vhdl_code = ""
        for hw_field in self.regbank.get_unique_hw_fields():
            template_snippets = dict()
            if hw_field.is_writable():
                offsets = []
                interface_modes = []
                write_ens = []
                full_field_name = self.get_full_field_name(hw_field)
                all_interfaces = hw_field.get_linked_fields()
                all_interfaces.append(hw_field)
                for field_index, field in enumerate(all_interfaces):
                    offsets.append(f"{field_index} => {field.offset}")
                    if not field.is_writable():
                        interface_modes.append(f"{field_index} => {'NO_WRITE'}")
                    else:
                        write_mode = field.get_write_modes()
                        # Check if too many write modes
                        mode_name = lowlevhw.get_field_mode_name(write_mode.pop())
                        interface_modes.append(f"{field_index} => {mode_name}")
                    register = self.regbank.get_register_of_field(field)
                    write_ens.append(f"register_write_en_i.{register.name}")
                template_snippets['OFFSETS'] = ", ".join(offsets)
                template_snippets['INTERFACE_MODES'] = ", ".join(interface_modes)
                template_snippets['FIELD_NAME'] = full_field_name
                if len(hw_field.get_linked_fields()) == 0:
                    register_wr_en_sig = f"register_write_en_i.{register.name}"
                    template_snippets['CONCAT_WRITE_ENS'] = f"(0 => {register_wr_en_sig})"
                else:
                    write_ens.reverse()
                    template_snippets['CONCAT_WRITE_ENS'] = " & ".join(write_ens)
                vhdl_code += self.get_templated('write_interface_inst.vho', template_snippets)
        return vhdl_code

    def get_vhdl_read_if_cores(self):
        vhdl_code = ""
        for register in self.regbank.get_registers():
            if register.is_readable():
                for field in register.get_fields():
                    template_snippets = dict()
                    read_mode = field.get_read_modes()
                    if len(read_mode) <= 1:
                        if len(read_mode) == 0:
                            template_snippets['TYPE'] = "none"
                        elif read_mode == set(['R']):
                            template_snippets['TYPE'] = "logic"
                        elif read_mode == set(['B']):
                            template_snippets['TYPE'] = "readback"
                        else:
                            raise ValueException("Unsupported read mode for VHDL generation.")
                    else:
                        raise ValueException("A field cannot have multiple read modes.")
                    full_field_name = self.get_full_field_name(field.hw_field)
                    template_snippets['WIDTH'] = field.width
                    template_snippets['OFFSET'] = field.offset
                    template_snippets['FIELD_NAME'] = full_field_name
                    template_snippets['REGISTER_NAME'] = register.name
                    vhdl_code += self.get_templated('read_interface_inst.vho', template_snippets)
        return vhdl_code

    def get_vhdl_field_instances(self):
        vhdl_code = ""
        for hw_field in self.regbank.get_unique_hw_fields():
            vhdl_code += self.get_field_implementation(hw_field, self.regbank.name)
        return vhdl_code

    def get_shadow_group_instances(self):
        vhdl_code = ""
        for shadow_group in self.regbank.shadow_groups:
            vhdl_code += self.get_shdwgrp_instance(shadow_group)
        return vhdl_code

    def get_vhdl_non_shadowed_sigs(self):
        vhdl_code = ""
        for hw_field in self.regbank.get_unique_hw_fields():
            if hw_field.writes_logic() and not hw_field.is_shadowed():
                field_record = self.get_field_rec_access(hw_field)
                vhdl_code += f"  logic_data_o.{field_record} <= logic_data_i.{field_record};\n"
        if vhdl_code != "":
            return vhdl_code
        else:
            return "  logic_data_o <= logic_data_i;\n"

    def fill_template_snippets(self):
        self.template_snippets['REGBANK_LIBRARY'] = self.regbank.name
        self.template_snippets['ADDRESS_WIDTH'] = self.regbank.address_width
        self.template_snippets['REGISTER_WIDTH'] = self.regbank.data_width
        self.template_snippets['WSTRB_WIDTH'] = self.regbank.wstrb_width
        self.template_snippets['ENABLE_ERROR_DECODING'] = str(self.regbank.enable_error_decoding).lower()
        self.template_snippets['ELEMENT_ADDRESSING'] = str(self.regbank.element_addressing).lower()
        self.template_snippets['REGISTER_WRITE_EN_RECORD'] = self.get_vhdl_register_en_record()
        self.template_snippets['FIELD_WRITE_EN_RECORD'] = self.get_vhdl_field_en_record()
        self.template_snippets['FIELD_DATA_RECORD'] = self.get_vhdl_field_data_record()
        self.template_snippets['TRANSFER_SHADOW_GROUP_RECORD'] = self.get_vhdl_transfer_shadow_group_record()
        self.template_snippets['SHADOW_GROUP_RECORDS'] = self.get_vhdl_shadow_group_records()
        self.template_snippets['BUS_READ_RECORD'] = self.get_vhdl_bus_read_record()
        self.template_snippets['LOGIC_READ_DATA_RECORD'] = self.get_vhdl_logic_read_data_record()
        self.template_snippets['LOGIC_RETURN_RECORD'] = self.get_vhdl_logic_return_record()
        self.template_snippets['REGISTER_WRITE_EN_SELECT'] = self.get_vhdl_reg_write_en_select()
        self.template_snippets['WRITE_INTERFACE_EN_SIGS'] = self.get_vhdl_write_if_en_sigs()
        self.template_snippets['WRITE_INTERFACE_CORES'] = self.get_vhdl_write_if_cores()
        self.template_snippets['FIELD_INSTANCES'] = self.get_vhdl_field_instances()
        self.template_snippets['SHADOW_GROUP_INSTANCES'] = self.get_shadow_group_instances()
        self.template_snippets['NON_SHADOWED_SIGNALS'] = self.get_vhdl_non_shadowed_sigs()
        self.template_snippets['READ_INTERFACE_CORES'] = self.get_vhdl_read_if_cores()
        self.template_snippets['REGISTER_TO_BUS_SELECT'] = self.get_vhdl_reg_to_bus_select()
        self.template_snippets['REGISTER_CONSTANTS'] = self.get_register_constants()
        self.template_snippets['REGISTER_TYPE_RECORDS'] = self.get_register_types();
        self.template_snippets['REGISTER_TYPE_FUNC_DECLARATIONS'] = self.get_register_convert_headers();
        self.template_snippets['REGISTER_TYPE_FUNCTIONS'] = self.get_register_convert_functions();


class AXIRegbankVHDLRecordGen(AXIRegbankVHDLslvGen):

    def __init__(self, regbank, output_folder) -> None:
        super().__init__(regbank, output_folder)

    def get_field_implementation(self, field, vhdl_library):
        template_snippets = dict()
        template_snippets['VHDL_LIBRARY'] = vhdl_library
        template_snippets['FIELD_WIDTH'] = field.width
        template_snippets['FIELD_NAME'] = self.get_full_field_name(field)
        if field.is_writable():
            template_snippets['WRITE_EN'] = f"write_en_i.{template_snippets['FIELD_NAME']}"
        else:
            template_snippets['WRITE_EN'] = "(others => '0')"
        field_rec_access = self.get_field_rec_access(field)
        if field.writes_logic():
            if field.width == 1:
                template_snippets['LOGIC_READ'] = f"(0)   => logic_data_o.{field_rec_access}"
            else:
                template_snippets['LOGIC_READ'] = f"      => logic_data_o.{field_rec_access}"
        else:
            template_snippets['LOGIC_READ'] = "      => open"
        if field.reads_logic():
            if field.width == 1:
                template_snippets['LOGIC_RETURN'] = f"(0) => logic_return_i.{field_rec_access}"
            else:
                template_snippets['LOGIC_RETURN'] = f"    => logic_return_i.{field_rec_access}"
        else:
            template_snippets['LOGIC_RETURN'] = "    => (others => '0')"
        # Vivado has problems with unsigned width 32 and sign bit set it seems (integer overflow error)
        if field.width == 32:
            template_snippets['RESET_VALUE'] = f'x"{field.reset_value:08X}\"'
        else:
            template_snippets['RESET_VALUE'] = f"std_logic_vector(to_unsigned(16#{field.reset_value:X}#, {field.width}))"
        return self.get_templated('register_field.vho', template_snippets)


    def get_register_type(self, register):
        if register.has_fields():
            return f"reg_{register.name}_t"
        else:
            return f"std_logic_vector({register.width-1} downto 0)"

    def get_field_type(self, field):
        if field.width == 1:
            return "std_logic"
        else:
            return f"std_logic_vector({field.width-1} downto 0)"

    def get_field_desc_string(self, field, separator):
        register = self.regbank.get_register_of_field(field)
        if register.has_fields():
            return f"{register.name}{separator}{field.name}"
        else:
            return f"{register.name}"

    def get_full_field_name(self, field):
        return self.get_field_desc_string(field, '_x_')

    def get_field_rec_access(self, field):
        return self.get_field_desc_string(field, '.')

    def get_register_types(self):
        vhdl_code = ""
        for register in self.regbank.get_registers():
            if register.has_fields():
                vhdl_code += f"  type reg_{register.name}_t is record\n"
                for field in register.get_fields():
                    field_type = self.get_field_type(field)
                    vhdl_code += f"    {field.name:<{AXIRegbankVHDLslvGen.FIELD_NAME_LENGTH}} : {field_type};\n"
                vhdl_code += "  end record;\n"
                vhdl_code += "\n"
        return vhdl_code

    def get_field_record_def(self, field, implemented_registers):
        vhdl_code = ""
        register = self.regbank.get_register_of_field(field)
        if not (register.name in implemented_registers):
            implemented_registers.update([register.name])
            register_type = self.get_register_type(register)
            register_name_str = f"{register.name:<{AXIRegbankVHDLslvGen.REGISTER_NAME_LENGTH}}"
            vhdl_code += f"     {register_name_str} : {register_type};\n"
        return vhdl_code

    def get_vhdl_logic_record(self, selector):
        vhdl_code = ""
        implemented_registers = set()
        for field in self.regbank.get_unique_hw_fields():
            selector_method = getattr(field, selector)
            if selector_method():
                vhdl_code += self.get_field_record_def(field, implemented_registers)
        if vhdl_code == "":
            vhdl_code = "    none : std_logic;\n"
        return vhdl_code

    def get_register_convert_headers(self):
        vhdl_code = ""
        for register in self.regbank.get_registers():
            if register.has_fields():
                if register.is_writable():
                    vhdl_code += f"  function {register.name}_to_slv (reg : reg_{register.name}_t) return std_logic_vector;\n"
                if register.is_readable():
                    vhdl_code += f"  function {register.name}_from_slv (reg_vec : std_logic_vector({register.width-1} downto 0)) return reg_{register.name}_t;\n"

        return vhdl_code

    def get_register_convert_to_slv(self, register):
        vhdl_code = f"  function {register.name}_to_slv (reg : reg_{register.name}_t) return std_logic_vector is\n"
        vhdl_code += f"    variable reg_vec : std_logic_vector({register.width-1} downto 0);\n"
        vhdl_code += f"  begin\n"
        vhdl_code += f"    reg_vec := (others => '0');\n"
        for field in register.get_fields():
            if (self.get_field_type(field) == "std_logic"):
                vhdl_code += f"    reg_vec({field.offset}) := reg.{field.name};\n"
            else:
                vhdl_code += f"    reg_vec({field.width + field.offset - 1} downto {field.offset}) := reg.{field.name};\n"
        vhdl_code += f"    return reg_vec;\n"
        vhdl_code += f"  end;\n\n"
        return vhdl_code

    def get_register_convert_from_slv(self, register):
        vhdl_code = f"  function {register.name}_from_slv (reg_vec : std_logic_vector({register.width-1} downto 0)) return reg_{register.name}_t is\n"
        vhdl_code += f"    variable reg : reg_{register.name}_t;\n"
        vhdl_code += f"  begin\n"
        for field in register.get_fields():
            if (self.get_field_type(field) == "std_logic"):
                vhdl_code += f"    reg.{field.name} := reg_vec({field.offset});\n"
            else:
                vhdl_code += f"    reg.{field.name} := reg_vec({field.width + field.offset - 1} downto {field.offset});\n"
        vhdl_code += f"    return reg;\n"
        vhdl_code += f"  end;\n\n"
        return vhdl_code

    def get_register_convert_functions(self):
        vhdl_code = ""
        for register in self.regbank.get_registers():
            if register.has_fields():
                if register.is_writable():
                    vhdl_code += self.get_register_convert_to_slv(register)
                if register.is_readable():
                    vhdl_code += self.get_register_convert_from_slv(register)

        return vhdl_code
