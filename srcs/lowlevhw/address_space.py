# -----------------------------------------------------------------------------
#  File        : address_space.py
#  Brief       :
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2023-09-29
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import operator
import typing

import lowlevhw


DEFAULT_TRANSPARENCY = False


class AddressSpaceElement:

    def __init__(self, definition, base: int, name: str, transparent: bool = DEFAULT_TRANSPARENCY) -> None:
        self.name = name
        self.base_address = base
        self.transparent = transparent
        self.definition = definition

    def __lt__(self, other):
        return self.base_address < other.base_address

    def __str__(self) -> str:
        output_string  = f"Address Space Element\n"
        output_string += f"  Name        : {self.name}\n"
        output_string += f"  Base addres : {hex(self.base_address)}\n"
        output_string += f"  Transparent : {self.transparent}\n"
        type_string_full = str(type(self.definition))
        type_end = type_string_full.rfind('.')
        type_string = type_string_full[type_end+1:-2]
        output_string += f"  Element     : {type_string} - {self.definition.name}"
        return output_string


class AddressSpace:

    TRANSPARENCY_MARKER = '*'

    def __init__(self, name: str) -> None:
        self.name = name
        self._elements: typing.Dict[str, AddressSpaceElement] = {}

    def __str__(self) -> str:
        return self.get_string()

    def get_string(self, abs_addresses: bool = True, base_address: int = 0x0000):
        return_string = f"Address Space <{self.name}>"
        for element in self._elements.values():
            if abs_addresses:
                address = base_address + element.base_address
            else:
                address = element.base_address
            return_string += '\n'
            tmarker = ''
            if element.transparent:
                tmarker = self.TRANSPARENCY_MARKER
            return_string += f"  0x{address:08x} : {tmarker}{element.name}{tmarker}"
            if isinstance(element.definition, AddressSpace):
                sub_string = element.definition.get_string(abs_addresses=abs_addresses, base_address=address)
                sub_string = '  '.join(sub_string.splitlines(True))
                return_string += f" - {sub_string}"
            if isinstance(element.definition, lowlevhw.RegisterBank):
                return_string += f" - {element.definition.get_string()} \n"
        if not self._elements:
            return_string += '\n'
            return_string += "  NO ELEMENTS IN ADDRESS SPACE"
        return return_string

    def add_element(self, element_definition, base: int = 0, name: str = '',
                    transparent: bool = DEFAULT_TRANSPARENCY) -> AddressSpaceElement:
        if not name:
            name = element_definition.name
        element = AddressSpaceElement(element_definition, base, name, transparent)
        self._elements[name] = element
        return element

    def __getitem__(self, location: str):
        local_location, sub_location = lowlevhw.split_element_location(location)
        matching_elements = []
        if location == '':
            return self
        for element in self.get_elements():
            if element.transparent:
                try:
                    matching_elements.append(element.definition[local_location])
                except KeyError:
                    continue
            elif element.name.lower() == local_location.lower():
                matching_elements.append(element.definition)
        if len(matching_elements) == 1:
            element = matching_elements[0]
            if sub_location == '':
                return element
            return element[sub_location]
        elif len(matching_elements) == 0:
            raise KeyError(f"Element <{local_location}> not found in address space {self.name}")
        else:
            raise KeyError(f"Multiple elements with <{local_location}> found in device definition")

    def get_element(self, name: str):
        local_name, sub_name = lowlevhw.split_element_location(name)
        if local_name[0] == '*' and local_name[-1] == '*':
            local_name = local_name[1:-1]
        local_element = self._elements[local_name]
        if not sub_name:
            return local_element.definition
        return local_element.definition.get_element(sub_name)

    def get_elements(self) -> typing.List[AddressSpaceElement]:
        elements = self._elements.values()
        return list(sorted(elements, key=operator.attrgetter('base_address')))

    def get_registers(self) -> typing.List[str]:
        registers = []
        for element in self.get_elements():
            if isinstance(element.definition, lowlevhw.RegisterBank):
                new_registers = element.definition.get_registers()
                new_registers = [register.name for register in new_registers]
            else:
                new_registers = element.definition.get_registers()
            if element.transparent:
                trans_mark = self.TRANSPARENCY_MARKER
            else:
                trans_mark = ''
            element_name = f'{trans_mark}{element.name}{trans_mark}'
            registers.extend([f'{element_name}:{register}' for register in new_registers])
        return registers

    def get_elements_sorted_by_address(self) -> typing.List[AddressSpaceElement]:
        element_list = self.get_elements()
        element_list.sort()
        return element_list

    def get_address(self, location: str) -> int:
        full_path = self.get_path(location)
        if len(full_path) > 1:
            raise KeyError(f"Could not determine exact, unique path to: {location}")
        if len(full_path) == 1:
            full_path = full_path[0]
        else: # Legacy requirement
            full_path = location
        (local_location, sub_location) = lowlevhw.split_element_location(full_path)
        space_element = self._elements[local_location]
        address = space_element.base_address
        if sub_location:
            address += space_element.definition.get_address(sub_location)
        return address

    def get_element_at_address(self, address: int) -> AddressSpaceElement:
        print("AddressSpace:get_element_at_address(): Not yet implemented, underlying elements must support address range")

    def get_path(self, location: str) -> typing.List[str]:
        possible_paths = []
        local_location, sub_location = lowlevhw.split_element_location(location)
        for element in self._elements.values():
            if element.transparent:
                try:
                    paths = element.definition.get_path(location)
                except KeyError:
                    continue
                for path in paths:
                    possible_paths.append(element.name + ':' + path)
            elif element.name == local_location:
                if sub_location:
                    try:
                        sub_paths = element.definition.get_path(sub_location)
                    except KeyError:
                        continue
                    for sub_path in sub_paths:
                        possible_paths.append(local_location + ':' + sub_path)
                else:
                    possible_paths.append(local_location)
        return possible_paths
