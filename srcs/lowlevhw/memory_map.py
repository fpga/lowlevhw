# -----------------------------------------------------------------------------
#  File        : memory_map.py
#  Brief       : Class representing the memory map of a hardware device.
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2019-02-01
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2020 European Spallation Source ERIC
# -----------------------------------------------------------------------------

from abc import ABCMeta, abstractmethod
from collections.abc import Iterable
import struct
import typing

import lowlevhw
from lowlevhw.helpers import float_to_fixed
from lowlevhw.helpers import fixed_to_float


class MemoryElementConverter:
    __metaclass__ = ABCMeta

    @abstractmethod
    def to_raw(self, element):
        pass

    @abstractmethod
    def from_raw(self, raw_element):
        pass


class RawFormat(MemoryElementConverter):
    """
    Class to represent raw format memory/register elements.

    An element is exactly one byte, i.e. the range is 0-256.
    """

    def to_raw(self, element) -> bytearray:
        return bytearray([element])

    def from_raw(self, raw_element):
        if isinstance(raw_element, Iterable):
            if len(raw_element) == 1:
                return raw_element[0]
            return list(raw_element)
        return raw_element


class IntegerFormat(MemoryElementConverter):

    def __init__(self, width: int, signed=True, byteorder='little') -> None:
        self.width = width
        self.signed = signed
        self.byteorder = byteorder

    def to_raw(self, element, signed=None):
        if signed is None:
            signed = self.signed
        raw_element = element.to_bytes(self.width, byteorder=self.byteorder, signed=signed)
        return raw_element

    def from_raw(self, raw_element):
        element = int.from_bytes(raw_element, byteorder=self.byteorder, signed=self.signed)
        return element


class FixedPointFormat(IntegerFormat):

    def __init__(self, int_bits, frac_bits, signed=True):
        width = (int_bits + frac_bits + 7) // 8
        super().__init__(width, signed=signed, byteorder='little')
        self.int_bits = int_bits
        self.frac_bits = frac_bits

    def to_raw(self, element):
        int_value = float_to_fixed(element, self.int_bits, self.frac_bits, signed=False)
        raw_element = super().to_raw(int_value)
        return raw_element

    def from_raw(self, raw_element):
        # int_element = super().from_raw(raw_element)
        int_element = int.from_bytes(raw_element, byteorder=self.byteorder, signed=self.signed)
        element = fixed_to_float(int_element, self.int_bits, self.frac_bits, self.signed)
        return element

class FloatFormat(MemoryElementConverter):

    def to_raw(self, element) -> bytearray:
        raw_element = bytearray(struct.pack('f', element))
        return raw_element

    def from_raw(self, raw_element):
        float_value = struct.unpack('f', bytearray(raw_element))[0]
        return float_value

class MemoryOrganization:
    __metaclass__ = ABCMeta

    def __init__(self, element_converter=RawFormat()) -> None:
        self.element_converter = element_converter

    @abstractmethod
    def to_raw(self, elements) -> bytearray:
        pass

    @abstractmethod
    def from_raw(self, raw_data):
        pass


class RawMemory(MemoryOrganization):

    def to_raw(self, elements) -> bytearray:
        if isinstance(elements, int):
            elements = [elements]
        return bytearray(elements)

    def from_raw(self, raw_data):
        return [x for x in raw_data]


class MultiByteElements(MemoryOrganization):

    def __init__(self, element_len: int, element_converter=None) -> None:
        if element_converter is None:
            element_converter = RawFormat()
        super().__init__(element_converter)
        self.element_len = element_len

    def to_raw(self, elements) -> bytearray:
        raw_data = bytearray()
        for element in elements:
            raw_element = self.element_converter.to_raw(element)
            raw_data.extend(raw_element)
        return raw_data

    def from_raw(self, raw_data):
        raw_elements = [bytearray(item) for item in zip(*[iter(raw_data)]*self.element_len)]
        elements = [self.element_converter.from_raw(raw_element) for raw_element in raw_elements]
        return elements


class TupleElements(MemoryOrganization):

    def __init__(self, element_len, element_nr, element_converter=RawFormat()):
        super().__init__(element_converter)
        self.element_len = element_len
        self.element_nr = element_nr

    def to_raw(self, elements):
        raw_data = bytearray()
        for element in elements:
            for component in element:
                raw_component = self.element_converter.to_raw(component)
                raw_data.extend(raw_component)
        return raw_data

    def from_raw(self, raw_data):
        full_element_len = self.element_len * self.element_nr
        sample_nr = len(raw_data) // full_element_len
        element_nr = sample_nr * self.element_nr
        element_len = self.element_len
        raw_tuples_split = [raw_data[i*element_len:(i+1)*element_len] for i in range(element_nr)]
        element_conv = self.element_converter
        if isinstance(self.element_converter, Iterable):
            single_elements = []
            for i in range(self.element_nr):
                pos_elements = [raw_tuples_split[j] for j in range(i, len(raw_tuples_split), self.element_nr)]
                conv_elements = map(element_conv[i].from_raw, pos_elements)
                single_elements.append(tuple(conv_elements))
            elements = list(zip(*single_elements))
        else:
            elements = list(map(element_conv.from_raw, raw_tuples_split))
            it = [iter(elements)] * self.element_nr
            elements = list(zip(*it))
        return elements


class InterleavedElements(MemoryOrganization):

    def __init__(self, element_len, stream_nr, element_converter=RawFormat()):
        super().__init__(element_converter)
        self.element_len = element_len
        self.stream_nr = stream_nr

    def to_raw(self, *element_streams):
        shortest_list_len = len(element_streams[0][0])
        for stream in element_streams[0]:
            shortest_list_len = min(shortest_list_len, len(stream))
        raw_data = bytearray()
        for i in range(shortest_list_len):
            for j, stream in enumerate(element_streams[0]):
                if isinstance(self.element_converter, Iterable):
                    raw_element = self.element_converter[j].to_raw(stream[i])
                else:
                    raw_element = self.element_converter.to_raw(stream[i])
                raw_data.extend(raw_element)
        return raw_data

    def from_raw(self, raw_data):
        split_streams = [[] for x in range(self.stream_nr)]
        for block_pos in range(0, len(raw_data), self.element_len*self.stream_nr):
            for stream_id in range(self.stream_nr):
                element_pos = block_pos + stream_id*self.element_len
                raw_element = raw_data[element_pos:element_pos+self.element_len]
                if isinstance(self.element_converter, Iterable):
                    mem_element = self.element_converter[stream_id].from_raw(raw_element)
                else:
                    mem_element = self.element_converter.from_raw(raw_element)
                split_streams[stream_id].append(mem_element)
        return split_streams


class MemoryArea:
    __metaclass__ = ABCMeta

    def __init__(self, name, base=None, size=None, base_block_bytes=1, size_block_bytes=1, organization=RawMemory()):
        self.name = name
        self.organization = organization
        self.base = base
        self.base_is_register = None
        self.base_block_bytes = base_block_bytes
        self.size = size
        self.size_is_register = None
        self.size_block_bytes = size_block_bytes
        self.legacy = None


class MemoryAreaFixed(MemoryArea):

    def __init__(self, name, base_address, size, organization=RawMemory(), base_block_bytes=1, size_block_bytes=1):
        super().__init__(name, base_address, size, base_block_bytes, size_block_bytes, organization)
        self.base_is_register = False
        self.size_is_register = False


class MemoryAreaDynBase(MemoryArea):

    def __init__(self, name, base_register, size, organization=RawMemory(), base_block_bytes=1, size_block_bytes=1):
        super().__init__(name, base_register, size, base_block_bytes, size_block_bytes, organization)
        self.base_is_register = True
        self.size_is_register = False


class MemoryAreaDynSize(MemoryArea):

    def __init__(self, name, base_address, size_register, organization=RawMemory(), base_block_bytes=1, size_block_bytes=1):
        super().__init__(name, base_address, size_register, base_block_bytes, size_block_bytes, organization)
        self.base_is_register = False
        self.size_is_register = True


class MemoryAreaDynBaseDynSize(MemoryArea):

    def __init__(self, name, base_register, size_register, organization=RawMemory(), base_block_bytes=1, size_block_bytes=1, legacy=None):
        super().__init__(name, base_register, size_register, base_block_bytes, size_block_bytes, organization)
        self.base_is_register = True
        self.size_is_register = True
        self.legacy = legacy


class MemoryMap:

    def __init__(self, name: str):
        self.name = name
        self.mem_size = 0
        self.mem_areas = []
        self.aliases = []

    def __str__(self) -> str:
        return self.get_string()

    def get_string(self, abs_addresses: bool = True, base_address: int = 0x0000) -> str:
        ret_string = f"Memory map <{self.name}>\n"
        fixed_base_areas = list()
        non_fixed_areas = list()
        for area in self.mem_areas:
            if not area.base_is_register:
                fixed_base_areas.append( (area.base, area) )
            else:
                non_fixed_areas.append(area)
        fixed_areas_sorted = sorted(fixed_base_areas, key=lambda x: x[0])

        for area_tup in fixed_areas_sorted:
            area = area_tup[1]
            if abs_addresses:
                start_address = base_address + area.base
            else:
                start_address = area.base
            start_addr_str = f"0x{start_address:X}"
            if area.size is not None:
                separator = "-"
                if area.size_is_register:
                    end_addr_str = area.size
                else:
                    if abs_addresses:
                        end_address = base_address + area.base + area.size
                    else:
                        end_address = area.base + area.size
                    end_addr_str = f"0x{end_address:X}"
            else:
                separator = ' '
                end_addr_str = ''
            ret_string += f"  {start_addr_str} {separator} {end_addr_str} : {area.name}\n"
        if len(non_fixed_areas) > 0:
            ret_string += "\n"
            ret_string += "Dynamic memory areas:\n"
        for area in non_fixed_areas:
            ret_string += f"  {area.name} ({area.base}, {area.size})\n"
        return ret_string

    def __getitem__(self, key):
        if key == '':
            return self
        areas = []
        areas.append(self.get_area_by_name(key))
        areas.append(self.get_alias(key))
        areas = [x for x in areas if x is not None]
        if len(areas) == 1:
            return areas[0]
        elif len(areas) > 1:
            raise Exception(f"ERROR: Multiple memory areas could be connected to <{key}>")
        raise KeyError(f"Memory area <{key}> not found in memory map <{self.name}>")

    def get_element(self, name):
        return self.get_area_by_name(name)

    def get_registers(self):
        return []

    def get_path(self, location: str) -> typing.List[str]:
        _ = self[location]
        return [location]

    def add_area(self, area_definition):
        self.mem_areas.append(area_definition)

    def add_alias(self, name, area_name, alias_format=None):
        self.aliases.append(lowlevhw.ElementAlias(name, area_name, alias_format))

    def get_area_by_name(self, name):
        for area in self.mem_areas:
            if area.name.lower() == name.lower():
                return area
        return None

    def get_areas(self):
        area_list = []
        for area in self.mem_areas:
            area_list.append(area)
        return area_list

    def get_alias(self, name):
        matching_aliases = []
        for alias in self.aliases:
            if alias.name == name:
                matching_aliases.append(alias)
        if len(matching_aliases) == 1:
            return matching_aliases[0]
        elif len(matching_aliases) == 0:
            return None
        else:
            print(f"Memory map : Problem with finding alias {name}.")

    def append_memory_map(self, memmap):
        for new_area in memmap.get_areas():
            self.mem_areas.append(new_area)

    def check_validity(self):
        pass
