# -----------------------------------------------------------------------------
#  File        : device_definition.py
#  Brief       : Class combining all the information about a device
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2023-09-29
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

"""Defintion of a compelete device."""

import typing

import lowlevhw


NAME_SPLIT_CHARACTER = ':'


def split_element_location(location) -> typing.Tuple[str, str]:
    first_split_character = location.find(NAME_SPLIT_CHARACTER)
    if first_split_character >= 0:
        head_location = location[0:first_split_character]
        sub_location = location[first_split_character+1:]
    else:
        head_location = location
        sub_location = ''
    return  (head_location, sub_location)


class DeviceDefinition:

    ROOT_ADDRESS_SPACE = '--DEVICE-ROOT--'
    LEGACY_REG_ADDRESS_SPACE = '--LEGACY-REGISTERS--'
    LEGACY_MEM_ADDRESS_SPACE = '--LEGACY-MEMORY--'

    TRANSPARENCY_MARKER = lowlevhw.AddressSpace.TRANSPARENCY_MARKER

    def __init__(self) -> None:
        self._root_space = lowlevhw.AddressSpace(self.ROOT_ADDRESS_SPACE)
        self._aliases = []

    def __str__(self) -> str:
        return self.get_string()

    def get_string(self, abs_addresses: bool = True)-> str:
        output_string = ""
        for space in self._root_space.get_elements():

            if self._root_space._elements[space.name].transparent:
                tmarker = self.TRANSPARENCY_MARKER
            else:
                tmarker = ''
            space_name = f"{tmarker}{space.name}{tmarker}"
            output_string += f"Root Address Space - {space_name}\n"
            output_string += '-' * 80 + "\n"

            for element in space.definition.get_elements_sorted_by_address():
                output_string += f"0x{element.base_address:>08X}: "
                tmarker = ''
                if element.transparent:
                    tmarker = self.TRANSPARENCY_MARKER
                output_string += f"{tmarker}{element.name}{tmarker} - "
                if isinstance(element.definition, lowlevhw.RegisterBank):
                    output_string += f"Register Bank <{element.definition.name}>\n"
                    output_string += element.definition.get_string() + "\n"
                else:
                    output_string += f"{element.definition.get_string(abs_addresses=abs_addresses, base_address=element.base_address)}\n"
        output_string = output_string[:-1]
        return output_string

    def __getitem__(self, location) -> object:
        alias = self.get_alias(location)
        if alias:
            return alias
        device_elements = []
        device_elements.append(self._root_space[location])
        if len(device_elements) == 1:
            return device_elements[0]
        elif len(device_elements) == 0:
            raise KeyError(f"Element <{location} not found in device definition")
        raise KeyError(f"Multiple elements with <{location}> found in device definition")

    def get_element(self, name: str):
        """Get element with name, including transparent elements."""
        local_name, sub_name = lowlevhw.split_element_location(name)
        if local_name[0] == '*' and local_name[-1] == '*':
            local_name = local_name[1:-1]
        address_space = self._root_space.get_element(local_name)
        if not sub_name:
            return address_space
        return address_space.get_element(sub_name)

    def get_registers(self) -> typing.List[str]:
        return self._root_space.get_registers()

    def get_path(self, location: str) -> str:
        """Full path to an element in device.

        The full path including transparent address spaces.
        """
        possible_paths = self._root_space.get_path(location)
        if len(possible_paths) == 1:
            return possible_paths[0]
        elif len(possible_paths) > 1:
            raise KeyError(f"Location '{location}' does lead to multiple elements: {possible_paths}")
        raise KeyError(f"Path to element '{location}' could not be determined")

    def add_address_space(self,
                          address_space: lowlevhw.AddressSpace,
                          name: typing.Optional[str] = None,
                          transparent: bool = lowlevhw.DEFAULT_TRANSPARENCY
                          ) -> None:
        if not name:
            name = address_space.name
        self._root_space.add_element(address_space, name=name, transparent=transparent)

    def get_address_spaces(self) -> typing.List[lowlevhw.AddressSpace]:
        return [element.definition for element in self._root_space.get_elements()]

    def get_address_space(self, name: str) -> lowlevhw.AddressSpace:
        try:
            return self._root_space.get_element(name)
        except KeyError as exc:
            raise KeyError(f"Address space <{name}> not found in device definition") from exc

    def add_register_bank(self, register_bank) -> None:
        # NOTE: LEGACY -- register map added but no further tests are performed
        if self.LEGACY_REG_ADDRESS_SPACE not in self._root_space.get_elements():
            self.add_address_space(lowlevhw.AddressSpace(self.LEGACY_REG_ADDRESS_SPACE), transparent=True)
        address_space = self._root_space.get_element(self.LEGACY_REG_ADDRESS_SPACE)
        address_space.add_element(register_bank, register_bank.base_address, transparent=True)

    def add_memory_map(self, memory_map) -> None:
        # NOTE: LEGACY -- memory map added but no further tests are performed
        if self.LEGACY_MEM_ADDRESS_SPACE not in self._root_space.get_elements():
            self.add_address_space(lowlevhw.AddressSpace(self.LEGACY_MEM_ADDRESS_SPACE), transparent=True)
        address_space = self._root_space.get_element(self.LEGACY_MEM_ADDRESS_SPACE)
        address_space.add_element(memory_map, transparent=True)

    def has_legacy_defintion(self) -> bool:
        address_space_names = [space.name for space in self._root_space.get_elements()]
        legacy_registers = self.LEGACY_REG_ADDRESS_SPACE in address_space_names
        legacy_memory = self.LEGACY_MEM_ADDRESS_SPACE in address_space_names
        return legacy_registers or legacy_memory

    def add_alias(self, name: str, element_name: str, fmt=None) -> None:
        self._aliases.append(lowlevhw.ElementAlias(name, element_name, fmt))

    def get_alias(self, location: str):
        aliases = []
        for alias in self._aliases:
            if alias.name.lower() == location.lower():
                aliases.append(alias)
        if len(aliases) == 1:
            return aliases[0]
        elif len(aliases) == 0:
            return None
        raise KeyError(f"device[] : Problem with finding alias {location}.")

    def get_address(self, location: str) -> int:
        full_path = self.get_path(location)
        (local_location, sub_location) = lowlevhw.split_element_location(full_path)
        space_element = self.get_element(local_location)
        address = 0
        if sub_location:
            address += space_element.get_address(sub_location)
        return address

    def print_register_map(self) -> None:
        if self.has_legacy_defintion():
            for addr_space_element in self.get_address_space('--LEGACY-REGISTERS--').get_elements_sorted_by_address():
                print(addr_space_element.definition)
        else:
            print (self.get_string())

    def print_memory_map_definition(self):
        for memory_map in self._memory_maps:
            print(memory_map)

    def print_memory_map(self):
        area_info = []
        for memory_map in self._memory_maps:
            for mem_area in memory_map.get_areas():
                dev_mem_area = self[mem_area.name]
                base_address = dev_mem_area.get_base_address()
                size = dev_mem_area.get_size()
                name = dev_mem_area.definition.name
                area_info.append( (base_address, name, size) )
        for mem_area in sorted(area_info, key=lambda area: area[0]):
            print(f"0x{mem_area[0]:0>8x} : {mem_area[1]} ({mem_area[2]}) bytes")

    def get_register_map_html(self, split_on_id=True):
        string = """<html>
  <head>
    <title></title>
  </head>

  <body>"""

        for (_, regbank) in sorted(self._register_banks.items()):
            generator = lowlevhw.generators.RegbankHtmlDocGen(regbank)
            string += generator.generate_string(split_on_id)
        string += """  </body>
        </html>"""
        return string
