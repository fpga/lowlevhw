# -----------------------------------------------------------------------------
#  File        : cocotb_axi_driver.py
#  Brief       : Driver for register access in cocotb
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2024-01-19
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2024 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import logging

import lowlevhw
import lowlevhw.device_driver


class CocotbAxi(lowlevhw.device_driver.DeviceDriver):

    DEV_STRING = 'DUMMY'

    def __init__(self) -> None:
        self.register_driver = None
        self.memory_driver = None
        self.log = logging.getLogger("LowLevHW.cocotb-axi")
        self.log.setLevel(logging.DEBUG)

    @classmethod
    def find_devices(cls) -> None:
        return None

    def open_device(self, register_driver=None, memory_driver=None) -> None:
        self.register_driver = register_driver
        self.memory_driver = memory_driver

    def close_device(self) -> None:
        pass

    def read_register(self, address) -> int:
        pass

    async def read_register_async(self, address) -> int:
        register_value = await self.register_driver.read_dword(address)
        self.log.info(f"Read 0x{register_value:08x} from register at 0x{address:x}")
        return register_value

    def write_register(self, address: int, data: int) -> None:
        pass

    async def write_register_async(self, address: int, data: int) -> None:
        await self.register_driver.write_dword(address, data)
        self.log.info(f"Write 0x{data:08x} to register at 0x{address:x}")

    def read_ram(self, offset, size):
        pass
    #     data = []
    #     for i in range(offset, offset+size):
    #         try:
    #             data.append(self.memory[i])
    #         except KeyError:
    #             data.append(0)
    #     return bytearray(data)

    def write_ram(self, offset, size, data):
        pass
    #     data_cnt = 0
    #     for i in range(offset, offset+size):
    #         self.memory[i] = data[data_cnt]
    #         data_cnt += 1
