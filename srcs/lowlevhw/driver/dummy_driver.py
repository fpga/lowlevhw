# -----------------------------------------------------------------------------
#  File        : dummy_driver.py
#  Brief       : Driver for a dummy device used for testing LowLevHW.
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2019-02-07
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import lowlevhw
import lowlevhw.device_driver


class Dummy(lowlevhw.device_driver.DeviceDriver):

    DEV_STRING = 'DUMMY'

    def __init__(self, device_definition: lowlevhw.DeviceDefinition=None):
        self.registers = {}
        self.memory = {}
        if device_definition is not None:
            for address_space in device_definition.get_address_spaces():
                for element in address_space.get_elements():
                    if isinstance(element.definition, lowlevhw.RegisterBank):
                        base_address = address_space.get_address(element.name)
                        for register in element.definition.get_registers():
                            register_address = element.definition.get_address(register.name)
                            self.write_register(base_address+register_address, register.reset_value)


    @classmethod
    def find_devices(cls):
        return cls.DEV_STRING

    def open_device(self, dev_context) -> None:
        if dev_context != self.DEV_STRING:
            raise OSError("Dummy device driver can only connect to 'DUMMY' device")

    def close_device(self) -> None:
        pass

    def read_register(self, address):
        if address in self.registers:
            return self.registers[address]
        else:
            return 0

    def write_register(self, address, data):
        self.registers[address] = data

    def read_ram(self, offset, size):
        data = []
        for i in range(offset, offset+size):
            try:
                data.append(self.memory[i])
            except KeyError:
                data.append(0)
        return bytearray(data)

    def write_ram(self, offset, size, data):
        data_cnt = 0
        for i in range(offset, offset+size):
            self.memory[i] = data[data_cnt]
            data_cnt += 1
