# -----------------------------------------------------------------------------
#  File        : dummy_driver_ssh.py
#  Brief       : Driver for a LowLevHW dummy device accessed over SSH.
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2021-07-05
# -----------------------------------------------------------------------------
#  Description : NOT FUNCTIONAL
# -----------------------------------------------------------------------------
#  Copyright (C) 2021 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import lowlevhw.device_driver


class Dummy(lowlevhw.device_driver.DeviceDriver, lowlevhw.device_driver.DeviceDriverSSH):

    DEV_STRING = 'DUMMY'

    def __init__(self):
        self.registers = {}
        self.memory = {}
        self.dev_node = None

    @classmethod
    def find_devices(cls, target, jumpbox=lowlevhw.SSHConfig()):
        essffw_devices = []
        remote = lowlevhw.device_driver.DeviceDriverSSH()
        remote.connect_remote(host, username, password, jumphost=jumphost, jumpusername=jumpusername, jumppassword=jumppassword)
        devices = remote.execute_remote("python3 -c 'import lowlevhw; print(lowlevhw.driver.Dummy.DEV_STRING)")
        essffw_devices = list(map(lambda x: x.replace('\n', '') , essffw_devices))
        remote.close_remote()
        return essffw_devices

    def open_device(self, dev_node,):
        self.connect_remote()
        self.dev_node = dev_node

    def close_device(self):
        self.close_remote()

    # def read_register(self, address):
    #     # if address in self.registers:
    #     #     return self.registers[address]
    #     # else:
    #     #     return 0
    #
    # def write_register(self, address, data):
    #     self.registers[address] = data
    #
    # def read_ram(self, offset, size):
    #     data = []
    #     for i in range(offset, offset+size):
    #         try:
    #             data.append(self.memory[i])
    #         except KeyError:
    #             data.append(0)
    #     return bytearray(data)
    #
    # def write_ram(self, offset, size, data):
    #     data_cnt = 0
    #     for i in range(offset, offset+size):
    #         self.memory[i] = data[data_cnt]
    #         data_cnt += 1
