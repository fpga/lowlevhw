# -----------------------------------------------------------------------------
#  File        : register_bank_2.py
#  Brief       :
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2023-10-12
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import lowlevhw


address_width  = 14
register_width = 32

register_bank_2 = lowlevhw.RegisterBank('register_bank_2', address_width, register_width, error_reporting=True)

# Basic read/write register with reset value
register_bank_2.add_register('id', address=0x0001, reset_value=0xABCD_ABCD, modes='B', description="This is the ID register.")

# Basic read/write register without reset value (default reset value)
register_bank_2.add_register('instance', modes='R')

# Add read-only register at lower address
register_bank_2.add_register('baud_rate', 0x12, modes='R', description="System ID, this identifies the system.\n This text tries multiline comments\n The formatting is a bit annoying in python" )

# Add fixed-value register
register_bank_2.add_register('error', 0x11, modes='RW')
