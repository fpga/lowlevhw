# -----------------------------------------------------------------------------
#  File        : register_bank_1.py
#  Brief       :
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2023-10-12
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import lowlevhw

import register_bank_1
import register_bank_2


register_map_1 = lowlevhw.AddressSpace('register_map_1')

register_map_1.add_element(register_bank_1.register_bank_1, 0x2000, name='reg1')
register_map_1.add_element(register_bank_2.register_bank_2, 0x3000, name='reg2')
