# -----------------------------------------------------------------------------
#  File        : register_bank_1.py
#  Brief       :
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2023-10-12
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import lowlevhw


address_width  = 14
register_width = 32

register_bank_1 = lowlevhw.RegisterBank('register_bank_1', address_width, register_width, element_addressing=True, error_reporting=True)

# Basic read/write register with reset value
register_bank_1.add_register('register_1', address=1, reset_value=0x1122_3344, modes='RW', description="This is register_1. And this text is a test of how the formatting works.")

# Basic read/write register without reset value (default reset value)
register_bank_1.add_register('register_2', modes='RW')

# Add read-only register at lower address
register_bank_1.add_register('system_id', 0, modes='R', description="System ID, this identifies the system.\n This text tries multiline comments\n The formatting is a bit annoying in python" )

# Add fixed-value register
register_bank_1.add_register('fw_id', 0xD, modes='B', reset_value=0xBEEF)
