# -----------------------------------------------------------------------------
#  File        : register_bank_2.py
#  Brief       :
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2023-10-12
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import lowlevhw

import register_bank_1
import register_bank_2
import register_map_1


class TestDevice1Access(lowlevhw.Device):

   def __init__(self):
      register_address_space = lowlevhw.AddressSpace('register')
      register_address_space.add_element(register_bank_2.register_bank_2, 0x01000, name='reg2a')
      register_address_space.add_element(register_bank_2.register_bank_2, 0x02000, name='reg2b')
      register_address_space.add_element(register_bank_1.register_bank_1, 0x00000, name='reg1')
      register_address_space.add_element(register_map_1.register_map_1,   0x10000, name='grouping')

      device_1 = lowlevhw.DeviceDefinition()
      device_1.add_address_space(register_address_space, transparent=True)

      super().__init__(device_1, lowlevhw.lowlevhw.driver.Dummy(device_1))
