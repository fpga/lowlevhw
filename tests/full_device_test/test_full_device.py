# -----------------------------------------------------------------------------
#  File        : register_bank_2.py
#  Brief       :
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2023-10-12
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

from device_1 import TestDevice1Access


def main() -> None:
    device = TestDevice1Access()

    reset_value = device.definition['reg1']['fw_id'].reset_value
    print(f"Reset value of 'reg1:fw_id' : {hex(reset_value)}")

    fw_id_address = device.definition['reg1'].get_address('fw_id')
    fw_id_global = device.definition.get_address('reg1:fw_id')
    print(f"fw_id register @ reg1 -- local: {hex(fw_id_address)}, global: {hex(fw_id_global)}")

    id_a_address = device.definition['reg2a'].get_address('id')
    id_a_global = device.definition.get_address('reg2a:id')
    print(f"id register @ reg2a -- local: {hex(id_a_address)}, global: {hex(id_a_global)}")

    id_b_address = device.definition['reg2a'].get_address('id')
    id_b_global = device.definition.get_address('reg2b:id')
    print(f"id register @ reg2b -- local: {hex(id_b_address)}, global: {hex(id_b_global)}")

    # device.definition['reg1']['fw_id'].get_full_address() <-- needs links
    # device.definition.get_full_address(RegisterObject)  <-- not possible as a register could be part of two

    grp_1_id_address = device.definition['grouping']['reg1'].get_address('fw_id')
    grp_1_id_global = device.definition.get_address('grouping:reg1:fw_id')
    print(f"fw_id register @ reg1 within grouping -- local: {hex(grp_1_id_address)}, global: {hex(grp_1_id_global)}")

if __name__ == '__main__':
    main()
