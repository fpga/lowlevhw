--------------------------------------------------------------------------------
--! @file   hdl_test_bank_tb.vhdl
--! @brief  Test bench for the HDL files of generated register bank
--!
--! @details
--!
--! @author       Christian Amstutz
--!
--! @date         2020-10-08
--!
--! \b Company:   European Spallation Source ERIC \n
--! \b Platform:  FPGA-generic \n
--! \b Standard:  VHDL-1993
--!
--! @copyright    Copyright (C) 2020 - 2021 European Spallation Source ERIC
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library element_mode_bank;
library element_mode_bank_slv;


entity testbench is
  -- empty
end testbench;

architecture tb of testbench is

  constant TEST_AXI_ADDR_WIDTH : integer := 16;

  signal sim_done                  : boolean := false;

  signal clk_100mhz                : std_logic := '1';

  signal reset_n                   : std_logic := '0';
  signal decode_error_write        : std_logic;
  signal decode_error_read         : std_logic;
  signal write_en                  : std_logic;
  signal write_strb                : std_logic_vector(element_mode_bank.register_bank_config.WSTRB_WIDTH-1 downto 0);
  signal write_address             : std_logic_vector(element_mode_bank.register_bank_config.ADDRESS_WIDTH-1 downto 0);
  signal bus_write_data            : std_logic_vector(element_mode_bank.register_bank_config.REGISTER_WIDTH-1 downto 0);
  signal read_en                   : std_logic;
  signal read_address              : std_logic_vector(element_mode_bank.register_bank_config.ADDRESS_WIDTH-1 downto 0);
  signal bus_read_data             : std_logic_vector(element_mode_bank.register_bank_config.REGISTER_WIDTH-1 downto 0);

  signal awaddr                    : std_logic_vector(TEST_AXI_ADDR_WIDTH-1 downto 0);
  signal awprot                    : std_logic_vector(element_mode_bank.axi4.AXI4_PROT_WIDTH-1 downto 0);
  signal awvalid                   : std_logic;
  signal awready                   : std_logic;
  signal wdata                     : std_logic_vector(element_mode_bank.axi4.AXI4L_DATA_WIDTH-1 downto 0);
  signal wstrb                     : std_logic_vector(3 downto 0);
  signal wvalid                    : std_logic;
  signal wready                    : std_logic;
  signal bresp                     : std_logic_vector(element_mode_bank.axi4.AXI4_RESP_WIDTH-1 downto 0);
  signal bvalid                    : std_logic;
  signal bready                    : std_logic;
  signal araddr                    : std_logic_vector(TEST_AXI_ADDR_WIDTH-1 downto 0);
  signal arprot                    : std_logic_vector(element_mode_bank.axi4.AXI4_PROT_WIDTH-1 downto 0);
  signal arvalid                   : std_logic;
  signal arready                   : std_logic;
  signal rdata                     : std_logic_vector(element_mode_bank.axi4.AXI4L_DATA_WIDTH-1 downto 0);
  signal rresp                     : std_logic_vector(element_mode_bank.axi4.AXI4_RESP_WIDTH-1 downto 0);
  signal rvalid                    : std_logic;
  signal rready                    : std_logic;

  signal awready_slv               : std_logic;
  signal bvalid_slv                : std_logic;
  signal wready_slv                : std_logic;
  signal rvalid_slv                : std_logic;
  signal rdata_slv                 : std_logic_vector(element_mode_bank.axi4.AXI4L_DATA_WIDTH-1 downto 0);

  signal transfer_shadow_group     : element_mode_bank.register_bank_config.transfer_shadow_group_t;
  signal logic_data                : element_mode_bank.register_bank_config.logic_read_data_t;
  signal logic_return              : element_mode_bank.register_bank_config.logic_return_t;
  signal transfer_shadow_group_axi : element_mode_bank.register_bank_config.transfer_shadow_group_t;
  signal logic_data_axi            : element_mode_bank.register_bank_config.logic_read_data_t;
  signal logic_data_slv            : element_mode_bank_slv.register_bank_config.logic_read_data_t;
  signal logic_return_slv          : element_mode_bank_slv.register_bank_config.logic_return_t;
  signal transfer_shadow_group_slv : element_mode_bank_slv.register_bank_config.transfer_shadow_group_t;


  procedure read_register(
    signal   clk         : in  std_logic;
    signal   read_en     : out std_logic;
    signal   bus_address : out std_logic_vector;
    constant address     : in  integer
  ) is
  begin
    wait until falling_edge(clk);
    read_en     <= '1';
    bus_address <= std_logic_vector(to_unsigned(address, bus_address'length));
    wait until falling_edge(clk);
    read_en <= '0';
  end procedure;

  procedure write_register(
    signal   clk         : in  std_logic;
    signal   write_en    : out std_logic;
    signal   bus_address : out std_logic_vector;
    signal   bus_data    : out std_logic_vector;
    constant address     : in  integer;
    constant value       : in  integer
  ) is
  begin
    wait until falling_edge(clk);
    write_en    <= '1';
    bus_address <= std_logic_vector(to_unsigned(address, bus_address'length));
    bus_data    <= std_logic_vector(to_unsigned(value, bus_data'length));
    wait until falling_edge(clk);
    write_en <= '0';
  end procedure;

  procedure read_register_axi(
    signal   clk     : in  std_logic;
    signal   araddr  : out std_logic_vector;
    signal   arvalid : out std_logic;
    signal   arready : in  std_logic;
    constant address : in  integer
  ) is
  begin
    wait until falling_edge(clk);
    arvalid <= '1';
    araddr <= std_logic_vector(to_unsigned(address, araddr'length));
    wait until falling_edge(clk);
    if arready = '1' then
      arvalid <= '0';
    else
      wait until arready = '1';
      arvalid <= '0';
    end if;
  end procedure;

  procedure write_register_axi(
    signal   clk     : in  std_logic;
    signal   awaddr  : out std_logic_vector;
    signal   awvalid : out std_logic;
    signal   awready : in  std_logic;
    signal   wdata   : out std_logic_vector;
    signal   wvalid  : out std_logic;
    signal   wready  : in  std_logic;
    signal   bvalid  : in  std_logic;
    signal   bready  : out std_logic;
    constant address : in  integer;
    constant value   : in  integer
  ) is
  begin
    wait until falling_edge(clk);
    awvalid <= '1';
    awaddr <= std_logic_vector(to_unsigned(address, araddr'length));
    if awready = '1' then
      wait until falling_edge(clk);
    else
      wait until awready = '1';
      wait until falling_edge(clk);
    end if;
    awvalid <= '0';
    wvalid <= '1';
    wdata <= std_logic_vector(to_unsigned(value, wdata'length));
    if wready = '1' then
      wait until falling_edge(clk);
    else
      wait until wready = '1';
      wait until falling_edge(clk);
    end if;
    wvalid <= '0';
    bready <= '1';
    if bvalid = '1' then
      wait until falling_edge(clk);
    else
      wait until bvalid = '1';
      wait until falling_edge(clk);
    end if;
    wait until falling_edge(clk);
    bready <= '0';
  end procedure;

begin

  dut : entity element_mode_bank.register_bank
    port map (
      clock_i                 => clk_100mhz,
      reset_n_i               => reset_n,
      decode_error_write_o    => decode_error_write,
      decode_error_read_o     => decode_error_read,
      write_en_i              => write_en,
      wstrb_i                 => write_strb,
      write_address_i         => write_address,
      bus_write_data_i        => bus_write_data,
      read_en_i               => read_en,
      read_address_i          => read_address,
      bus_read_data_o         => bus_read_data,
      transfer_shadow_group_i => transfer_shadow_group,
      logic_data_o            => logic_data,
      logic_return_i          => logic_return
    );

  transfer_shadow_group_axi.shadow_group_1 <= transfer_shadow_group.shadow_group_1;

  dut_axi : entity element_mode_bank.register_bank_axi
    generic map (
      AXI_ADDR_WIDTH => TEST_AXI_ADDR_WIDTH
    )
    port map (
      s_axi_aclk              => clk_100mhz,
      s_axi_aresetn           => reset_n,
      s_axi_awaddr            => awaddr,
      s_axi_awprot            => awprot,
      s_axi_awvalid           => awvalid,
      s_axi_awready           => awready,
      s_axi_wdata             => wdata,
      s_axi_wstrb             => wstrb,
      s_axi_wvalid            => wvalid,
      s_axi_wready            => wready,
      s_axi_bresp             => bresp,
      s_axi_bvalid            => bvalid,
      s_axi_bready            => bready,
      s_axi_araddr            => araddr,
      s_axi_arprot            => arprot,
      s_axi_arvalid           => arvalid,
      s_axi_arready           => arready,
      s_axi_rdata             => rdata,
      s_axi_rresp             => rresp,
      s_axi_rvalid            => rvalid,
      s_axi_rready            => rready,
      transfer_shadow_group_i => transfer_shadow_group_axi,
      register_data_o         => logic_data_axi,
      register_return_i       => logic_return
    );

  transfer_shadow_group_slv.shadow_group_1 <= transfer_shadow_group.shadow_group_1;
  logic_return_slv.system_id  <= logic_return.system_id;
  logic_return_slv.register_1 <= logic_return.register_1;
  logic_return_slv.register_2 <= logic_return.register_2;
  logic_return_slv.a_factor   <= logic_return.register_10.a_factor;
  logic_return_slv.b_factor   <= logic_return.register_10.b_factor;
  logic_return_slv.en(0)      <= logic_return.register_10.en;
  logic_return_slv.param_x    <= logic_return.param_x;

  dut_axi_slv : entity element_mode_bank_slv.register_bank_axi
    generic map (
      AXI_ADDR_WIDTH => TEST_AXI_ADDR_WIDTH
    )
    port map (
      s_axi_aclk              => clk_100mhz,
      s_axi_aresetn           => reset_n,
      s_axi_awaddr            => awaddr,
      s_axi_awprot            => awprot,
      s_axi_awvalid           => awvalid,
      s_axi_awready           => awready_slv,
      s_axi_wdata             => wdata,
      s_axi_wstrb             => wstrb,
      s_axi_wvalid            => wvalid,
      s_axi_wready            => wready_slv,
      s_axi_bresp             => bresp,
      s_axi_bvalid            => bvalid_slv,
      s_axi_bready            => bready,
      s_axi_araddr            => araddr,
      s_axi_arprot            => arprot,
      s_axi_arvalid           => arvalid,
      s_axi_arready           => arready,
      s_axi_rdata             => rdata_slv,
      s_axi_rresp             => rresp,
      s_axi_rvalid            => rvalid_slv,
      s_axi_rready            => rready,
      transfer_shadow_group_i => transfer_shadow_group_slv,
      register_data_o         => logic_data_slv,
      register_return_i       => logic_return_slv
    );

  -- Clock at 100 MHz
  clk_100mhz <= not clk_100mhz after 5 ns when sim_done /= true else '0';

  test_procedure : process
  begin

    write_en                  <= '0';
    write_strb                <= (others => '1');
    read_en                   <= '0';
    write_address             <= (others => '0');
    read_address              <= (others => '0');
    bus_write_data            <= (others => '0');
    transfer_shadow_group     <= (others => '0');

    awaddr  <= (others => '0');
    awprot  <= (others => '0');
    awvalid <= '0';
    wdata   <= (others => '0');
    wstrb   <= (others => '1');
    wvalid  <= '0';
    bready  <= '1';
    araddr  <= (others => '0');
    arprot  <= (others => '0');
    arvalid <= '0';
    rready  <= '1';

    wait for 100 ns;
    reset_n <= '1';

    report "Test values towards logic after reset";

    wait for 20 ns;
    assert logic_data.register_1                 = x"1122_3344" report "Wrong value after reset at 'register_1'" severity error;
    assert logic_data.register_2                 = x"0000_0000" report "Wrong value after reset at 'register_2'" severity error;
    assert logic_data.register_10.a_factor       = x"00"        report "Wrong value after reset at 'register_10.a_factor'" severity error;
    assert logic_data.register_10.b_factor       = x"44"        report "Wrong value after reset at 'register_10.b_factor'" severity error;
    assert logic_data.register_10.en             = '0'          report "Wrong value after reset at 'register_10.en'" severity error;
    assert logic_data.adc_value                  = x"0000_0000" report "Wrong value after reset at 'adc_value'" severity error;
    assert logic_data.register_30.reg30_a_factor = x"00"        report "Wrong value after reset at 'register_30.reg30_a_factor'" severity error;
    assert logic_data.register_30.reg30_b_factor = x"44"        report "Wrong value after reset at 'register_30.reg30_b_factor'" severity error;
    assert logic_data.param_x                    = x"0FFF_FFFF" report "Wrong value after reset at 'param_x'" severity error;

    wait for 100 ns;

    report "***** Testing with legacy bus access *****";

    report "Test bus read values after reset, makes only sense for read-back registers";

    read_register(clk_100mhz, read_en, read_address, element_mode_bank.register_types.ADC_VALUE_ADDR);
    assert bus_read_data = x"0000_0000" report "Wrong value after reset over bus from 'adc_value'" severity error;
    assert decode_error_read = '0' report "Unexpected read decode error" severity error;

    read_register(clk_100mhz, read_en, read_address, element_mode_bank.register_types.FW_ID_ADDR);
    assert bus_read_data = x"0000_BEEF" report "Wrong value after reset over bus from 'fw_id'" severity error;
    assert decode_error_read = '0' report "Unexpected read decode error" severity error;

    read_register(clk_100mhz, read_en, read_address, element_mode_bank.register_types.PARAM_X_CLR_ADDR);
    assert bus_read_data = x"0FFF_FFFF" report "Wrong value after reset over bus from 'param_x_clr'" severity error;
    assert decode_error_read = '0' report "Unexpected read decode error" severity error;

    read_register(clk_100mhz, read_en, read_address, element_mode_bank.register_types.REGISTER_30_ADDR);
    assert bus_read_data = x"0044_0000" report "Wrong value after reset over bus from 'register_30'" severity error;
    assert decode_error_read = '0' report "Unexpected read decode error" severity error;

    wait for 100 ns;


    report "Test bus read values after change in logic";

    logic_return.system_id <= x"8765_4321";
    read_register(clk_100mhz, read_en, read_address, element_mode_bank.register_types.SYSTEM_ID_ADDR);
    assert bus_read_data = x"8765_4321" report "Wrong value after reading updated value from 'system_id'" severity error;
    assert decode_error_read = '0' report "Unexpected read decode error" severity error;

    logic_return.register_1 <= x"1234_5678";
    read_register(clk_100mhz, read_en, read_address, element_mode_bank.register_types.REGISTER_1_ADDR);
    assert bus_read_data = x"1234_5678" report "Wrong value after reading updated value from 'register_1'" severity error;
    assert decode_error_read = '0' report "Unexpected read decode error" severity error;

    logic_return.register_2 <= x"ABCD_EF12";
    read_register(clk_100mhz, read_en, read_address, element_mode_bank.register_types.REGISTER_2_ADDR);
    assert bus_read_data = x"ABCD_EF12" report "Wrong value after reading updated value from 'register_2'" severity error;
    assert decode_error_read = '0' report "Unexpected read decode error" severity error;

    logic_return.register_10.a_factor <= x"66";
    logic_return.register_10.b_factor <= x"77";
    logic_return.register_10.en       <= '1';
    read_register(clk_100mhz, read_en, read_address, element_mode_bank.register_types.REGISTER_10_ADDR);
    assert bus_read_data = x"4077_0066" report "Wrong value after reading updated value from 'register_10'" severity error;
    assert decode_error_read = '0' report "Unexpected read decode error" severity error;

    logic_return.param_x <= x"89AB_CDEF";
    read_register(clk_100mhz, read_en, read_address, element_mode_bank.register_types.PARAM_X_ADDR);
    assert bus_read_data = x"89AB_CDEF" report "Wrong value after reading updated value from 'param_x'" severity error;
    assert decode_error_read = '0' report "Unexpected read decode error" severity error;


    wait for 100 ns;

    report "Test bus write, read logic registers";

    write_register(clk_100mhz, write_en, write_address, bus_write_data, element_mode_bank.register_types.REGISTER_1_ADDR, 16#0ABB_CCDD#);
    wait until falling_edge(clk_100mhz);
    assert logic_data.register_1 = x"0ABB_CCDD" report "Wrong value after bus write to 'register_1'" severity error;
    assert decode_error_write = '0' report "Unexpected write decode error" severity error;

    write_register(clk_100mhz, write_en, write_address, bus_write_data, element_mode_bank.register_types.REGISTER_2_ADDR, 16#1ABB_CCDD#);
    wait until falling_edge(clk_100mhz);
    assert logic_data.register_2 = x"1ABB_CCDD" report "Wrong value after bus write to 'register_2'" severity error;
    assert decode_error_write = '0' report "Unexpected write decode error" severity error;

    write_register(clk_100mhz, write_en, write_address, bus_write_data, element_mode_bank.register_types.REGISTER_10_ADDR, 16#4234_5678#);
    wait until falling_edge(clk_100mhz);
    assert logic_data.register_10.a_factor = x"78" report "Wrong value after bus write to 'register_10.a_factor'" severity error;
    assert logic_data.register_10.b_factor = x"34" report "Wrong value after bus write to 'register_10.b_factor'" severity error;
    assert logic_data.register_10.en       = '1'   report "Wrong value after bus write to 'register_10.en'"       severity error;
    assert decode_error_write = '0' report "Unexpected write decode error" severity error;

    write_register(clk_100mhz, write_en, write_address, bus_write_data, element_mode_bank.register_types.PARAM_X_ADDR, 16#0FED_BEEF#);
    wait until falling_edge(clk_100mhz);
    assert logic_data.param_x = x"0FED_BEEF" report "Wrong value after bus write to 'param_x'" severity error;
    assert decode_error_write = '0' report "Unexpected write decode error" severity error;

    report "Test bus write and read the value back, read-back registers";

    write_register(clk_100mhz, write_en, write_address, bus_write_data, element_mode_bank.register_types.ADC_VALUE_ADDR, 16#0BEE_EEFF#);
    wait until falling_edge(clk_100mhz);
    assert logic_data.adc_value = x"0BEE_EEFF" report "Wrong value after bus write to 'adc_value'" severity error;
    assert decode_error_write = '0' report "Unexpected write decode error" severity error;
    read_register(clk_100mhz, read_en, read_address, element_mode_bank.register_types.ADC_VALUE_ADDR);
    assert bus_read_data = x"0BEE_EEFF" report "Wrong value after reading written value from 'adc_value'" severity error;
    assert decode_error_read = '0' report "Unexpected read decode error" severity error;

    write_register(clk_100mhz, write_en, write_address, bus_write_data, element_mode_bank.register_types.REGISTER_30_ADDR, 16#0FED_BEEF#);
    wait until falling_edge(clk_100mhz);
    assert logic_data.register_30.reg30_a_factor = x"EF" report "Wrong value after bus write to 'register_30.reg30_a_factor'" severity error;
    assert logic_data.register_30.reg30_b_factor = x"ED" report "Wrong value after bus write to 'register_30.reg30_b_factor'" severity error;
    assert decode_error_write = '0' report "Unexpected write decode error" severity error;
    read_register(clk_100mhz, read_en, read_address, element_mode_bank.register_types.REGISTER_30_ADDR);
    assert bus_read_data = x"00ED_00EF" report "Wrong value after reading written value from 'register_30'" severity error;
    assert decode_error_read = '0' report "Unexpected read decode error" severity error;

    wait for 100 ns;

    report "Test bus write - set/clear registers";

    write_register(clk_100mhz, write_en, write_address, bus_write_data, element_mode_bank.register_types.PARAM_X_ADDR, 16#5555_5555#);
    write_register(clk_100mhz, write_en, write_address, bus_write_data, element_mode_bank.register_types.PARAM_X_SET_ADDR, 16#00FF_0002#);
    wait until falling_edge(clk_100mhz);
    assert logic_data.param_x = x"55FF_5557" report "Wrong value after setting fields in  'param_x'" severity error;

    write_register(clk_100mhz, write_en, write_address, bus_write_data, element_mode_bank.register_types.PARAM_X_ADDR, 16#5555_5555#);
    write_register(clk_100mhz, write_en, write_address, bus_write_data, element_mode_bank.register_types.PARAM_X_CLR_ADDR, 16#00F0_0001#);
    wait until falling_edge(clk_100mhz);
    assert logic_data.param_x = x"5505_5554" report "Wrong value after clearing fields in  'param_x'" severity error;

    wait for 100 ns;

    -- Test bus write - strobe registers
    write_register(clk_100mhz, write_en, write_address, bus_write_data, element_mode_bank.register_types.STROBE_REG_ADDR, 16#5555_5555#);
    wait until falling_edge(clk_100mhz);
    assert logic_data.strobe_reg = x"5555_5555" report "Wrong value after writing 'strobe_reg'" severity error;
    wait until falling_edge(clk_100mhz);
    assert logic_data.strobe_reg = x"0000_0000" report "Value not released for 'strobe_reg'" severity error;

    wait for 100 ns;

    report "Test decode errors";

    write_register(clk_100mhz, write_en, write_address, bus_write_data, 16#300#, 16#5555_5555#);
    assert decode_error_write = '1' report "Write decode error not detected" severity error;
    assert decode_error_read = '0' report "Unexpected read decode error" severity error;
    wait until falling_edge(clk_100mhz);
    wait until falling_edge(clk_100mhz);
    assert decode_error_write = '1' report "Write decode error does not stay high" severity error;

    read_register(clk_100mhz, read_en, read_address, 16#301#);
    assert decode_error_read = '1' report "Read decode error not detected" severity error;
    if decode_error_read = '1' then
      wait until falling_edge(clk_100mhz);
      wait until falling_edge(clk_100mhz);
      assert decode_error_read = '1' report "Read decode error does not stay high" severity error;
    else
      wait until falling_edge(clk_100mhz);
      wait until falling_edge(clk_100mhz);
    end if;

    write_register(clk_100mhz, write_en, write_address, bus_write_data, element_mode_bank.register_types.REGISTER_1_ADDR, 16#5555_5555#);
    assert decode_error_write = '0' report "Write decode error not released" severity error;

    read_register(clk_100mhz, read_en, read_address, element_mode_bank.register_types.REGISTER_1_ADDR);
    assert decode_error_read = '0' report "Read decode error not released" severity error;

    wait for 500 ns;
    report "***** Testing with AXI bus access *****";

    reset_n <= '0';
    wait until falling_edge(clk_100mhz);
    reset_n <= '1';
    wait for 50 ns;

    report "Test bus read values after reset, makes only sense for read-back registers";

    read_register_axi(clk_100mhz, araddr, arvalid, arready, element_mode_bank.register_types.ADC_VALUE_ADDR*4);
    wait until (rvalid = '1') and (rvalid_slv = '1');
    assert rdata = x"0000_0000" report "Wrong value after reset over AXI bus from 'adc_value'" severity error;
    assert rdata_slv = x"0000_0000" report "Wrong value after reset over AXI bus from 'adc_value'" severity error;
    wait until falling_edge(clk_100mhz);

    read_register_axi(clk_100mhz, araddr, arvalid, arready, element_mode_bank.register_types.FW_ID_ADDR*4);
    wait until (rvalid = '1') and (rvalid_slv = '1');
    assert rdata = x"0000_BEEF" report "Wrong value after reset over AXI bus from 'fw_id'" severity error;
    assert rdata_slv = x"0000_BEEF" report "Wrong value after reset over AXI bus from 'fw_id'" severity error;
    wait until falling_edge(clk_100mhz);

    read_register_axi(clk_100mhz, araddr, arvalid, arready, element_mode_bank.register_types.PARAM_X_CLR_ADDR*4);
    wait until (rvalid = '1') and (rvalid_slv = '1');
    assert rdata = x"0FFF_FFFF" report "Wrong value after reset over AXI bus from 'param_x_clr'" severity error;
    assert rdata_slv = x"0FFF_FFFF" report "Wrong value after reset over AXI bus from 'param_x_clr'" severity error;
    wait until falling_edge(clk_100mhz);

    read_register_axi(clk_100mhz, araddr, arvalid, arready, element_mode_bank.register_types.REGISTER_30_ADDR*4);
    wait until (rvalid = '1') and (rvalid_slv = '1');
    assert rdata = x"0044_0000" report "Wrong value after reset over AXI bus from 'register_30'" severity error;
    assert rdata_slv = x"0044_0000" report "Wrong value after reset over AXI bus from 'register_30'" severity error;
    wait until falling_edge(clk_100mhz);

    wait for 100 ns;

    report "Test bus read values after change in logic";

    logic_return.system_id <= x"8765_4321";
    read_register_axi(clk_100mhz, araddr, arvalid, arready, element_mode_bank.register_types.SYSTEM_ID_ADDR*4);
    wait until (rvalid = '1') and (rvalid_slv = '1');
    assert rdata = x"8765_4321" report "Wrong value after reading updated value from 'system_id'" severity error;
    assert rdata_slv = x"8765_4321" report "Wrong value after reading updated value from 'system_id'" severity error;
    wait until falling_edge(clk_100mhz);

    logic_return.register_1 <= x"1234_5678";
    read_register_axi(clk_100mhz, araddr, arvalid, arready, element_mode_bank.register_types.REGISTER_1_ADDR*4);
    wait until rvalid = '1';
    assert rdata = x"1234_5678" report "Wrong value after reading updated value from 'register_1'" severity error;
    assert rdata_slv = x"1234_5678" report "Wrong value after reading updated value from 'register_1'" severity error;
    wait until falling_edge(clk_100mhz);

    logic_return.register_2 <= x"ABCD_EF12";
    read_register_axi(clk_100mhz, araddr, arvalid, arready, element_mode_bank.register_types.REGISTER_2_ADDR*4);
    wait until (rvalid = '1') and (rvalid_slv = '1');
    assert rdata = x"ABCD_EF12" report "Wrong value after reading updated value from 'register_2'" severity error;
    assert rdata_slv = x"ABCD_EF12" report "Wrong value after reading updated value from 'register_2'" severity error;
    wait until falling_edge(clk_100mhz);

    logic_return.register_10.a_factor <= x"66";
    logic_return.register_10.b_factor <= x"77";
    logic_return.register_10.en       <= '1';
    read_register_axi(clk_100mhz, araddr, arvalid, arready, element_mode_bank.register_types.REGISTER_10_ADDR*4);
    wait until (rvalid = '1') and (rvalid_slv = '1');
    assert rdata = x"4077_0066" report "Wrong value after reading updated value from 'register_10'" severity error;
    assert rdata_slv = x"4077_0066" report "Wrong value after reading updated value from 'register_10' (SLV)" severity error;
    wait until falling_edge(clk_100mhz);

    logic_return.param_x <= x"89AB_CDEF";
    read_register_axi(clk_100mhz, araddr, arvalid, arready, element_mode_bank.register_types.PARAM_X_ADDR*4);
    wait until (rvalid = '1') and (rvalid_slv = '1');
    assert rdata = x"89AB_CDEF" report "Wrong value after reading updated value from 'param_x'" severity error;
    assert rdata_slv = x"89AB_CDEF" report "Wrong value after reading updated value from 'param_x'" severity error;
    wait until falling_edge(clk_100mhz);

    report "Test bus write, read logic registers";

    write_register_axi(clk_100mhz, awaddr, awvalid, awready, wdata, wvalid, wready, bvalid, bready, element_mode_bank.register_types.REGISTER_1_ADDR*4, 16#0ABB_CCDD#);
    assert logic_data_axi.register_1 = x"0ABB_CCDD" report "Wrong value after bus write to 'register_1'" severity error;
    assert logic_data_slv.register_1 = x"0ABB_CCDD" report "Wrong value after bus write to 'register_1'" severity error;
    -- assert decode_error_write = '0' report "Unexpected write decode error" severity error;
    wait until falling_edge(clk_100mhz);

    write_register_axi(clk_100mhz, awaddr, awvalid, awready, wdata, wvalid, wready, bvalid, bready, element_mode_bank.register_types.REGISTER_2_ADDR*4, 16#1ABB_CCDD#);
    assert logic_data_axi.register_2 = x"1ABB_CCDD" report "Wrong value after bus write to 'register_2'" severity error;
    assert logic_data_slv.register_2 = x"1ABB_CCDD" report "Wrong value after bus write to 'register_2'" severity error;
    -- assert decode_error_write = '0' report "Unexpected write decode error" severity error;
    wait until falling_edge(clk_100mhz);

    write_register_axi(clk_100mhz, awaddr, awvalid, awready, wdata, wvalid, wready, bvalid, bready, element_mode_bank.register_types.REGISTER_10_ADDR*4, 16#4234_5678#);
    assert logic_data_axi.register_10.a_factor = x"78" report "Wrong value after bus write to 'register_10.a_factor'" severity error;
    assert logic_data_axi.register_10.b_factor = x"34" report "Wrong value after bus write to 'register_10.b_factor'" severity error;
    assert logic_data_axi.register_10.en       = '1'   report "Wrong value after bus write to 'register_10.en'" severity error;
    assert logic_data_slv.a_factor = x"78" report "Wrong value after bus write to 'register_10.a_factor'" severity error;
    assert logic_data_slv.b_factor = x"34" report "Wrong value after bus write to 'register_10.b_factor'" severity error;
    assert logic_data_slv.en = "1"         report "Wrong value after bus write to 'register_10.en'" severity error;
    -- assert decode_error_write = '0' report "Unexpected write decode error" severity error;
    wait until falling_edge(clk_100mhz);

    write_register_axi(clk_100mhz, awaddr, awvalid, awready, wdata, wvalid, wready, bvalid, bready, element_mode_bank.register_types.PARAM_X_ADDR*4, 16#0FED_BEEF#);
    assert logic_data_axi.param_x = x"0FED_BEEF" report "Wrong value after bus write to 'param_x'" severity error;
    assert logic_data_slv.param_x = x"0FED_BEEF" report "Wrong value after bus write to 'param_x'" severity error;
    -- assert decode_error_write = '0' report "Unexpected write decode error" severity error;
    wait until falling_edge(clk_100mhz);

    report "Test bus write and read the value back, read-back registers";

    write_register_axi(clk_100mhz, awaddr, awvalid, awready, wdata, wvalid, wready, bvalid, bready, element_mode_bank.register_types.ADC_VALUE_ADDR*4, 16#0BEE_EEFF#);
    assert logic_data_axi.adc_value = x"0BEE_EEFF" report "Wrong value after bus write to 'adc_value'" severity error;
    assert logic_data_slv.adc_value = x"0BEE_EEFF" report "Wrong value after bus write to 'adc_value'" severity error;
    -- assert decode_error_write = '0' report "Unexpected write decode error" severity error;
    read_register_axi(clk_100mhz, araddr, arvalid, arready, element_mode_bank.register_types.ADC_VALUE_ADDR*4);
    wait until (rvalid = '1') and (rvalid_slv = '1');
    assert rdata = x"0BEE_EEFF" report "Wrong value after reading written value from 'adc_value'" severity error;
    assert rdata_slv = x"0BEE_EEFF" report "Wrong value after reading written value from 'adc_value'" severity error;
    wait until falling_edge(clk_100mhz);
    -- assert decode_error_read = '0' report "Unexpected read decode error" severity error;

    write_register_axi(clk_100mhz, awaddr, awvalid, awready, wdata, wvalid, wready, bvalid, bready, element_mode_bank.register_types.REGISTER_30_ADDR*4, 16#0FED_BEEF#);
    -- wait until falling_edge(clk_100mhz);
    assert logic_data_axi.register_30.reg30_a_factor = x"EF" report "Wrong value after bus write to 'register_30.reg30_a_factor'" severity error;
    assert logic_data_axi.register_30.reg30_b_factor = x"ED" report "Wrong value after bus write to 'register_30.reg30_b_factor'" severity error;
    assert logic_data_slv.reg30_a_factor = x"EF" report "Wrong value after bus write to 'register_30.reg30_a_factor'" severity error;
    assert logic_data_slv.reg30_b_factor = x"ED" report "Wrong value after bus write to 'register_30.reg30_b_factor'" severity error;
    -- assert decode_error_write = '0' report "Unexpected write decode error" severity error;
    read_register_axi(clk_100mhz, araddr, arvalid, arready, element_mode_bank.register_types.REGISTER_30_ADDR*4);
    wait until (rvalid = '1') and (rvalid_slv = '1');
    assert rdata = x"00ED_00EF" report "Wrong value after reading written value from 'register_30'" severity error;
    assert rdata_slv = x"00ED_00EF" report "Wrong value after reading written value from 'register_30'" severity error;
    -- assert decode_error_read = '0' report "Unexpected read decode error" severity error;
    wait until falling_edge(clk_100mhz);

    report "Test delayed RREADY signal";

    wait for 50 ns;
    rready <= '0';
    read_register_axi(clk_100mhz, araddr, arvalid, arready, element_mode_bank.register_types.SYSTEM_ID_ADDR*4);
    wait for 20 ns;
    wait until falling_edge(clk_100mhz);
    rready <= '1';
    assert rdata = x"8765_4321" report "Wrong value after reading updated value from 'system_id'" severity error;
    assert rdata_slv = x"8765_4321" report "Wrong value after reading updated value from 'system_id'" severity error;

    wait for 50 ns;

    report "Test parallel access AW and W channel";

    awvalid <= '1';
    awaddr  <= std_logic_vector(to_unsigned(element_mode_bank.register_types.ADC_VALUE_ADDR*4, TEST_AXI_ADDR_WIDTH));
    wvalid  <= '1';
    wdata   <= std_logic_vector(to_unsigned(16#998877#, wdata'length));
    if (awready = '1') and (wready = '1') and (awready_slv = '1') and (wready_slv = '1') then
      wait until falling_edge(clk_100mhz);
    else
      wait until (awready = '1') and (wready = '1') and (awready_slv = '1') and (wready_slv = '1');
      wait until falling_edge(clk_100mhz);
    end if;
    awvalid <= '0';
    wvalid <= '0';
    bready <= '1';
    if (bvalid = '1') and (bvalid_slv = '1') then
      wait until falling_edge(clk_100mhz);
    else
      wait until (bvalid = '1') and (bvalid_slv = '1');
      wait until falling_edge(clk_100mhz);
    end if;
    wait until falling_edge(clk_100mhz);
    bready <= '0';
    assert logic_data_axi.adc_value = x"0099_8877" report "Wrong value after accessing AW and W channels in parallel writing 'adc_value'" severity error;
    assert logic_data_slv.adc_value = x"0099_8877" report "Wrong value after accessing AW and W channels in parallel writing 'adc_value'" severity error;

    wait for 200 ns;

    -- report "Test intermediate new read";
    -- read_register_axi(clk_100mhz, araddr, arvalid, arready, 16#430#*4);
    -- arvalid <= '1';
    -- araddr <= std_logic_vector(to_unsigned(16#40D#, TEST_AXI_ADDR_WIDTH));
    -- wait until arready = '1';
    -- wait until falling_edge(clk_100mhz);
    -- arvalid <= '0';
    --
    -- wait for 200 ns;
    --
    -- write_register_axi(clk_100mhz, awaddr, awvalid, awready, wdata, wvalid, wready, bvalid, bready, 16#400#*4, 16#0FED_E001#);
    -- wait for 20 ns;
    -- write_register_axi(clk_100mhz, awaddr, awvalid, awready, wdata, wvalid, wready, bvalid, bready, element_mode_bank.register_types.REGISTER_10_ADDR*4, 16#0876_5432#);
    -- wait for 20 ns;
    -- write_register_axi(clk_100mhz, awaddr, awvalid, awready, wdata, wvalid, wready, bvalid, bready, element_mode_bank.register_types.ADC_VALUE_ADDR*4, 16#11EE_EE11#);
    --
    -- wait for 200 ns;
    --

    -- wait for 200 ns;
    --
    -- -- Test AXI access to non-existing registers
    -- write_register_axi(clk_100mhz, awaddr, awvalid, awready, wdata, wvalid, wready, bvalid, bready, 16#488#*4, 16#0FED_E001#);
    -- wait for 20 ns;
    -- read_register_axi(clk_100mhz, araddr, arvalid, arready, 16#438#*4);
    --
    -- wait for 50 ns;
    --
    -- -- Test recovery from error access
    -- logic_return.system <= x"0000_0001";
    -- write_register_axi(clk_100mhz, awaddr, awvalid, awready, wdata, wvalid, wready, bvalid, bready, 16#400#*4, 16#1234_1234#);
    -- wait for 20 ns;
    -- read_register_axi(clk_100mhz, araddr, arvalid, arready, 16#400#*4);


    report "Test shadowed bus write";

    write_register(clk_100mhz, write_en, write_address, bus_write_data, element_mode_bank.register_types.SHADOW_1_ADDR, 16#4433_2211#);
    write_register_axi(clk_100mhz, awaddr, awvalid, awready, wdata, wvalid, wready, bvalid, bready, element_mode_bank.register_types.SHADOW_1_ADDR*4, 16#4433_2211#);
    wait until falling_edge(clk_100mhz);
    assert decode_error_write = '0' report "Unexpected write decode error" severity error;

    write_register(clk_100mhz, write_en, write_address, bus_write_data, element_mode_bank.register_types.SHADOW_2_ADDR, 16#6677_8899#);
    write_register_axi(clk_100mhz, awaddr, awvalid, awready, wdata, wvalid, wready, bvalid, bready, element_mode_bank.register_types.SHADOW_2_ADDR*4, 16#6677_8899#);
    wait until falling_edge(clk_100mhz);
    assert decode_error_write = '0' report "Unexpected write decode error" severity error;

    assert logic_data.shadow_1     /= x"4433_2211" report "Wrong value before unshadowing write to 'shadow_1' on legacy register bank" severity error;
    assert logic_data_axi.shadow_1 /= x"4433_2211" report "Wrong value before unshadowing write to 'shadow_1' on AXI register bank" severity error;
    assert logic_data_slv.shadow_1 /= x"4433_2211" report "Wrong value before unshadowing write to 'shadow_1' on AXI/SLV register bank" severity error;

    assert logic_data.shadow_2.low_byte     /= x"99" report "Wrong value before unshadowing write to 'shadow_2' on legacy register bank" severity error;
    assert logic_data_axi.shadow_2.low_byte /= x"99" report "Wrong value before unshadowing write to 'shadow_2' on AXI register bank" severity error;
    assert logic_data_slv.low_byte          /= x"99" report "Wrong value before unshadowing write to 'shadow_2' on AXI/SLV register bank" severity error;

    wait for 50 ns;

    read_register(clk_100mhz, read_en, read_address, element_mode_bank.register_types.SHADOW_1_ADDR);
    read_register_axi(clk_100mhz, araddr, arvalid, arready, element_mode_bank.register_types.SHADOW_1_ADDR*4);
    wait until (rvalid = '1') and (rvalid_slv = '1');
    assert rdata         = x"4433_2211" report "Wrong value after read-back value from 'shadow_1' before value application" severity error;
    assert rdata_slv     = x"4433_2211" report "Wrong value after read-back value from 'shadow_1' before value application" severity error;
    assert bus_read_data = x"4433_2211" report "Wrong value after read-back value from 'shadow_1' before value application" severity error;

    wait until falling_edge(clk_100mhz);
    transfer_shadow_group.shadow_group_1 <= '1';
    wait until falling_edge(clk_100mhz);
    transfer_shadow_group.shadow_group_1 <= '0';

    assert logic_data.shadow_1     = x"4433_2211" report "Wrong value after publishing write to 'shadow_1' on legacy register bank" severity error;
    assert logic_data_axi.shadow_1 = x"4433_2211" report "Wrong value after publishing write to 'shadow_1' on AXI register bank" severity error;
    assert logic_data_slv.shadow_1 = x"4433_2211" report "Wrong value after publishing write to 'shadow_1' on AXI/SLV register bank" severity error;

    assert logic_data.shadow_2.low_byte     = x"99" report "Wrong value after publishing write to 'shadow_2' on legacy register bank" severity error;
    assert logic_data_axi.shadow_2.low_byte = x"99" report "Wrong value after publishing write to 'shadow_2' on AXI register bank" severity error;
    assert logic_data_slv.low_byte          = x"99" report "Wrong value after publishing write to 'shadow_2' on AXI/SLV register bank" severity error;

    wait for 50 ns;

    read_register(clk_100mhz, read_en, read_address, element_mode_bank.register_types.SHADOW_1_ADDR);
    read_register_axi(clk_100mhz, araddr, arvalid, arready, element_mode_bank.register_types.SHADOW_1_ADDR*4);
    wait until (rvalid = '1') and (rvalid_slv = '1');
    assert rdata         = x"4433_2211" report "Wrong value after read-back value from 'shadow_1' after value application" severity error;
    assert rdata_slv     = x"4433_2211" report "Wrong value after read-back value from 'shadow_1' after value application" severity error;
    assert bus_read_data = x"4433_2211" report "Wrong value after read-back value from 'shadow_1' after value application" severity error;

    wait for 100 ns;

    report "Tests finished";

    wait for 200 ns;
    sim_done <= true;
    wait;

  end process test_procedure;

end architecture tb;
