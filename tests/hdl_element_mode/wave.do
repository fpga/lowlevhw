onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand /testbench/dut/logic_data_o
add wave -noupdate -expand -subitemconfig {/testbench/dut/logic_return_i.register_10 -expand} /testbench/dut/logic_return_i
add wave -noupdate /testbench/dut/bus_read_data_o
add wave -noupdate -divider AXI
add wave -noupdate -expand /testbench/dut_axi/register_data_o
add wave -noupdate -expand /testbench/dut_axi/register_return_i
add wave -noupdate /testbench/dut_axi/s_axi_rdata
add wave -noupdate -divider {AXI Legacy}
add wave -noupdate -expand /testbench/dut_axi_legacy/register_data_o
add wave -noupdate -expand /testbench/dut_axi_legacy/register_return_i
add wave -noupdate /testbench/dut_axi_legacy/s_axi_rdata
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {250000000 fs} 0}
quietly wave cursor active 1
configure wave -namecolwidth 232
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 fs} {3728809594 fs}
