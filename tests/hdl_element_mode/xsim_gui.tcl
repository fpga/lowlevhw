#-------------------------------------------------------------------------------
# Project     : ESS LLRF FPGA
#-------------------------------------------------------------------------------
# File        : xsim_gui.tcl
# Authors     : Christian Amstutz
# Created     : 2020-10-09
# Platform    : Xilinx Vivado 2020.1
# Standard    :
#-------------------------------------------------------------------------------
# Description : Simulation script for regisster generator test bench.
# Problems    :
#-------------------------------------------------------------------------------
# Copyright (c) 2020 European Spallation Source ERIC
#-------------------------------------------------------------------------------

add_wave *
run all
