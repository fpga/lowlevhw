# -----------------------------------------------------------------------------
#  File        : hdl_test_bank.py
#  Brief       : Test script to generate the register bank for the HDL tests.
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2023-10-12
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import os

import lowlevhw


def main() -> None:
    address_width  = 14
    register_width = 32

    bank = lowlevhw.RegisterBank('element_mode_bank', address_width, register_width, element_addressing=True, error_reporting=True)
    bank.base_address = 0x400

    # Basic read/write register with reset value
    bank.add_register('register_1', address=1, reset_value=0x1122_3344, modes='RW', description="This is register_1. And this text is a test of how the formatting works.")

    # Basic read/write register without reset value (default reset value)
    bank.add_register('register_2', modes='RW')

    # Add read-only register at lower address
    bank.add_register('system_id', 0, modes='R', description="System ID, this identifies the system.\n This text tries multiline comments\n The formatting is a bit annoying in python" )

    # Add fixed-value register
    bank.add_register('fw_id', 0xD, modes='B', reset_value=0xBEEF)

    # register 10
    bank.add_register('register_10', 0x00A, modes='RW')
    bank['register_10'].add_field('a_factor', 0, 8, description="A factor is the factor for the A\n. Its not the same factor as the B factor.")
    bank['register_10'].add_field('b_factor', 16, 8, reset_value=0x44)
    bank['register_10'].add_field('en', 30, 1)
    bank['register_10'].reset_value = 0x0044_0000

    bank.add_register('adc_value', 0x00C)

    # register 30
    bank.add_register('register_30', 0x030)
    bank['register_30'].add_field('reg30_a_factor', 0, 8)
    bank['register_30'].add_field('reg30_b_factor', 16, 8, reset_value=0x44)
    bank['register_30'].reset_value = 0x0044_0000

    # param x
    bank.add_register('param_x', 0x020, reset_value=0x0FFF_FFFF, modes='RW')
    bank.add_register('param_x_set', 0x021, 'S')
    bank['param_x_set'].add_linked_field('param_x_set', bank['param_x']['param_x'], 0)
    bank.add_register('param_x_clr', 0x022, modes='CB')
    bank['param_x_clr'].link_to_register(bank['param_x'], 'clr')

    # strobe reg
    bank.add_register('strobe_reg', 0x025, reset_value=0x0000_0000, modes='T')

    # shadow registers
    bank.add_register('shadow_1', address=0x50, modes='BW')
    bank.add_register('shadow_2', address=0x51, modes='RW')
    bank['shadow_2'].add_field('low_byte', 0, 8)
    shdw_grp1 = bank.add_shadow_group('shadow_group_1')
    shdw_grp1.add_element(bank['shadow_1'])
    shdw_grp1.add_element(bank['shadow_2'])

    print()
    print(str(bank))
    print()

    script_location = os.path.dirname(__file__)
    output_folder = f'{script_location}/../../output/test_text'
    generator = lowlevhw.generators.RegbankTextDocGen(bank, output_folder)
    generator.generate()


if __name__ == '__main__':
    main()
