# -----------------------------------------------------------------------------
#  File        : hdl_test_bank.py
#  Brief       : Test script to generate the register bank for the HDL tests.
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2023-10-12
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import os

import lowlevhw


def main() -> None:
    address_width  = 14
    register_width = 32

    bank = lowlevhw.RegisterBank('test_register_bank', address_width, register_width)
    # bank.base_address = 0x400

    # Basic read/write register with reset value
    bank.add_register('register_1', address=0x4, reset_value=0x1122_3344, modes='RW', description="This is register_1. And this text is a test of how the formatting works.")

    # Basic read/write register without reset value (default reset value)
    bank.add_register('register_2', modes='RW')

    # Add read-only register at lower address
    bank.add_register('system_id', 0x0, modes='R', description="System ID, this identifies the system.\n This text tries multiline comments\n The formatting is a bit annoying in python" )

    # Add fixed-value register
    bank.add_register('fw_id', 0x10, modes='B', reset_value=0xBEEF)

    # register 10
    bank.add_register('register_10', 0x014, modes='RW')
    bank['register_10'].add_field('a_factor', 0, 8, description="A factor is the factor for the A\n. Its not the same factor as the B factor.")
    bank['register_10'].add_field('b_factor', 16, 8, reset_value=0x44)
    bank['register_10'].add_field('en', 30, 1)
    bank['register_10'].reset_value = 0x0044_0000

    bank.add_register('adc_value', 0x018)

    # register 30
    bank.add_register('register_30', 0x01C)
    bank['register_30'].add_field('reg30_a_factor', 0, 8)
    bank['register_30'].add_field('reg30_b_factor', 16, 8, reset_value=0x44)
    bank['register_30'].reset_value = 0x0044_0000

    # param x
    bank.add_register('param_x', 0x020, reset_value=0x0FFF_FFFF, modes='RW')
    bank.add_register('param_x_set', 0x024, 'S')
    bank['param_x_set'].add_linked_field('param_x_set', bank['param_x']['param_x'], 0)
    bank.add_register('param_x_clr', 0x028, modes='CB')
    bank['param_x_clr'].link_to_register(bank['param_x'], 'clr')

    # strobe reg
    bank.add_register('strobe_reg', 0x02C, reset_value=0x0000_0000, modes='T')

    # shadow registers
    bank.add_register('shadow_1', address=0x50, modes='BW')
    bank.add_register('shadow_2', address=0x54, modes='RW')
    bank['shadow_2'].add_field('low_byte', 0, 8)
    shdw_grp1 = bank.add_shadow_group('shadow_group_1')
    shdw_grp1.add_element(bank['shadow_1'])
    shdw_grp1.add_element(bank['shadow_2'])

    mgmt_bank = lowlevhw.RegisterBank('mgmt_bank', address_width, register_width)
    mgmt_bank.add_register('id', 0x0000, reset_value=0xAAAA)
    mgmt_bank.add_register('git')

    status_reg = mgmt_bank.add_register('STATUS', 0x20, modes="R")
    status_reg.add_field('FRAME_VALID', 0, 1)
    status_reg.add_field('LINE_VALID', 1, 1)
    status_reg.add_field('PIX_READY', 2, 1)
    status_reg.add_field('OVERFLOW', 4, 1)
    status_reg.add_field('UNDERFLOW', 5, 1)

    version = mgmt_bank.add_register('version', 0x0010)
    version.add_field('PATCH', 0, 8)
    version.add_field('MINOR', 8, 8)
    version.add_field('MAJOR', 16, 16)

    register_address_space = lowlevhw.AddressSpace('register')
    register_address_space.add_element(bank, base=0x1000, name='test_bank_A')
    register_address_space.add_element(bank, base=0x2000, name='test_bank_B')
    register_address_space.add_element(mgmt_bank, 0x0000)

    gentest_device = lowlevhw.DeviceDefinition()
    gentest_device.add_address_space(register_address_space)

    script_location = os.path.dirname(__file__)
    output_folder = os.path.join(script_location, "../../output/test_c_header")
    # bank_generator = lowlevhw.generators.RegisterBankCHeaderGen(bank)
    # bank_generator.generate(output_folder)
    # space_generator = lowlevhw.generators.RegisterSpaceCHeaderGen(register_address_space)
    # space_generator.generate(output_folder)
    device_generator = lowlevhw.generators.DeviceCHeaderGen(gentest_device, output_folder)
    device_generator.generate()

if __name__ == '__main__':
    main()
