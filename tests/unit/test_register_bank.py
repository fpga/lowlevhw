#-------------------------------------------------------------------------------
#  File        : test_register_map.py
#  Brief       : Tests for register_map module
#-------------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2019-07-31
#-------------------------------------------------------------------------------
#  Description :
#-------------------------------------------------------------------------------
#  Copyright (C) 2018 - 2019 European Spallation Source ERIC
#-------------------------------------------------------------------------------

import pytest

import lowlevhw
import lowlevhw.register_bank


@pytest.fixture(name='hw_field', scope='module')
def fixture_hw_field() -> lowlevhw.register_bank.HWField:
    return lowlevhw.register_bank.HWField('test', 0x10, 10)

@pytest.fixture(name='virt_field1', scope='module')
def fixture_virt_field1(hw_field) -> lowlevhw.register_bank.VirtualField:
    return lowlevhw.register_bank.VirtualField('test_virtual1', 20, hw_field, 'R')

@pytest.fixture(name='virt_field2', scope='module')
def fixture_virt_field2(virt_field1) -> lowlevhw.register_bank.VirtualField:
    return lowlevhw.register_bank.VirtualField('test_virtual2', 20, virt_field1, 'W')

@pytest.fixture(name='rw_register')
def fixture_rw_register() -> lowlevhw.Register:
    return lowlevhw.Register('rw_register', 16, 'RW', 0xABCD)

@pytest.fixture(name='multi_field_register')
def fixture_multi_field_register() -> lowlevhw.Register:
    multi_register = lowlevhw.Register('multi_register', 16, 'RW', 0xABCD)
    multi_register.add_field('TEST1', 0, 2)
    multi_register.add_field('TEST2', 10, 4)
    return multi_register

@pytest.fixture(name='test_register_bank')
def fixture_test_register_bank(rw_register, multi_field_register) -> lowlevhw.RegisterBank:
    test_regbank = lowlevhw.RegisterBank('test_regbank', 10, 16)
    test_regbank.add_existing_register(rw_register, 0x10)
    test_regbank.add_existing_register(multi_field_register)
    return test_regbank

# ------------------------------------------------------------------------------
class TestField:

    def test_init(self):
        test_field = lowlevhw.Field('test', 0x10, 'RW')
        assert test_field.name == 'test'
        assert test_field.description == ""
        assert test_field.offset == 0x10
        assert test_field.linked_fields == []
        assert test_field.modes & set('RW')

    @pytest.mark.parametrize('test_case', (
        ('hw_field', True),
        ('virt_field1', False),
        ('virt_field2', True)
        ))
    def test_is_writable(self, test_case, request):
        field = request.getfixturevalue(test_case[0])
        assert field.is_writable() == test_case[1]

    @pytest.mark.parametrize('test_case', (
        ('hw_field', True),
        ('virt_field1', True),
        ('virt_field2', False)
        ))
    def test_is_readable(self, test_case, request):
        field = request.getfixturevalue(test_case[0])
        assert field.is_readable() == test_case[1]

# ------------------------------------------------------------------------------
class TestHWField:

    def test_hwfield_str(self, hw_field):
        assert str(hw_field) == "test                     : 10 bit(s) @ 16                (0x000) - BW "

    def test_hwfield_hwfield(self, hw_field):
        assert hw_field.hw_field is hw_field

    def test_hwfield_parentfield(self, hw_field):
        assert hw_field.parent_field is None

    def test_hwfield_is_implementable(self, hw_field):
        assert hw_field.is_implementable() is True

    def test_field_writes_logic(self, hw_field):
        assert hw_field.writes_logic() is True

    def test_field_reads_logic(self, hw_field):
        assert hw_field.reads_logic() is True


# class VirtualField -----------------------------------------------------------

def test_virtualfield_str(virt_field1, virt_field2):
    assert str(virt_field1) == "test_virtual1            : 10 bit(s) @ 20                (0x000) - R    --> test"
    assert str(virt_field2) == "test_virtual2            : 10 bit(s) @ 20                (0x000) - W    --> test"


def test_virtualfield_parentfield(hw_field, virt_field1, virt_field2):
    assert virt_field1.parent_field is hw_field
    assert virt_field2.parent_field is virt_field1


def test_virtualfield_hwfield(hw_field, virt_field1, virt_field2):
    assert virt_field1.hw_field is hw_field
    assert virt_field2.hw_field is hw_field


def test_virtualfield_is_implementable(virt_field1):
    assert virt_field1.is_implementable() is False

# ------------------------------------------------------------------------------

class TestRegister:

    def test_register_init(self):
        test_register = lowlevhw.Register('test', 16)
        assert test_register.name == 'test'
        assert test_register.width == 16
        assert isinstance(test_register.fields, dict)
        assert len(test_register.fields) == 1
        assert isinstance(test_register.fields['test'], lowlevhw.register_bank.HWField)

    def test_register_set_reset_value(self, rw_register):
        rw_register.reset_value = 0x1234
        assert rw_register.reset_value == 0x1234

    # def test_register_get_fields(rw_register):

    def test_string_representation_single_field(self, rw_register):
        assert str(rw_register) == "rw_register              : AAAAAAAAAAAAAAAA (0xABCD) - RW"

    def test_string_representation_multi_field(self, multi_field_register) -> None:
        test_strings = ['multi_register', '--BBBB--------AA', 'TEST1', 'TEST2', '2 bit', '4 bit', '(0x0)', 'RW']
        for test_string in test_strings:
            assert test_string in str(multi_field_register)

    def test_has_fields(self, rw_register):
        assert rw_register.has_fields() is False

    def test_get_occupation(self, rw_register):
        assert rw_register.get_occupation() == [0] * 16

    def test_graphic_string_no_field(self, rw_register):
        graphic_string = rw_register.graphic_string()
        assert graphic_string == 'AAAAAAAAAAAAAAAA'

    @pytest.mark.parametrize('width', (1, 2, 3, 4, 8, 15))
    def test_graphic_string_field_widths(self, rw_register, width):
        rw_register.add_field('TEST1', 0, width)
        expected_string = ''
        for i in range(16):
            if i < width:
                expected_string += 'A'
            else:
                expected_string += '-'
        expected_string = expected_string[::-1]
        graphic_string = rw_register.graphic_string()
        assert graphic_string == expected_string

    @pytest.mark.parametrize('position', (0, 1, 2, 3, 4, 8, 14))
    def test_graphic_string_field_position(self, rw_register, position):
        width = 2
        rw_register.add_field('TEST1', position, width)
        expected_string = ''
        for i in range(16):
            if i < position:
                expected_string += '-'
            elif i < width+position:
                expected_string += 'A'
            else:
                expected_string += '-'
        expected_string = expected_string[::-1]
        graphic_string = rw_register.graphic_string()
        assert graphic_string == expected_string

    def test_graphic_string_multiple_fields(self, multi_field_register):
        graphic_string = multi_field_register.graphic_string()
        assert graphic_string == '--BBBB--------AA'

    def test_get_fields(self, multi_field_register):
        fields = multi_field_register.get_fields()
        assert len(fields) == 2
        assert fields[0].name == 'TEST1'
        assert fields[1].name == 'TEST2'

    def test_get_fields_incl_reserved(self, multi_field_register: lowlevhw.Register):
        fields = multi_field_register.get_fields_incl_reserved()
        assert len(fields) == 4
        assert fields[0].name == 'TEST1'
        assert fields[1].name == '__reserved_2'
        assert fields[2].name == 'TEST2'
        assert fields[3].name == '__reserved_14'

    def test_register_get_reset_value(self, rw_register):
        assert rw_register.reset_value == 0xABCD

    def test_register_set_modes(self, rw_register):
        rw_register.modes = 'B'
        assert rw_register.modes & set('B')
        for field in rw_register.fields.values():
            assert field.modes & set('B')

    def test_register_get_modes(self, rw_register):
        assert rw_register.modes & set('RW')


# class ShadowGroup ------------------------------------------------------------

def test_shadow_group_init():
    test_shadow_group = lowlevhw.ShadowGroup("sgroup")
    assert test_shadow_group.name == "sgroup"
    assert test_shadow_group.hw_fields == list()


 # -----------------------------------------------------------------------------
class TestRegisterBank:

    def test_init(self):
        test_regbank = lowlevhw.RegisterBank('test_regbank', 10, 16)
        assert test_regbank.name == 'test_regbank'
        assert test_regbank.address_width == 10
        assert test_regbank.data_width == 16
        assert test_regbank.register_map == {}
        assert test_regbank.shadow_groups == []

    def test_get_element_register(self, test_register_bank):
        register = test_register_bank.get_element('rw_register')
        assert isinstance(register, lowlevhw.Register)
        assert register.name == 'rw_register'

    def test_get_element_field(self, test_register_bank):
        field = test_register_bank.get_element('multi_register:TEST1')
        assert isinstance(field, lowlevhw.Field)
        assert field.name == 'TEST1'

    @pytest.mark.parametrize('register_name', (
        {'access': 'rw_register', 'name': 'rw_register'},
        {'access': 'multi_register', 'name': 'multi_register'},
        {'access': 'MULTI_REGISTER', 'name': 'multi_register'},
        {'access': 'Multi_Register', 'name': 'multi_register'}
        ))
    def test_get_register(self, test_register_bank: lowlevhw.RegisterBank, register_name) -> None:
        register = test_register_bank[register_name['access']]
        assert isinstance(register, lowlevhw.Register)
        assert register.name == register_name['name']

    @pytest.mark.parametrize('field_name', ['TEST1', 'test1', 'Test1'])
    def test_get_field(self, test_register_bank: lowlevhw.RegisterBank, field_name: str):
        field = test_register_bank['multi_register'][field_name]
        assert isinstance(field, lowlevhw.Field)
        assert field.name == 'TEST1'

    @pytest.mark.parametrize('register_name', ['RW_REGISTER', 'rw_register', 'RW_Register'])
    def test_get_register_by_name(self, test_register_bank: lowlevhw.RegisterBank, register_name: str) -> None:
        register = test_register_bank.get_register_by_name(register_name)
        assert isinstance(register, lowlevhw.Register)
        assert register.name == 'rw_register'

    def test_get_register_by_name_fail(self, test_register_bank: lowlevhw.RegisterBank):
        test_register_name = 'ab_register'
        failed_register = test_register_bank.get_register_by_name(test_register_name)
        assert failed_register is None

    @pytest.mark.parametrize('test_cases', (
        (0x10, 'rw_register'),
        (0x12, 'multi_register')
    ))
    def test_get_register_by_address(self, test_register_bank: lowlevhw.RegisterBank, test_cases):
        (test_address, name) = test_cases
        register = test_register_bank[test_address]
        assert isinstance(register, lowlevhw.Register)
        assert register.name == name

    def test_get_register_by_address_fail(self, test_register_bank: lowlevhw.RegisterBank):
        test_address = 0x0
        failed_register = test_register_bank[test_address]
        assert failed_register is None

    def test_get_registers(self, test_register_bank):
        register_list = test_register_bank.get_registers()
        assert register_list[0].name == 'rw_register'
        assert register_list[1].name == 'multi_register'

    @pytest.mark.parametrize('test_cases', (
        ('rw_register', 0x10),
        ('multi_register', 0x12)
    ))
    def test_get_address(self, test_register_bank, test_cases):
        (name, expected_address) = test_cases
        address = test_register_bank.get_address(name)
        assert address == expected_address

    def test_get_address_list(self, test_register_bank):
        address_list = test_register_bank.get_address_list()
        assert address_list == [0x10, 0x12]
