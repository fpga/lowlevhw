#------------------------------------------------------------------------------
#  File        : test_dummy_driver.py
#  Brief       : Tests for dummy device driver used for testing
#------------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2019-08-01
#------------------------------------------------------------------------------
#  Description :
#------------------------------------------------------------------------------
#  Copyright (C) 2019 - 2021 European Spallation Source ERIC
#------------------------------------------------------------------------------

import pytest

import lowlevhw.driver


@pytest.fixture(name='test_dummy')
def fixture_test_dummy() -> lowlevhw.driver.Dummy:
    return lowlevhw.driver.Dummy()


def test_dummy_init():
    test_dummy = lowlevhw.driver.Dummy()
    assert test_dummy.registers == {}
    assert test_dummy.memory == {}


def test_dummy_find_device(test_dummy):
    assert test_dummy.find_devices() == 'DUMMY'


def test_dummy_open_device(test_dummy):
    test_dummy.open_device('DUMMY')


def test_dummy_open_device_error(test_dummy):
    with pytest.raises(OSError):
        test_dummy.open_device('OTHER')


def test_dummy_read_register(test_dummy):
    test_dummy.write_register(0x100, 0xABCD)
    assert test_dummy.read_register(0x100) == 0xABCD


def test_dummy_read_register_non_exist(test_dummy):
    assert test_dummy.read_register(0x100) == 0


def test_dummy_write_register(test_dummy):
    test_dummy.write_register(0x100, 0xABCD)
    assert len(test_dummy.registers) == 1
    assert test_dummy.registers[0x100] == 0xABCD


def test_dummy_write_register_rewrite(test_dummy):
    test_dummy.write_register(0x100, 0xABCD)
    assert len(test_dummy.registers) == 1
    assert test_dummy.registers[0x100] == 0xABCD
    test_dummy.write_register(0x100, 0x11)
    assert len(test_dummy.registers) == 1
    assert test_dummy.registers[0x100] == 0x11


def test_dummy_read_ram(test_dummy):
    test_dummy.write_ram(0x100, 10, [0xAB] * 10)
    assert test_dummy.read_ram(0x100, 10) == bytearray([0xAB] * 10)


def test_dummy_read_ram_non_exist(test_dummy):
    assert test_dummy.read_ram(0x100, 1) == b'\x00'


def test_dummy_write_ram(test_dummy):
    test_dummy.write_ram(0x100, 10, [0x66] * 10)
    assert len(test_dummy.memory) == 10
    assert test_dummy.memory[0x100] == 0x66
    assert test_dummy.memory[0x103] == 0x66
    assert test_dummy.memory[0x109] == 0x66
