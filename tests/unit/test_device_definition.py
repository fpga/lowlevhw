# -----------------------------------------------------------------------------
#  File        : test_device_definition.py
#  Brief       :
#  -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2023-10-02
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import pytest

import lowlevhw
import lowlevhw.driver


@pytest.fixture(name='test_register_bank')
def fixture_test_register_bank() -> lowlevhw.RegisterBank:
    main_registers = lowlevhw.RegisterBank('main_registers', 8, 32)
    main_registers.add_register('ID', 0x00, 'R')
    main_registers.add_register('MEM0_BASE', 0x01, 'RW')
    main_registers.add_register('MEM0_SIZE', 0x02, 'RW')

    version_reg = main_registers.add_register('VERSION', 0x10, 'BW')
    version_reg.add_field('MINOR', 0,  8, reset_value=0xAB)
    version_reg.add_field('MAJOR', 8, 24, reset_value=0x123456)

    temp_reg = main_registers.add_register('TEMP', 0x12, 'RW')
    temp_reg.add_field('LOW', 0, 8)
    temp_reg.add_field('MIDDLE', 8, 23)
    temp_reg.add_field('MSB', 32, 1)

    return main_registers


@pytest.fixture(name='test_memory_map')
def fixture_test_memory_map() -> lowlevhw.MemoryMap:
    main_memory = lowlevhw.MemoryMap('main_memory')
    main_memory.add_area(lowlevhw.MemoryAreaDynBaseDynSize(
        'MEM0_AREA', 'MEM0_BASE', 'MEM0_SIZE',
        lowlevhw.RawMemory()
        ))
    return main_memory


@pytest.fixture(name='legacy_definition')
def fixture_legacy_definition(test_register_bank, test_memory_map) -> lowlevhw.DeviceDefinition:
    dev_defintion = lowlevhw.DeviceDefinition()
    dev_defintion.add_register_bank(test_register_bank)
    dev_defintion.add_memory_map(test_memory_map)
    return dev_defintion


@pytest.fixture(name='dev_definition')
def fixture_dev_definition(test_register_bank, test_memory_map) -> lowlevhw.DeviceDefinition:
    reg_map = lowlevhw.AddressSpace('reg')
    reg_map.add_element(test_register_bank, 0x1000)
    ddr_map = lowlevhw.AddressSpace('ddr')
    ddr_map.add_element(test_memory_map)
    device = lowlevhw.DeviceDefinition()
    device.add_address_space(reg_map, transparent=True)
    device.add_address_space(ddr_map, transparent=True)
    return device


@pytest.mark.parametrize('test_input,expected', [
        ('unsplit', ('unsplit', '')),
        ('split1:split2', ('split1', 'split2')),
        ('split1:split2:split3', ('split1', 'split2:split3'))
    ])
def test_split_element_location(test_input, expected):
    splitted = lowlevhw.split_element_location(test_input)
    assert splitted == expected


class TestLegacyUsage:

    def test_reg_address_map_generation(self, test_register_bank):
        device_def = lowlevhw.DeviceDefinition()
        device_def.add_register_bank(test_register_bank)
        address_spaces = device_def.get_address_spaces()
        assert len(address_spaces) == 1
        assert address_spaces[0].name == '--LEGACY-REGISTERS--'

    def test_mem_address_map_generation(self, test_memory_map):
        device_def = lowlevhw.DeviceDefinition()
        device_def.add_memory_map(test_memory_map)
        address_spaces = device_def.get_address_spaces()
        assert len(address_spaces) == 1
        assert address_spaces[0].name == '--LEGACY-MEMORY--'

    def test_register_retrieval(self, legacy_definition):
        id_register = legacy_definition['ID']
        assert isinstance(id_register, lowlevhw.Register)
        assert id_register.name == 'ID'

    def test_field_retrieval(self, legacy_definition):
        minor_field = legacy_definition['VERSION']['MINOR']
        assert isinstance(minor_field, lowlevhw.Field)
        assert minor_field.name == 'MINOR'

    def test_field_retrieval_by_colon_string(self, legacy_definition):
        minor_field = legacy_definition['VERSION:MINOR']
        assert isinstance(minor_field, lowlevhw.Field)
        assert minor_field.name == 'MINOR'

    def test_get_address_of_register(self, legacy_definition):
        address = legacy_definition.get_address('MEM0_BASE')
        assert hex(address) == hex(0x01)

    def test_get_address_of_field(self, legacy_definition):
        address = legacy_definition.get_address('TEMP:LOW')
        assert hex(address) == hex(0x12)

    def test_get_address_of_register_colon_access(self, legacy_definition):
        address = legacy_definition.get_address('MEM0_BASE')
        assert hex(address) == hex(0x01)

    def test_get_address_of_field_colon_access(self, legacy_definition):
        address = legacy_definition.get_address('TEMP:LOW')
        assert hex(address) == hex(0x12)


class TestMappedUsage:

    def test_register_bank_retrieval(self, dev_definition):
        register_bank = dev_definition['main_registers']
        assert register_bank.name == 'main_registers'

    def test_register_retrieval(self, dev_definition):
        register = dev_definition['main_registers']['ID']
        assert register.name == 'ID'

    def test_field_retrieval(self, dev_definition):
        minor_field = dev_definition['main_registers']['VERSION']['MINOR']
        assert isinstance(minor_field, lowlevhw.Field)
        assert minor_field.name == 'MINOR'

    def test_field_retrieval_by_colon_string(self, dev_definition):
        minor_field = dev_definition['main_registers:VERSION:MINOR']
        assert isinstance(minor_field, lowlevhw.Field)
        assert minor_field.name == 'MINOR'

    def test_get_element_field(self, dev_definition):
        minor_field = dev_definition.get_element('reg:main_registers:VERSION:MINOR')
        assert isinstance(minor_field, lowlevhw.Field)
        assert minor_field.name == 'MINOR'

    def test_get_registers(self, dev_definition):
        registers = dev_definition.get_registers()
        expected_registers = [
            '*reg*:main_registers:ID',
            '*reg*:main_registers:MEM0_BASE',
            '*reg*:main_registers:MEM0_SIZE',
            '*reg*:main_registers:VERSION',
            '*reg*:main_registers:TEMP'
        ]
        assert registers == expected_registers

    def test_get_path(self, dev_definition):
        path = dev_definition.get_path('main_registers:ID')
        assert path == 'reg:main_registers:ID'

    def test_get_path_not_existing_register(self, dev_definition):
        with pytest.raises(KeyError) as excinfo:
            _ = dev_definition.get_path('main_registers:NON_EXISTING')
        assert 'NON_EXISTING' in str(excinfo.value)
        assert 'Path' in str(excinfo.value)

    class TestGetAddress:

        def test_register_bank(self, dev_definition):
            address = dev_definition.get_address('main_registers')
            assert hex(address) == hex(0x1000)

        def test_register(self, dev_definition):
            address = dev_definition.get_address('main_registers:MEM0_BASE')
            assert hex(address) == hex(0x1001)

        def test_field(self, dev_definition):
            address = dev_definition.get_address('main_registers:TEMP:LOW')
            assert hex(address) == hex(0x1012)

        def test_register_colon_access(self, dev_definition):
            address = dev_definition.get_address('main_registers:MEM0_BASE')
            assert hex(address) == hex(0x1001)

        def test_field_colon_access(self, dev_definition):
            address = dev_definition.get_address('main_registers:TEMP:LOW')
            assert hex(address) == hex(0x1012)

        def test_register_in_transparent_regbank_in_subspace(self, dev_definition: lowlevhw.DeviceDefinition, test_register_bank):
            sub_space = lowlevhw.AddressSpace('subspace')
            sub_space.add_element(test_register_bank, 0x0, transparent=True)
            dev_definition.get_element('reg').add_element(sub_space, 0x8_0000, 'new_sub_name')
            test_register_address = dev_definition.get_address('new_sub_name:VERSION')
            assert test_register_address == 0x8_0000 + 0x10

    def test_get_address_spaces(self, dev_definition: lowlevhw.DeviceDefinition):
        address_spaces = dev_definition.get_address_spaces()
        assert len(address_spaces) == 2
        assert isinstance(address_spaces[0], lowlevhw.AddressSpace)

    def test_get_address_space(self, dev_definition: lowlevhw.DeviceDefinition):
        ddr_space = dev_definition.get_address_space('ddr')
        assert isinstance(ddr_space, lowlevhw.AddressSpace)
        assert ddr_space.name == 'ddr'

