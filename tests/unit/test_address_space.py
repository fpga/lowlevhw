#-------------------------------------------------------------------------------
#  File        : test_address_map.py
#  Brief       : Tests for address_map module
#-------------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2023-09-29
#-------------------------------------------------------------------------------
#  Description :
#-------------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
#-------------------------------------------------------------------------------

import pytest

import lowlevhw


@pytest.fixture(name='simple_register_map')
def fixture_simple_register_map() -> lowlevhw.AddressSpace:
    test_map = lowlevhw.AddressSpace('registers')
    register_bank = lowlevhw.RegisterBank('test_registers', 16, 32)
    register_bank.add_register('REGISTER_1')
    test_map.add_element(register_bank, 0x1000, 'test_regs1', transparent=False)
    test_map.add_element(register_bank, 0x2000, 'test_regs2', transparent=False)
    return test_map


@pytest.fixture(name='complex_register_map')
def fixture_complex_register_map() -> lowlevhw.AddressSpace:
    register_bank = lowlevhw.RegisterBank('test_registers', 16, 32)
    register_bank.add_register('REGISTER_1')

    register_map = lowlevhw.AddressSpace('test_map')
    register_map.add_element(register_bank, 0x0000, 'sub_regs', transparent=False)

    test_map = lowlevhw.AddressSpace('registers')
    test_map.add_element(register_bank, 0x01000, 'test_regs1', transparent=False)
    test_map.add_element(register_bank, 0x02000, 'test_regs2', transparent=False)
    test_map.add_element(register_map,  0x10000, transparent=False)
    return test_map


@pytest.fixture(name='complex_transparent_register_map')
def fixture_complex_transparent_register_map() -> lowlevhw.AddressSpace:
    register_bank = lowlevhw.RegisterBank('test_registers', 16, 32)
    register_bank.add_register('REGISTER_1')

    register_map = lowlevhw.AddressSpace('test_map')
    register_map.add_element(register_bank, 0x0000, 'sub_regs', transparent=False)

    test_map = lowlevhw.AddressSpace('registers')
    test_map.add_element(register_bank, 0x01000, 'test_regs1', transparent=False)
    test_map.add_element(register_bank, 0x02000, 'test_regs2', transparent=False)
    test_map.add_element(register_map,  0x10000, transparent=True)
    return test_map


def test_address_space_string_empty():
    test_name = 'MySpace'
    address_space = lowlevhw.AddressSpace(test_name)
    address_space_string = str(address_space)
    assert 'Address Space' in address_space_string
    assert test_name in address_space_string
    assert 'NO ELEMENTS' in address_space_string


def test_address_space_string_with_space(simple_register_map):
    address_space_string = str(simple_register_map)
    assert 'Address Space' in address_space_string
    assert simple_register_map.name in address_space_string
    assert 'test_regs1' in address_space_string
    assert 'test_regs2' in address_space_string


def test_access_empty_map():
    empty_map = lowlevhw.AddressSpace('registers')
    with pytest.raises(KeyError) as excinfo:
        _ = empty_map['test']
    assert 'registers' in str(excinfo.value)


def test_access_element_simple_bank(simple_register_map):
    register_bank = simple_register_map['test_regs1']
    assert register_bank.name == 'test_registers'


def test_access_element_complex_bank(complex_register_map):
    register_bank1 = complex_register_map['test_regs1']
    sub_register_bank = complex_register_map['test_map']['sub_regs']
    assert register_bank1.name == 'test_registers'
    assert sub_register_bank.name == 'test_registers'


def test_access_element_transparent_bank(complex_register_map):
    sub_space_element = complex_register_map._elements['test_map']
    sub_space_element.transparent = True
    register_bank1 = complex_register_map['test_regs1']
    sub_register_bank = complex_register_map['sub_regs']
    assert register_bank1.name == 'test_registers'
    assert sub_register_bank.name == 'test_registers'


def test_access_subelement_transparent_bank(complex_register_map):
    sub_space_element = complex_register_map._elements['test_map']
    sub_space_element.transparent = True
    sub_bank_register = complex_register_map['sub_regs:REGISTER_1']
    assert sub_bank_register.name == 'REGISTER_1'


def test_get_element_register_bank(complex_register_map):
    register_bank = complex_register_map.get_element('test_regs1')
    assert isinstance(register_bank, lowlevhw.RegisterBank)
    register_bank.name == 'test_registers'


def test_get_element_address_space(complex_register_map):
    register_bank = complex_register_map.get_element('test_map')
    assert isinstance(register_bank, lowlevhw.AddressSpace)
    register_bank.name == 'test_map'


def test_get_element_sub_register_bank(complex_register_map):
    register_bank = complex_register_map.get_element('test_map:sub_regs')
    assert isinstance(register_bank, lowlevhw.RegisterBank)
    register_bank.name == 'sub_regs'


def test_get_element_register_in_sub_register_bank(complex_register_map):
    register_bank = complex_register_map.get_element('test_map:sub_regs:REGISTER_1')
    assert isinstance(register_bank, lowlevhw.Register)
    register_bank.name == 'REGISTER_1'


def test_get_element_register_in_transparent_sub_register_bank(complex_transparent_register_map):
    register_bank = complex_transparent_register_map.get_element('test_map:sub_regs:REGISTER_1')
    assert isinstance(register_bank, lowlevhw.Register)
    register_bank.name == 'REGISTER_1'


def test_get_registers(complex_transparent_register_map):
    registers = complex_transparent_register_map.get_registers()
    expected_registers = [
       'test_regs1:REGISTER_1',
       'test_regs2:REGISTER_1',
       '*test_map*:sub_regs:REGISTER_1'
    ]
    assert registers == expected_registers


def test_add_element_without_mapping():
    register_map = lowlevhw.AddressSpace('registers')
    register_bank = lowlevhw.RegisterBank('test_registers', 16, 32)
    register_map.add_element(register_bank)
    assert len(register_map._elements) == 1
    assert register_map._elements['test_registers'].name == 'test_registers'


def test_add_element_name_remapping(simple_register_map):
    simple_register_map = lowlevhw.AddressSpace('registers')
    register_bank = lowlevhw.RegisterBank('test_registers', 16, 32)
    simple_register_map.add_element(register_bank, name='test_reg')
    assert simple_register_map._elements['test_reg'].name == 'test_reg'
    assert simple_register_map._elements['test_reg'].definition.name == 'test_registers'


def test_add_element_at_address(simple_register_map):
    test_address = 0x1000
    simple_register_map = lowlevhw.AddressSpace('registers')
    register_bank = lowlevhw.RegisterBank('test_registers', 16, 32)
    simple_register_map.add_element(register_bank, test_address)
    test_register_bank = simple_register_map._elements['test_registers']
    assert test_register_bank.definition.name == 'test_registers'
    assert test_register_bank.base_address == test_address


def test_get_address_of_register_bank():
    test_address = 0x1000
    register_map = lowlevhw.AddressSpace('registers')
    register_bank = lowlevhw.RegisterBank('test_registers', 16, 32)
    register_map.add_element(register_bank, base=test_address)
    register_bank_address = register_map.get_address('test_registers')
    assert register_bank_address == test_address


def test_get_address_of_register():
    test_address = 0x1000
    register_map = lowlevhw.AddressSpace('registers')
    register_bank = lowlevhw.RegisterBank('test_registers', 16, 32)
    register_bank.add_register('TEST_REG', 0x10)
    register_map.add_element(register_bank, base=test_address)
    register_bank_address = register_map.get_address('test_registers:TEST_REG')
    assert register_bank_address == (test_address + 0x10)


def test_get_path_not_transparent_space(complex_register_map):
    test_element = 'test_map:sub_regs:REGISTER_1'
    path = complex_register_map.get_path(test_element)
    assert path == ['test_map:sub_regs:REGISTER_1']


def test_get_path_transparent_space(complex_transparent_register_map):
    test_element = 'sub_regs:REGISTER_1'
    path = complex_transparent_register_map.get_path(test_element)
    assert path == ['test_map:sub_regs:REGISTER_1']


def test_get_path_non_existing_register(complex_transparent_register_map):
    test_element = 'sub_regs:REGISTER_8'
    path = complex_transparent_register_map.get_path(test_element)
    assert path == []


def test_get_path_non_existing_space(complex_transparent_register_map):
    test_element = 'not_existing'
    path = complex_transparent_register_map.get_path(test_element)
    assert path == []


def test_get_path_in_register_bank(complex_transparent_register_map):
    test_element = 'test_regs1:REGISTER_1'
    path = complex_transparent_register_map.get_path(test_element)
    assert path == ['test_regs1:REGISTER_1']