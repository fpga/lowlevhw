# -----------------------------------------------------------------------------
#  File        : test_helpers.py
#  Brief       :
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2020-07-01
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2020 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import random

import numpy as np
import pytest

from lowlevhw.testing import assert_waveform_equal
from lowlevhw.testing import assert_waveform_allclose
from lowlevhw.testing import assert_frequency


class TestAssertWaveformEqual:

    def test_equal(self):
        test_data = list(range(1000))
        comp_data = test_data.copy()
        assert_waveform_equal(test_data, comp_data)

    def test_short_equal(self):
        test_data = list(range(5))
        comp_data = test_data.copy()
        assert_waveform_equal(test_data, comp_data)

    @pytest.mark.parametrize('offset', range(-5, 0))
    def test_shifted_neg(self, offset):
        test_data = list(range(1000))
        comp_data = test_data.copy()
        test_data = test_data[-offset:] + ([2] * -offset)
        with pytest.raises(AssertionError) as assertinfo:
            assert_waveform_equal(test_data, comp_data)
        assert f"{offset} samples" in str(assertinfo.value)

    @pytest.mark.parametrize('offset', range(1, 5))
    def test_shifted_pos(self, offset):
        test_data = list(range(1000))
        comp_data = test_data.copy()
        test_data = ([2] * offset) + test_data[:-offset]
        with pytest.raises(AssertionError) as assertinfo:
            assert_waveform_equal(test_data, comp_data)
        assert f" {offset} samples" in str(assertinfo.value)

    def test_notequal(self):
        test_data = list(range(1000))
        comp_data = [random.randint(1,30) for _ in range(1000)]
        with pytest.raises(AssertionError):
            assert_waveform_equal(test_data, comp_data)


class TestAssertWaveformAllclose:

    def test_equal(self):
        test_data = list(range(1000))
        comp_data = test_data.copy()
        assert_waveform_allclose(test_data, comp_data)

    def test_almost_equal(self):
        test_data = np.arange(0, 1000, 1.0)
        comp_data = test_data.copy()
        # Alter the test data
        test_data[0] = test_data[0] + 0.01
        test_data[10] = test_data[10] + 0.01
        test_data[100] = test_data[100] - 0.01
        assert_waveform_allclose(test_data, comp_data, atol=0.02)

    def test_short_equal(self):
        test_data = list(range(5))
        comp_data = test_data.copy()
        assert_waveform_equal(test_data, comp_data)

    @pytest.mark.parametrize('offset', range(-5, 0))
    def test_shifted_neg(self, offset):
        test_data = list(range(1000))
        comp_data = test_data.copy()
        test_data = [x + random.randint(-100,100)/10000 for x in test_data]
        test_data = test_data[-offset:] + ([2] * -offset)
        with pytest.raises(AssertionError) as assertinfo:
            assert_waveform_allclose(test_data, comp_data, atol=0.2)
        assert f"{offset} samples" in str(assertinfo.value)

    @pytest.mark.parametrize('offset', range(-5, 0))
    def test_shifted_neg_tol(self, offset):
        shift_tolerance = 3
        test_data = list(range(1000))
        comp_data = test_data.copy()
        test_data = [x + random.randint(-100,100)/10000 for x in test_data]
        test_data = test_data[-offset:] + ([2] * -offset)
        if abs(offset) > shift_tolerance:
            with pytest.raises(AssertionError) as assertinfo:
                assert_waveform_allclose(test_data, comp_data, atol=0.2, shifttol=shift_tolerance)
                assert f"{offset} samples" in str(assertinfo.value)
        else:
            assert_waveform_allclose(test_data, comp_data, atol=0.2, shifttol=shift_tolerance)

    @pytest.mark.parametrize('offset', range(1, 6))
    def test_shifted_pos(self, offset):
        test_data = list(range(1000))
        comp_data = test_data.copy()
        test_data = [x + random.randint(-100,100)/10000 for x in test_data]
        test_data = ([2] * offset) + test_data[:-offset]
        with pytest.raises(AssertionError) as assertinfo:
            assert_waveform_allclose(test_data, comp_data, atol=0.2)
        assert f" {offset} samples" in str(assertinfo.value)

    @pytest.mark.parametrize('offset', range(1, 6))
    def test_shifted_pos_tol(self, offset):
        shift_tolerance = 3
        test_data = list(range(1000))
        comp_data = test_data.copy()
        test_data = [x + random.randint(-100,100)/10000 for x in test_data]
        test_data = ([2] * offset) + test_data[:-offset]
        if abs(offset) > shift_tolerance:
            with pytest.raises(AssertionError) as assertinfo:
                assert_waveform_allclose(test_data, comp_data, atol=0.2, shifttol=shift_tolerance)
                assert f"{offset} samples" in str(assertinfo.value)
        else:
            assert_waveform_allclose(test_data, comp_data, atol=0.2, shifttol=shift_tolerance)

    def test_wrong(self):
        test_data = list(range(1000))
        comp_data = test_data.copy()
        test_data = [x + random.randint(-400,400)/10000 for x in test_data]
        with pytest.raises(AssertionError):
            assert_waveform_equal(test_data, comp_data)


class TestAssertFrequency:

    @pytest.mark.parametrize('frequency', [0.001, 0.01, 0.1, 0.5, 1, 1.232123, 1.252123, 2, 5, 8])
    def test_notol_ok_no_phase(self, frequency):
        f_sampling = 1000
        t = np.linspace(0, 10, 10*f_sampling+1, endpoint=True)
        sample_data = np.sin(2*np.pi*frequency*t)
        assert_frequency(sample_data, frequency, f_sampling)

    @pytest.mark.parametrize('phase', [0, 0.01, 0.1, 1.0, np.pi, 2*np.pi])
    def test_notol_ok_phase(self, phase):
        frequency = 10
        f_sampling = 1000
        t = np.linspace(0, 10, 10*f_sampling+1, endpoint=True)
        sample_data = np.sin(2*np.pi*frequency*t+phase)
        assert_frequency(sample_data, frequency, f_sampling)

    @pytest.mark.parametrize('frequency', [0.1, 1])
    def test_notol_wrong(self, frequency):
        f_sampling = 1000
        t = np.linspace(0, 10, 10*f_sampling+1, endpoint=True)
        sample_data = np.sin(2*np.pi*frequency*t)
        with pytest.raises(AssertionError):
            assert_frequency(sample_data, frequency+10, f_sampling)
