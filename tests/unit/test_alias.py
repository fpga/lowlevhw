# -----------------------------------------------------------------------------
#  File        : test_dummy_driver.py
#  Brief       : Tests for dummy device driver used for testing
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2019-08-01
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import pytest

import lowlevhw
import lowlevhw.driver


@pytest.fixture(name='test_memory_map')
def fixture_test_memory_map() -> lowlevhw.MemoryMap:
    test_memmap = lowlevhw.MemoryMap('TEST_MAP')
    test_memmap.add_area(lowlevhw.MemoryAreaFixed('DATA1', 0x1000, 0x100))
    test_memmap.add_alias('DATA1_INTERN', 'DATA1', lowlevhw.MultiByteElements(2, lowlevhw.IntegerFormat(2)))
    return test_memmap


class TestMappedUsage:

    @pytest.fixture
    def test_device(self, test_memory_map) -> lowlevhw.Device:
        mem_map = lowlevhw.AddressSpace('mem')
        mem_map.add_element(test_memory_map, base=0x0000)
        dummy_definition = lowlevhw.DeviceDefinition()
        dummy_definition.add_address_space(mem_map, transparent=True)
        dummy_definition.add_alias('DATA1_ALIAS', 'TEST_MAP:DATA1')
        dummy_definition.add_alias('DATA1_FMT', 'TEST_MAP:DATA1', lowlevhw.MultiByteElements(2, lowlevhw.IntegerFormat(2)))

        dummy_device = lowlevhw.Device(dummy_definition, lowlevhw.driver.Dummy())
        dummy_device.connect('DUMMY')
        dummy_device['TEST_MAP']['DATA1'].write([1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0])
        return dummy_device

    def test_global_aliases_created(self, test_device):
        assert isinstance(test_device.definition._aliases[0], lowlevhw.ElementAlias)
        assert isinstance(test_device.definition._aliases[1], lowlevhw.ElementAlias)
        assert len(test_device.definition._aliases) == 2

    def test_access_alias(self, test_device):
        aliased_area = test_device['DATA1_ALIAS']
        assert isinstance(aliased_area, lowlevhw.DeviceElementAlias)

    def test_alias_read_raw(self, test_device):
        original_data = test_device['TEST_MAP']['DATA1'].read_raw()
        aliased_data = test_device['DATA1_ALIAS'].read_raw()
        assert original_data == aliased_data

    def test_alias_read_std_format(self, test_device):
        original_data = test_device['TEST_MAP']['DATA1'].read()
        aliased_data = test_device['DATA1_ALIAS'].read()
        assert original_data == aliased_data

    def test_alias_read_std_format_with_len(self, test_device):
        original_data = test_device['TEST_MAP']['DATA1'].read(10)
        aliased_data = test_device['DATA1_ALIAS'].read(10)
        assert original_data == aliased_data

    def test_alias_read_formated(self, test_device):
        aliased_data = test_device['DATA1_FMT'].read()
        assert aliased_data[0:2] == [1, 2]

    def test_alias_read_formated_with_len(self, test_device):
        aliased_data = test_device['DATA1_FMT'].read(4)
        assert aliased_data == [1, 2]

    def test_alias_write_raw(self, test_device):
        test_data = [255, 254, 253, 252, 251]
        test_device['DATA1_ALIAS'].write_raw(test_data)
        assert test_device['DATA1_ALIAS'].read_raw()[0:len(test_data)] == bytearray(test_data)

    def test_alias_write_std_format(self, test_device):
        test_data = [255, 254, 253, 252, 251]
        test_device['DATA1_ALIAS'].write(test_data)
        assert test_device['DATA1_ALIAS'].read()[0:len(test_data)] == test_data

    def test_alias_write_formated(self, test_device):
        test_data = [255, 254, 253, 252, 251]
        test_device['DATA1_FMT'].write(test_data)
        assert test_device['DATA1_FMT'].read()[0:len(test_data)] == test_data


class TestLegacyUsage:

    @pytest.fixture
    def test_device(self, test_memory_map) -> lowlevhw.Device:
        dummy_definition = lowlevhw.DeviceDefinition()
        dummy_definition.add_memory_map(test_memory_map)
        dummy_definition.add_alias('DATA1_ALIAS', 'DATA1')
        dummy_definition.add_alias('DATA1_FMT', 'DATA1', lowlevhw.MultiByteElements(2, lowlevhw.IntegerFormat(2)))
        dummy_device = lowlevhw.Device(dummy_definition, lowlevhw.driver.Dummy())
        dummy_device.connect('DUMMY')
        dummy_device['DATA1'].write([1, 0, 2, 0, 3, 0, 4, 0, 5, 0, 6, 0])
        return dummy_device

    def test_global_aliases_created(self, test_device):
        assert isinstance(test_device.definition._aliases[0], lowlevhw.ElementAlias)
        assert isinstance(test_device.definition._aliases[1], lowlevhw.ElementAlias)
        assert len(test_device.definition._aliases) == 2

    def test_access_alias(self, test_device):
        aliased_area = test_device['DATA1_ALIAS']
        assert isinstance(aliased_area, lowlevhw.DeviceElementAlias)

    # def test_memmap_access_alias(self, test_device):
    #     aliased_area = test_device['DATA1_INTERN']
    #     assert isinstance(aliased_area, lowlevhw.DeviceElementAlias)

    def test_alias_read_raw(self, test_device):
        original_data = test_device['DATA1'].read_raw()
        aliased_data = test_device['DATA1_ALIAS'].read_raw()
        assert original_data == aliased_data

    def test_alias_read_std_format(self, test_device):
        original_data = test_device['DATA1'].read()
        aliased_data = test_device['DATA1_ALIAS'].read()
        assert original_data == aliased_data

    def test_alias_read_std_format_with_len(self, test_device):
        original_data = test_device['DATA1'].read(10)
        aliased_data = test_device['DATA1_ALIAS'].read(10)
        assert original_data == aliased_data

    def test_alias_read_formated(self, test_device):
        aliased_data = test_device['DATA1_FMT'].read()
        assert aliased_data[0:2] == [1, 2]

    def test_alias_read_formated_with_len(self, test_device):
        aliased_data = test_device['DATA1_FMT'].read(4)
        assert aliased_data == [1, 2]

    def test_alias_write_raw(self, test_device):
        test_data = [255, 254, 253, 252, 251]
        test_device['DATA1_ALIAS'].write_raw(test_data)
        assert test_device['DATA1_ALIAS'].read_raw()[0:len(test_data)] == bytearray(test_data)

    def test_alias_write_std_format(self, test_device):
        test_data = [255, 254, 253, 252, 251]
        test_device['DATA1_ALIAS'].write(test_data)
        assert test_device['DATA1_ALIAS'].read()[0:len(test_data)] == test_data

    def test_alias_write_formated(self, test_device):
        test_data = [255, 254, 253, 252, 251]
        test_device['DATA1_FMT'].write(test_data)
        assert test_device['DATA1_FMT'].read()[0:len(test_data)] == test_data

    # def test_memmap_alias_read(self, test_device):
    #     original_data = test_device['DATA1'].read_raw()
    #     aliased_data = test_device['DATA1_INTERN'].read_raw()
    #     assert original_data == aliased_data
