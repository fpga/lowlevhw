# -----------------------------------------------------------------------------
#  File        : test_helpers.py
#  Brief       : Test cases to test functions of lowlevhw.helpers
#  -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2023-09-27
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import pytest

import lowlevhw
import lowlevhw.helpers


class TestFixedFloatConversion:

    @pytest.mark.parametrize("test_input, int_part, frac_part, expected",[
            (1.25, 3, 4, 0b001_0100),        # test positive value
            (-1.25, 3, 4, 0b110_1011 + 1),   # test negative value
            (0, 4, 2, 0b0000_00)             # test zero
        ])
    def test_to_fixed_default(self, test_input, int_part, frac_part, expected):
        q3_4_result = lowlevhw.helpers.float_to_fixed(test_input, int_part, frac_part)
        assert bin(q3_4_result) == bin(expected)


    @pytest.mark.parametrize("test_input, int_part, frac_part, expected",[
            (1.25, 3, 4, 0b001_0100),        # test positive value
            (-1.25, 3, 4, 0b110_1011 + 1),   # test negative value
            (0, 4, 2, 0b0000_00)             # test zero
        ])
    def test_to_fixed_signed(self, test_input, int_part, frac_part, expected):
        q3_4_result = lowlevhw.helpers.float_to_fixed(test_input, int_part, frac_part, signed=True)
        assert bin(q3_4_result) == bin(expected)


    @pytest.mark.parametrize("test_input, int_part, frac_part, expected",[
            (1.25, 3, 4, 0b001_0100),        # test positive value
            (6.6875, 3, 4, 0b110_1011),      # test negative value
            (0, 4, 2, 0b0000_00)             # test zero
        ])
    def test_to_fixed_unsigned(self, test_input, int_part, frac_part, expected):
        q3_4_result = lowlevhw.helpers.float_to_fixed(test_input, int_part, frac_part, signed=False)
        assert bin(q3_4_result) == bin(expected)


    @pytest.mark.parametrize("bin_input, int_part, frac_part, expected",[
            (0b001_0100, 3, 4, 1.25),       # test positive value
            (0b110_1011+1, 3, 4, -1.25),    # test negative value
            (0b00_00, 2, 2, 0.0)            # test zero
        ])
    def test_to_float_default(self, bin_input, int_part, frac_part, expected):
        float_result = lowlevhw.helpers.fixed_to_float(bin_input, int_part, frac_part)
        assert float_result == expected


    @pytest.mark.parametrize("bin_input, int_part, frac_part, expected",[
            (0b001_0100, 3, 4, 1.25),       # test positive value
            (0b110_1011+1, 3, 4, -1.25),    # test negative value
            (0b00_00, 2, 2, 0.0)            # test zero
        ])
    def test_to_float_signed(self, bin_input, int_part, frac_part, expected):
        float_result = lowlevhw.helpers.fixed_to_float(bin_input, int_part, frac_part, signed=True)
        assert float_result == expected


    @pytest.mark.parametrize("bin_input, int_part, frac_part, expected",[
            (0b001_0100, 3, 4, 1.25),       # test positive value
            (0b110_1011, 3, 4, 6.6875),     # test large value, usually negative
            (0b00_00, 2, 2, 0.0)            # test zero
        ])
    def test_to_float_unsigned(self, bin_input, int_part, frac_part, expected):
        float_result = lowlevhw.helpers.fixed_to_float(bin_input, int_part, frac_part, signed=False)
        assert float_result == expected


@pytest.mark.parametrize("float_input, int_part, frac_part, expected",[
        (1.250001, 3, 4, 1.25),
        (1.249999, 3, 4, 1.1875),
        (-1.250001, 3, 4, -1.3125),
        (-1.249999, 3, 4, -1.25),
        (0.0, 2, 2, 0.0)             # test zero
    ])
def test_quantize_signed(float_input, int_part, frac_part, expected):
    quant_result = lowlevhw.helpers.quantize(float_input, int_part, frac_part, signed=True)
    assert quant_result == expected


class TestByteArrayToHex:

    def test_correct_type(self):
        test_array = bytearray((0xA, 0xB, 0xC, 0xD))
        hex_string = lowlevhw.helpers.bytearray_to_hexstr(test_array)
        assert isinstance(hex_string, str), "Result of bytearray_to_hexstr() must be a string"

    def test_single_bytes(self):
        test_array = bytearray((0xA, 0xB, 0xC, 0xD))
        hex_string = lowlevhw.helpers.bytearray_to_hexstr(test_array)
        assert hex_string == "0x0A 0x0B 0x0C 0x0D"

    def test_multidigit_big_endian(self):
        test_array = bytearray((0xA, 0xB, 0xC, 0xD))
        hex_string = lowlevhw.helpers.bytearray_to_hexstr(test_array, 4, 'big')
        assert hex_string == "0x0A0B0C0D"

    def test_multidigit_little_endian(self):
        test_array = bytearray((0xA, 0xB, 0xC, 0xD))
        hex_string = lowlevhw.helpers.bytearray_to_hexstr(test_array, 4)
        assert hex_string == "0x0D0C0B0A"


@pytest.mark.parametrize("width, mask", [
        (0, 0b0),
        (1, 0b1),
        (2, 0b11),
        (3, 0b111),
        (4, 0b1111),
        (7, 0b111_1111),
        (8, 0b1111_1111),
        (9, 0b1_1111_1111),
        (16, 0b1111_1111_1111_1111),
        (32, 0b1111_1111_1111_1111_1111_1111_1111_1111)
    ])
def test_maks_bits(width, mask):
    result = lowlevhw.helpers.mask_bits(width)
    assert result == mask


@pytest.mark.parametrize("offset, width, extracted_bits", [
        (0, 0, 0b0),
        (0, 1, 0b0),
        (0, 8, 0x78),
        (1, 8, 0b0011_1100),
        (8, 8, 0x56),
        (15, 1, 0b0),
        (14, 1, 0b1)
    ])
def test_extract_part(offset, width, extracted_bits):
    test_input = 0x5678
    result = lowlevhw.helpers.extract_part(test_input, offset, width)
    assert result == extracted_bits
