# -----------------------------------------------------------------------------
#  File        : test_memory_map.py
#  Brief       : Class representing the memory map of a hardware device.
#  -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2019-07-30
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import pytest

import lowlevhw


class TestRawFormat:

    def test_to_raw(self):
        raw_formater = lowlevhw.RawFormat()
        formated_data = raw_formater.to_raw(10)
        assert isinstance(formated_data , bytearray)
        assert formated_data == b'\x0A'

    def test_from_raw(self):
        raw_formater = lowlevhw.RawFormat()
        formated_data = raw_formater.from_raw(23)
        assert formated_data == 23


class TestIntegerFormat:

    def test_init_default(self):
        int_formater = lowlevhw.IntegerFormat(4)
        assert int_formater.width == 4
        assert int_formater.signed is True
        assert int_formater.byteorder == 'little'

    def test_init_specific(self):
        int_formater = lowlevhw.IntegerFormat(3, False, 'big')
        assert int_formater.width == 3
        assert int_formater.signed is False
        assert int_formater.byteorder == 'big'

    def test_to_raw(self):
        int_formater = lowlevhw.IntegerFormat(2, False, 'little')
        assert int_formater.to_raw(512) == b'\x00\x02'

    def test_from_raw(self):
        int_formater = lowlevhw.IntegerFormat(4, False, 'big')
        assert int_formater.from_raw(b'\x00\x00\x01\x01') == 257


class TestFixedPointFormat:

    def test_init(self):
        fixp_formater = lowlevhw.FixedPointFormat(2, 16)
        assert fixp_formater.int_bits == 2
        assert fixp_formater.frac_bits == 16
        assert fixp_formater.width == 3
        assert fixp_formater.signed is True
        assert fixp_formater.byteorder == 'little'

    @pytest.mark.parametrize("formated_data, expected",[
            (1.25, b'\x40\x01'),     # test positive value
            (-1.25, b'\xc0\xfe'),    # test negative value
            (0, b'\x00\x00'),        # test zero
        ])
    def test_toraw(self, formated_data, expected):
        fixp_formater = lowlevhw.FixedPointFormat(8, 8)
        assert fixp_formater.to_raw(formated_data) == expected

    @pytest.mark.parametrize("raw_data, expected",[
            (b'\x40\x01', 1.25),     # test positive value
            (b'\xc0\xfe', -1.25),    # test negative value
            (b'\x00\x00', 0),        # test zero
        ])
    def test_fromraw(self, raw_data, expected):
        fixp_formater = lowlevhw.FixedPointFormat(8, 8)
        assert fixp_formater.from_raw(raw_data) == expected


class TestFloatFormat:

    def test_to_float(self):
        float_formater = lowlevhw.FloatFormat()
        assert float_formater.to_raw(23.943213) == b'\xb3\x8b\xbf\x41'

    def test_from_float(self):
        float_formater = lowlevhw.FloatFormat()
        assert float_formater.from_raw(b'\x41\xcd\x88\xd3') == -1175118282752

class TestMultiByteElements:

    def test_init(self):
        tuple_organization = lowlevhw.MultiByteElements(2)
        assert tuple_organization.element_len == 2
        assert isinstance(tuple_organization.element_converter, lowlevhw.RawFormat)


class TestTupleElements:

    @pytest.fixture(scope="class")
    def test_data(self):
        return [(1, 2, 3), (4, 5, 6), (7, 8, 9), (10, 11, 12)]

    @pytest.fixture(scope="class")
    def test_data_raw(self, test_data):
        flat_test_data = [item for comp_tuple in test_data for item in comp_tuple]
        return bytearray(flat_test_data)

    def test_init(self):
        tuple_organization = lowlevhw.TupleElements(2, 3)
        assert tuple_organization.element_len == 2
        assert tuple_organization.element_nr == 3
        assert isinstance(tuple_organization.element_converter, lowlevhw.RawFormat)

    def test_to_raw(self, test_data, test_data_raw):
        tuple_organization = lowlevhw.TupleElements(1, 3)
        raw_data = tuple_organization.to_raw(test_data)
        assert len(raw_data) == 12
        assert raw_data == bytearray(test_data_raw)

    def test_from_raw_fitting_elements(self, test_data, test_data_raw):
        tuple_organization = lowlevhw.TupleElements(1, 3)
        unpacked_data = tuple_organization.from_raw(test_data_raw)
        assert len(unpacked_data) == 4
        assert len(unpacked_data[0]) == 3
        assert unpacked_data == test_data

    def test_from_raw_non_fitting_elements(self, test_data):
        tuple_organization = lowlevhw.TupleElements(1, 3)
        flat_test_data = [item for comp_tuple in test_data for item in comp_tuple]
        raw_data = bytearray(flat_test_data)
        # addional element that should be ignored
        raw_data.extend(bytearray([34]))
        unpacked_data = tuple_organization.from_raw(raw_data)
        assert len(unpacked_data) == 4
        assert len(unpacked_data[0]) == 3
        assert unpacked_data == test_data

    def test_from_raw_multibyte(self, test_data_raw):
        tuple_organization = lowlevhw.TupleElements(2, 2)
        unpacked_data = tuple_organization.from_raw(test_data_raw)
        assert len(unpacked_data) == 3
        assert len(unpacked_data[0]) == 2
        assert unpacked_data == [([1, 2], [3, 4]), ([5, 6], [7, 8]), ([9, 10], [11, 12])]

    def test_from_raw_multi_converter(self, test_data, test_data_raw):
        tuple_organization = lowlevhw.TupleElements(1, 3, element_converter=(
            lowlevhw.RawFormat(), lowlevhw.RawFormat(), lowlevhw.RawFormat())
        )
        unpacked_data = tuple_organization.from_raw(test_data_raw)
        assert len(unpacked_data) == 4
        assert len(unpacked_data[0]) == 3
        assert unpacked_data == test_data
