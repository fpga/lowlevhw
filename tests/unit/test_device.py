# -----------------------------------------------------------------------------
#  File        : test_device.py
#  Brief       :
#  -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2021-03-26
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2021 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import typing

import pytest

import lowlevhw
import lowlevhw.driver


@pytest.fixture(name='simple_register_bank')
def fixture_simple_register_bank() -> lowlevhw.RegisterBank:
    main_registers = lowlevhw.RegisterBank('main_registers', 8, 32, element_addressing=True)
    main_registers.add_register('ID', 0x00, 'R')
    main_registers.add_register('MEM0_BASE', 0x01, 'BW')
    main_registers.add_register('MEM0_SIZE', 0x02, 'BW')

    version_reg = main_registers.add_register('VERSION', 0x10, 'BW')
    version_reg.add_field('MINOR', 0,  8, reset_value=0xAB)
    version_reg.add_field('MAJOR', 8, 24, reset_value=0x123456)

    temp_reg = main_registers.add_register('TEMP', 0x12, 'RW')
    temp_reg.add_field('LOW', 0, 8)
    temp_reg.add_field('MIDDLE', 8, 23)
    temp_reg.add_field('MSB', 32, 1)

    return main_registers

@pytest.fixture(name='lower_register_bank')
def fixture_lower_register_bank() -> lowlevhw.RegisterBank:
    low_registers = lowlevhw.RegisterBank('lower_registers', 8, 32, element_addressing=True)
    low_registers.add_register('LOW_ID', 0x01, 'R')

    version_reg = low_registers.add_register('VERSION', 0x10, 'BW')
    version_reg.add_field('MINOR', 0,  8, reset_value=0x89)
    version_reg.add_field('MAJOR', 8, 24, reset_value=0xABCDEF)

    return low_registers

class TestMappedUsage:

    @pytest.fixture
    def test_memory_map(self) -> lowlevhw.MemoryMap:
        main_memory = lowlevhw.MemoryMap('main_memory')
        main_memory.add_area(lowlevhw.MemoryAreaDynBaseDynSize(
                'MEM0_AREA', 'main_registers:MEM0_BASE', 'main_registers:MEM0_SIZE', lowlevhw.RawMemory()
            ))
        main_memory.add_area(lowlevhw.MemoryAreaFixed('FIXED_MEM_AREA', 0x10000, 0x1000))
        return main_memory

    @pytest.fixture
    def test_device_reset(self, simple_register_bank, lower_register_bank, test_memory_map):
        reg_map = lowlevhw.AddressSpace('reg')
        reg_map.add_element(simple_register_bank, base=0x1000)
        reg_map.add_element(lower_register_bank,  base=0x0100)
        mem_map = lowlevhw.AddressSpace('mem')
        mem_map.add_element(test_memory_map, base=0x2000)
        test_dev_defintion = lowlevhw.DeviceDefinition()
        test_dev_defintion.add_address_space(reg_map, transparent=True)
        test_dev_defintion.add_address_space(mem_map, transparent=True)

        test_device = lowlevhw.Device(test_dev_defintion, lowlevhw.driver.Dummy(test_dev_defintion))
        test_device.connect('DUMMY')

        yield test_device
        test_device.disconnect()

    @pytest.fixture
    def test_device(self, test_device_reset):
        test_device_reset['main_registers']['MEM0_BASE'].write(0x100)
        test_device_reset['main_registers']['MEM0_SIZE'].write(5)
        yield test_device_reset

    @pytest.fixture
    def memory_data(self, test_device) -> typing.List[int]:
        test_data = range(6)
        test_data = list(test_data[1:6])
        test_data_extended = list(test_data) + ([0]*5)
        test_device.driver.write_ram(0x2100, 5, test_data_extended)
        return test_data

    class TestDeviceClass:

        def test_register_bank_access(self, test_device, simple_register_bank):
            regbank = test_device['main_registers']
            assert isinstance(regbank, lowlevhw.DeviceRegisterBank)
            assert regbank.definition.name == simple_register_bank.name

        def test_memory_map_access(self, test_device, test_memory_map):
            regbank = test_device['main_memory']
            assert isinstance(regbank, lowlevhw.DeviceMemoryMap)
            assert regbank.definition.name == test_memory_map.name

        def test_failed_access(self, test_device):
            with pytest.raises(KeyError) as exception_info:
                _ = test_device['NOT_EXISTING_REGISTER']
            assert 'NOT_EXISTING_REGISTER' in str(exception_info.value), "Name of missing register is not reported"
            assert "Element <" in str(exception_info.value)

        def test_read_register_in_subspace(self, test_device, simple_register_bank):
            sub_space = lowlevhw.AddressSpace('subspace')
            sub_space.add_element(simple_register_bank, 0x0, 'subbank', transparent=True)
            test_device.definition.get_element('reg').add_element(sub_space, 0x8_0000, 'new_sub_name')
            test_device[0x8_0000+0x12].write(0xBEEF)
            test_data = test_device['new_sub_name:TEMP'].read()
            assert hex(test_data) == hex(0xBEEF)


    class TestDeviceRegisterBank:

        @pytest.fixture
        def main_register_bank(self, test_device):
            return test_device['main_registers']

        @pytest.mark.parametrize('register_name', ['VERSION', 'version', 'VeRsIOn'])
        def test_register_name_access(self, main_register_bank: lowlevhw.DeviceRegisterBank, register_name: str) -> None:
            version_register = main_register_bank[register_name]
            assert isinstance(version_register, lowlevhw.DeviceRegister)
            assert version_register.definition.name == 'VERSION'

        def test_register_access_by_colon_string(self, test_device):
            version_register = test_device['main_registers:VERSION']
            assert isinstance(version_register, lowlevhw.DeviceRegister)
            assert version_register.definition.name == 'VERSION'

        def test_register_name_access_in_sub_space(self, test_device):
            test_device.definition._root_space._elements['reg'].transparent = False
            version_register = test_device['reg']['main_registers']['VERSION']
            assert version_register.definition.name == 'VERSION'

        def test_register_name_colon_access_in_sub_space(self, test_device):
            test_device.definition._root_space._elements['reg'].transparent = False
            version_register = test_device['reg:main_registers:VERSION']
            assert version_register.definition.name == 'VERSION'

        def test_register_name_access_in_transparent_bank(self, test_device):
            test_device.definition._root_space._elements['reg'].transparent = False
            test_device.definition['reg']._elements['main_registers'].transparent = True
            version_register = test_device['reg']['VERSION']
            assert version_register.definition.name == 'VERSION'

        def test_base_address(self, main_register_bank) -> None:
            assert main_register_bank.base_address == 0x1000


    class TestDeviceRegister:

        @pytest.fixture(name='r_register_version')
        def fixture_r_register_version(self, test_device) -> lowlevhw.DeviceRegister:
            return test_device['main_registers']['VERSION']

        @pytest.fixture(name='rw_register')
        def fixture_rw_register(self, test_device)-> lowlevhw.DeviceRegister:
            return test_device['main_registers']['TEMP']

        @pytest.mark.parametrize('field_name', ['MINOR', 'minor', 'MinoR'])
        def test_field_name_access(self, r_register_version, field_name) -> None:
            minor_field = r_register_version[field_name]
            assert isinstance(minor_field, lowlevhw.DeviceField)
            assert minor_field.definition.name == 'MINOR'

        def test_field_access_by_colon_string(self, test_device):
            minor_field = test_device['main_registers:VERSION:MINOR']
            assert isinstance(minor_field, lowlevhw.DeviceField)
            assert minor_field.definition.name == 'MINOR'

        def test_field_acess_fail(self, test_device: lowlevhw.Device) -> None:
            with pytest.raises(KeyError) as exception_info:
                test_device['main_registers']['ID']['NOT_EXISTING_FIELD'].read()
            assert "<NOT_EXISTING_FIELD>" in str(exception_info.value), "Name of missing field is not reported"
            assert "<ID>" in str(exception_info.value), "Name of register of the field is not reported"
            assert "Field" in str(exception_info.value) and "not found" in str(exception_info.value)

        def test_read(self, r_register_version) -> None:
            version = r_register_version.read()
            assert hex(version) == hex(0x123456ab)

        def test_write(self, rw_register) -> None:
            test_value = 0xbeef
            rw_register.raw = test_value
            version = rw_register.raw
            assert hex(version) == hex(test_value)

        def test_write_too_large_value(self, rw_register) -> None:
            test_value = 0x1_dead_beef
            with pytest.raises(Exception) as e_info:
                rw_register.write(test_value)
            assert e_info.type is ValueError
            assert "too large" in str(e_info.value)
            assert "register" in str(e_info.value)
            assert "0x1DEADBEEF" in str(e_info.value)
            assert "32" in str(e_info.value)

        def test_write_part_too_large_value(self, rw_register) -> None:
            test_value = 0xbeef
            with pytest.raises(Exception) as e_info:
                rw_register.write_part(test_value, 2, 4)
            assert e_info.type is ValueError
            assert "too large" in str(e_info.value)
            assert "part" in str(e_info.value)
            assert "0xBEEF" in str(e_info.value)
            assert "3" in str(e_info.value)

        def test_write_part_beyond_register(self, rw_register) -> None:
            test_value = 0x1_dead_beef
            with pytest.raises(Exception) as e_info:
                rw_register.write_part(test_value, 30, 34)
            assert e_info.type is IndexError
            assert "Out-of-bound" in str(e_info.value)
            assert "register" in str(e_info.value)

        def test_write_part_negative_begin(self, rw_register) -> None:
            test_value = 0x1_dead_beef
            with pytest.raises(Exception) as e_info:
                rw_register.write_part(test_value, -2, 4)
            assert e_info.type is IndexError
            assert "negative" in str(e_info.value)

        def test_write_part_too_small_end(self, rw_register) -> None:
            test_value = 0x1_dead_beef
            with pytest.raises(Exception) as e_info:
                rw_register.write_part(test_value, 4, 1)
            assert e_info.type is IndexError
            assert "larger" in str(e_info.value)

        def test_read_part_beyond_register(self, rw_register) -> None:
            with pytest.raises(Exception) as e_info:
                rw_register.read_part(30, 34)
            assert e_info.type is IndexError
            assert "Out-of-bound" in str(e_info.value)
            assert "register" in str(e_info.value)

        def test_read_part_negative_begin(self, rw_register) -> None:
            with pytest.raises(Exception) as e_info:
                rw_register.read_part(-2, 4)
            assert e_info.type is IndexError
            assert "negative" in str(e_info.value)

        def test_read_part_too_small_end(self, rw_register) -> None:
            with pytest.raises(Exception) as e_info:
                rw_register.read_part(4, 1)
            assert e_info.type is IndexError
            assert "larger" in str(e_info.value)

        def test_raw_getter(self, r_register_version) -> None:
            version = r_register_version.raw
            assert hex(version) == hex(0x123456ab)

        def test_raw_setter(self, rw_register) -> None:
            test_value = 0xbeef
            rw_register.raw = test_value
            version = rw_register.read()
            assert hex(version) == hex(test_value)

        def test_value_getter(self, r_register_version) -> None:
            version = r_register_version.value
            assert hex(version) == hex(0x123456ab)

        def test_value_setter(self, rw_register) -> None:
            test_value = 0xbeef
            rw_register.value = test_value
            version = rw_register.read()
            assert hex(version) == hex(test_value)

        @pytest.mark.parametrize('part', [((0, 8), 0xab), ((8, 31), 0x123456)], ids=['MINOR', 'MAJOR'])
        def test_read_part(self, r_register_version, part) -> None:
            begin_bit = part[0][0]
            end_bit = part[0][1]
            major_version = r_register_version.read_part(begin_bit, end_bit)
            assert major_version == part[1]

        @pytest.mark.parametrize('part', (
                ((0, 8), 0xcd, 0x123456cd),
                ((8, 31), 0x654321, 0x65432178)
            ),
            ids=['MINOR', 'MAJOR'])
        def test_write_part(self, rw_register, part) -> None:
            rw_register.write(0x12345678)
            begin_bit = part[0][0]
            end_bit = part[0][1]
            rw_register.write_part(part[1], begin_bit, end_bit)
            assert hex(rw_register.read()) == hex(part[2])

        def test_set(self, rw_register: lowlevhw.DeviceRegister) -> None:
            rw_register.raw = 0xABCD_ED34
            assert rw_register.raw != 0xFFFF_FFFF, "Initial condition not met"
            rw_register.set()
            assert rw_register.raw == 0xFFFF_FFFF

        def test_clear(self, rw_register: lowlevhw.DeviceRegister) -> None:
            rw_register.raw = 0xABCD_ED34
            assert rw_register.raw != 0x0000_0000, "Initial condition not met"
            rw_register.clear()
            assert rw_register.raw == 0x0000_0000

        def test_read_all_fields(self, r_register_version: lowlevhw.DeviceRegister) -> None:
            version = r_register_version.read_all_fields()
            assert len(version) == 3
            assert hex(version['--FULL--']) == '0x123456ab'
            assert hex(version['MINOR']) == hex(0xab)
            assert hex(version['MAJOR']) == hex(0x123456)

        def test_read_all_fields_unsplit_register(self, test_device):
            mem_base = test_device['main_registers']['MEM0_BASE'].read_all_fields()
            assert len(mem_base) == 2
            assert hex(mem_base['--FULL--']) == hex(0x100)

        @pytest.mark.parametrize('test_case', ((0, True), (31, False)), ids=("True", "False"))
        def test_check_bit(self, r_register_version, test_case) -> None:
            bit_position = test_case[0]
            result = test_case[1]
            bit_value = r_register_version.check_bit(bit_position)
            assert bit_value == result

        @pytest.mark.parametrize('test_bit', [0, 1, 4, 7, 31])
        def test_write_bit(self, rw_register: lowlevhw.DeviceRegister, test_bit: int) -> None:
            rw_register.raw = 0
            assert rw_register.check_bit(test_bit) is False, "Initial condition not met"
            rw_register.write_bit(1, test_bit)
            assert rw_register.check_bit(test_bit) is True
            rw_register.write_bit(0, test_bit)
            assert rw_register.check_bit(test_bit) is False

        @pytest.mark.parametrize('test_bit', [0, 1, 4, 7, 31])
        def test_set_bit(self, rw_register: lowlevhw.DeviceRegister, test_bit: int) -> None:
            rw_register.raw = 0
            assert rw_register.check_bit(test_bit) is False, "Initial condition not met"
            rw_register.set_bit(test_bit)
            assert rw_register.check_bit(test_bit) is True

        @pytest.mark.parametrize('test_bit', [0, 1, 4, 7, 31])
        def test_clear_bit(self, rw_register: lowlevhw.DeviceRegister, test_bit: int) -> None:
            rw_register.raw = 0xFFFF_FFFF
            assert rw_register.check_bit(test_bit) is True, "Initial condition not met"
            rw_register.clear_bit(test_bit)
            assert rw_register.check_bit(test_bit) is False

        @pytest.mark.parametrize('test_bit', [0, 1, 4, 7, 31])
        def test_toggle_bit(self, rw_register: lowlevhw.DeviceRegister, test_bit: int) -> None:
            rw_register.raw = 0xFFFF_FFFF
            assert rw_register.check_bit(test_bit) is True, "Initial condition not met"
            rw_register.toggle_bit(test_bit)
            assert rw_register.check_bit(test_bit) is False
            rw_register.toggle_bit(test_bit)
            assert rw_register.check_bit(test_bit) is True


    class TestDeviceField:

        @pytest.fixture(name='rw_field')
        def fixture_rw_field(self, test_device) -> lowlevhw.DeviceField:
            return test_device['main_registers']['TEMP']['MIDDLE']

        def test_read(self, test_device) -> None:
            test_field = test_device['main_registers']['VERSION']['MINOR']
            result = test_field.read()
            assert hex(result) == '0xab'

        def test_value_getter(self, rw_field) -> None:
            test_value = 0x76
            rw_field.write(test_value)
            result = rw_field.value
            assert result == test_value

        def test_write(self, rw_field) -> None:
            test_value = 0xabdf
            rw_field.write(test_value)
            result = rw_field.read()
            assert hex(result) == hex(test_value)

        def test_write_too_large_value(self, rw_field) -> None:
            test_value = 0x1_dead_beef
            with pytest.raises(Exception) as e_info:
                rw_field.write(test_value)
            assert e_info.type is ValueError
            assert "too large" in str(e_info.value)
            assert "field" in str(e_info.value)
            assert "0x1DEADBEEF" in str(e_info.value)
            assert "23" in str(e_info.value)

        def test_write_part_too_large_value(self, rw_field) -> None:
            test_value = 0xbeef
            with pytest.raises(Exception) as e_info:
                rw_field.write_part(test_value, 2, 4)
            assert e_info.type is ValueError
            assert "too large" in str(e_info.value)
            assert "part" in str(e_info.value)
            assert "0xBEEF" in str(e_info.value)
            assert "3" in str(e_info.value)

        def test_write_part_beyond_register(self, rw_field) -> None:
            test_value = 0x1_dead_beef
            with pytest.raises(Exception) as e_info:
                rw_field.write_part(test_value, 30, 34)
            assert e_info.type is IndexError
            assert "Out-of-bound" in str(e_info.value)
            assert "field" in str(e_info.value)

        def test_write_part_negative_begin(self, rw_field) -> None:
            test_value = 0x1_dead_beef
            with pytest.raises(Exception) as e_info:
                rw_field.write_part(test_value, -2, 4)
            assert e_info.type is IndexError
            assert "negative" in str(e_info.value)

        def test_write_part_too_small_end(self, rw_field) -> None:
            test_value = 0x1_dead_beef
            with pytest.raises(Exception) as e_info:
                rw_field.write_part(test_value, 4, 1)
            assert e_info.type is IndexError
            assert "larger" in str(e_info.value)

        def test_read_part_beyond_field(self, rw_field) -> None:
            with pytest.raises(Exception) as e_info:
                rw_field.read_part(30, 34)
            assert e_info.type is IndexError
            assert "Out-of-bound" in str(e_info.value)
            assert "field" in str(e_info.value)

        def test_read_part_negative_begin(self, rw_field) -> None:
            with pytest.raises(Exception) as e_info:
                rw_field.read_part(-2, 4)
            assert e_info.type is IndexError
            assert "negative" in str(e_info.value)

        def test_read_part_too_small_end(self, rw_field) -> None:
            with pytest.raises(Exception) as e_info:
                rw_field.read_part(4, 1)
            assert e_info.type is IndexError
            assert "larger" in str(e_info.value)

        def test_raw_getter(self, test_device) -> None:
            version = test_device['main_registers']['VERSION'].raw
            assert hex(version) == hex(0x123456ab)

        def test_value_setter(self, test_device):
            test_value = 0xabdf
            test_field = test_device['main_registers']['TEMP']['MIDDLE']
            test_field.value = test_value
            result = test_field.read()
            assert hex(result) == hex(test_value)

        @pytest.mark.parametrize('test_case', [((0, 3), 0xb), ((4, 7), 0xa)], ids=['MINOR_LOW', 'MINOR_HIGH'])
        def test_read_part(self, test_device, test_case):
            begin_bit = test_case[0][0]
            end_bit = test_case[0][1]
            major_version = test_device['main_registers']['VERSION']['MINOR'].read_part(begin_bit, end_bit)
            assert major_version == test_case[1]

        @pytest.mark.parametrize('test_case', (
                ((0, 3), 0xc, 0xac),
                ((4, 7), 0xd, 0xdb)
            ),
            ids=['LS_NIBBLE', 'NIBBLE_1'])
        def test_write_part(self, rw_field: lowlevhw.DeviceField, test_case) -> None:
            begin_bit = test_case[0][0]
            end_bit = test_case[0][1]
            write_value = test_case[1]
            result = test_case[2]
            rw_field.write(0xab)
            rw_field.write_part(write_value, begin_bit, end_bit)
            assert hex(rw_field.read()) == hex(result)

        @pytest.mark.parametrize('test_case', ((7, True), (6, False)), ids=('True', 'False'))
        def test_check_bit(self, test_device: lowlevhw.Device, test_case) -> None:
            bit_position: int = test_case[0]
            expected: bool = test_case[1]
            bit_value = test_device['main_registers']['VERSION']['MINOR'].check_bit(bit_position)
            assert bit_value == expected

        @pytest.mark.parametrize('test_bit', [0, 1, 4, 7])
        def test_write_bit(self, rw_field: lowlevhw.DeviceField, test_bit: int) -> None:
            rw_field.raw = 0
            assert rw_field.check_bit(test_bit) is False, "Initial condition not met"
            rw_field.write_bit(1, test_bit)
            assert rw_field.check_bit(test_bit) is True
            rw_field.write_bit(0, test_bit)
            assert rw_field.check_bit(test_bit) is False

        @pytest.mark.parametrize('test_bit', [0, 1, 4, 7])
        def test_set_bit(self, rw_field: lowlevhw.DeviceField, test_bit: int) -> None:
            rw_field.raw = 0
            assert rw_field.check_bit(test_bit) is False, "Initial condition not met"
            rw_field.set_bit(test_bit)
            assert rw_field.check_bit(test_bit) is True

        @pytest.mark.parametrize('test_bit', [0, 1, 4, 7])
        def test_clear_bit(self, rw_field: lowlevhw.DeviceField, test_bit: int) -> None:
            rw_field.raw = 0xFF
            assert rw_field.check_bit(test_bit) is True, "Initial condition not met"
            rw_field.clear_bit(test_bit)
            assert rw_field.check_bit(test_bit) is False

        @pytest.mark.parametrize('test_bit', [0, 1, 4, 7])
        def test_toggle_bit(self, rw_field: lowlevhw.DeviceField, test_bit: int) -> None:
            rw_field.raw = 0xFF
            assert rw_field.check_bit(test_bit) is True, "Initial condition not met"
            rw_field.toggle_bit(test_bit)
            assert rw_field.check_bit(test_bit) is False
            rw_field.toggle_bit(test_bit)
            assert rw_field.check_bit(test_bit) is True


    class TestDeviceMemoryMap:

        @pytest.mark.parametrize('area_name', ['MEM0_AREA', 'mem0_area', 'Mem0_Area'])
        def test_mem_area_access(self, test_device: lowlevhw.Device, area_name: str) -> None:
            test_area = test_device['main_memory'][area_name]
            assert isinstance(test_area, lowlevhw.DeviceMemoryArea)
            assert test_area.definition.name == 'MEM0_AREA'

        def test_read_by_tuple_access(self, test_device, memory_data):
            memory_data = test_device['main_memory'][(0x100, 5)].read()
            assert memory_data == memory_data

        def test_read_single_byte_access(self, test_device, memory_data):
            read_data = test_device['main_memory'][0x102].read()
            assert read_data == [memory_data[2]]


    class TestDeviceMemoryArea:

        def test_get_base_address(self, test_device):
            base_address = test_device['main_memory']['MEM0_AREA'].get_base_address()
            assert hex(base_address) == hex(0x2100)

        def test_get_size(self, test_device):
            assert test_device['main_memory']['MEM0_AREA'].get_size() == 5

        def test_set_base_address(self, test_device):
            test_device['main_memory']['MEM0_AREA'].set_base_address(0x200)
            assert test_device['main_registers']['MEM0_BASE'].read() == 0x200

        def test_read_name(self, test_device, memory_data):
            memory_data = test_device['main_memory']['MEM0_AREA'].read()
            assert memory_data == memory_data

        def test_write(self, test_device):
            test_data = range(10, 16)
            test_data = list(test_data[1:6]) + ([0]*5)
            test_device['main_memory']['MEM0_AREA'].write(test_data[0:5])
            memory_data = test_device['main_memory'][(0x100, 10)].read()
            assert memory_data == test_data

    def test_test_register_after_reset(self, test_device_reset):
        result = test_device_reset.test_registers_after_reset()
        assert result == True

    def test_test_register_write(self, test_device_reset):
        result = test_device_reset.test_register_write()
        assert result == True


class TestLegacyUsage:

    @pytest.fixture
    def test_memory_map(self) -> lowlevhw.MemoryMap:
        main_memory = lowlevhw.MemoryMap('main_memory')
        main_memory.add_area(lowlevhw.MemoryAreaDynBaseDynSize(
                'MEM0_AREA', 'MEM0_BASE', 'MEM0_SIZE', lowlevhw.RawMemory()
            ))
        main_memory.add_area(lowlevhw.MemoryAreaFixed('FIXED_MEM_AREA', 0x10000, 0x1000))
        return main_memory

    @pytest.fixture
    def test_device(self, simple_register_bank, test_memory_map):
        test_dev_defintion = lowlevhw.DeviceDefinition()
        test_dev_defintion.add_register_bank(simple_register_bank)
        test_dev_defintion.add_memory_map(test_memory_map)
        test_device = lowlevhw.Device(test_dev_defintion, lowlevhw.driver.Dummy(test_dev_defintion))
        test_device.connect('DUMMY')
        test_device['MEM0_BASE'].write(0x100)
        test_device['MEM0_SIZE'].write(5)
        yield test_device
        test_device.disconnect()

    @pytest.fixture
    def memory_data(self, test_device):
        test_data = range(6)
        test_data = list(test_data[1:6])
        test_data_extended = list(test_data) + ([0]*5)
        test_device.driver.write_ram(0x100, 5, test_data_extended)
        return test_data


    class TestDeviceClass:

        def test_register_name_access(self, test_device):
            version_register = test_device['VERSION']
            assert isinstance(version_register, lowlevhw.DeviceRegister)
            assert version_register.definition.name == 'VERSION'

        @pytest.mark.parametrize('registers', (
                (0x00, 'ID', 0x00),
                (0x01, 'MEM0_BASE', 0x100),
                (0x10, 'VERSION', 0x123456AB)
            ),
            ids=('0x00', '0x01', '0x10'))
        def test_register_address_access(self, test_device, registers):
            (register_address, register_name, expected_value) = registers
            test_register = test_device[register_address]
            assert isinstance(test_register, lowlevhw.DeviceRegister)
            # assert test_register.definition.name == register_name             # currently the register is not looked-up but a temporary register is created
            assert test_register.read() == expected_value

        def test_field_access_by_colon_string(self, test_device):
            minor_field = test_device['VERSION:MINOR']
            assert isinstance(minor_field, lowlevhw.DeviceField)
            assert minor_field.definition.name == 'MINOR'

        def test_memory_name_access(self, test_device):
            memory_area = test_device['FIXED_MEM_AREA']
            assert isinstance(memory_area, lowlevhw.DeviceMemoryArea)
            assert memory_area.definition.name == 'FIXED_MEM_AREA'

        def test_memory_tuple_access(self, test_device):
            memory_area = test_device[(0x0000, 10)]
            assert isinstance(memory_area, lowlevhw.DeviceMemoryArea)
            assert memory_area.definition.name == '____temp____'

        def test_register_acess_fail(self, test_device):
            with pytest.raises(KeyError) as exception_info:
                test_device['NOT_EXISTING_REGISTER'].read()
            assert 'NOT_EXISTING_REGISTER' in str(exception_info.value), "Name of missing register is not reported"
            assert "Element <" in str(exception_info.value)


    class TestDeviceRegister:

        def test_read(self, test_device):
            version = test_device['VERSION'].read()
            assert hex(version) == '0x123456ab'

        def test_value_getter(self, test_device):
            version = test_device['VERSION'].value
            assert hex(version) == '0x123456ab'

        def test_write(self, test_device):
            test_value = 0xBEEF
            test_device['TEMP'].write(test_value)
            version = test_device['TEMP'].read()
            assert hex(version) == hex(test_value)

        def test_value_setter(self, test_device):
            test_value = 0xBEEF
            test_device['TEMP'].value = test_value
            version = test_device['TEMP'].read()
            assert hex(version) == hex(test_value)

        @pytest.mark.parametrize('part', [((0, 8), 0xAB), ((8, 31), 0x123456)], ids=['MINOR', 'MAJOR'])
        def test_read_part(self, test_device, part):
            begin_bit = part[0][0]
            end_bit = part[0][1]
            major_version = test_device['VERSION'].read_part(begin_bit, end_bit)
            assert major_version == part[1]

        def test_read_all_fields(self, test_device):
            version = test_device['VERSION'].read_all_fields()
            assert len(version) == 3
            assert hex(version['--FULL--']) == '0x123456ab'
            assert hex(version['MINOR']) == '0xab'
            assert hex(version['MAJOR']) == '0x123456'

        def test_read_all_fields_unsplit_register(self, test_device):
            mem_base = test_device['MEM0_BASE'].read_all_fields()
            assert len(mem_base) == 2
            assert hex(mem_base['--FULL--']) == '0x100'

        def test_field_acess_fail(self, test_device):
            with pytest.raises(KeyError) as exception_info:
                test_device['ID']['NOT_EXISTING_FIELD'].read()
            assert "<NOT_EXISTING_FIELD>" in str(exception_info.value), "Name of missing field is not reported"
            assert "<ID>" in str(exception_info.value), "Name of register is not reported"
            assert "Field" in str(exception_info.value) and "not found" in str(exception_info.value)


    class TestDeviceField:

        def test_value_getter(self, test_device):
            test_value = 0x76
            test_device['TEMP:MIDDLE'].write(test_value)
            result = test_device['TEMP:MIDDLE'].value
            assert result == test_value

        def test_value_setter(self, test_device):
            test_value = 0xabdf
            test_device['TEMP:MIDDLE'].value = test_value
            result = test_device['TEMP:MIDDLE'].read()
            assert hex(result) == hex(test_value)


    class TestDeviceMemoryArea:

        def test_get_base_address(self, test_device):
            assert test_device['MEM0_AREA'].get_base_address() == 0x100

        def test_get_size(self, test_device):
            assert test_device['MEM0_AREA'].get_size() == 5

        def test_set_base_address(self, test_device):
            test_device['MEM0_AREA'].set_base_address(0x200)
            assert test_device['MEM0_BASE'].read() == 0x200

        def test_read(self, test_device, memory_data):
            memory_data = test_device['MEM0_AREA'].read()
            assert memory_data == memory_data

        def test_write(self, test_device):
            test_data = range(6)
            test_data = list(test_data[1:6]) + ([0]*5)
            test_device['MEM0_AREA'].write(test_data[0:5])
            memory_data = test_device[(0x100, 10)].read()
            assert memory_data == test_data
