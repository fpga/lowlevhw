# -----------------------------------------------------------------------------
#  File        : test_console_print.py
#  Brief       :
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz
#  Date        : 2023-11-10
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2023 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import lowlevhw


test_bank = lowlevhw.RegisterBank('test_bank', 32, 32)
test_bank.add_register('simple_register',    0x0, 'B', description="A first register")
complex_reg = test_bank.add_register('complex_register', 0x100, 'R', description="A register with fields")
complex_reg.add_field('first_field', 0, 1, description="A field")
complex_reg.add_field('second_field', 13, 13, description="An unlucky field")
test_bank.add_register('another_register', 0x200, description="Yet another register")

test_space = lowlevhw.AddressSpace('test_space')
test_space.add_element(test_bank, 0x1000, 'test_space_1')
test_space.add_element(test_bank, 0x2000, 'test_space_2')

test_device = lowlevhw.DeviceDefinition()
reg_space = lowlevhw.AddressSpace('registers')
reg_space.add_element(test_space, 0x10000)
reg_space.add_element(test_bank, 0x1000)
reg_space.add_element(test_bank, 0x20000, 'test_bank_2')
test_device.add_address_space(reg_space, transparent=True)


def test_print(title, object) -> None:
    separator = '—' * 120
    separator = separator[0:79] + '|' + separator[81:]
    title_line = separator
    title_line = separator[0:3] + ' ' + title
    print(separator)
    print(title_line)
    print(separator)
    print(f"{object}")
    print(separator)
    print()


def test_register_print() -> None:
    test_register = lowlevhw.Register('test_register', 32, 'BW', reset_value=0x1234_4567,
                                   description="This is a test register")
    test_print("Register", test_register)

    test_register_with_fields = lowlevhw.Register('test_register', 32, 'BW', reset_value=0x1234_4567,
                                   description="This is a test register")
    test_register_with_fields.add_field('first_field', 0, 8)
    test_register_with_fields.add_field('second_field', 10, 4)
    test_print("Register with fields", test_register_with_fields)

    test_register_with_many_fields = lowlevhw.Register('test_register', 32, 'BW', reset_value=0x1234_4567,
                                   description="This is a test register")
    for i in range(32):
        test_register_with_many_fields.add_field(f'field_{i}', i, 1)
    test_print("Register with many fields", test_register_with_many_fields)

    test_register_linked_field = lowlevhw.Register('test_register', 32, 'BW', reset_value=0x1234_4567,
                                   description="This is a test register")
    first_field = test_register_linked_field.add_field('first_field', 0, 8)
    test_register_linked_field.add_linked_field('second_field', first_field, 20, 'R')
    test_print("Register with linked field", test_register_linked_field)

    all_fields = test_register_with_fields.get_fields_incl_reserved()
    field_string = ""
    for field in all_fields:
        field_string += str(field) + '\n'
    field_string = field_string[:-2]
    test_print("Fields including reserved", field_string)


def test_register_bank_print() -> None:
    test_print("Register bank", test_bank)


def test_address_space_print() -> None:
    test_print("Address space", test_space)

    empty_space = lowlevhw.AddressSpace('empty_space')
    test_print("Empty address space", empty_space)


def test_device_print() -> None:
    test_print("Device", test_device)


def main() -> None:
    print()
    test_register_print()
    test_register_bank_print()
    test_address_space_print()
    test_device_print()


if __name__ == '__main__':
    main()
