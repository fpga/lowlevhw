# -----------------------------------------------------------------------------
#  File        : address_mode_test_bank.py
#  Brief       : Byte addressed bank for testing
# -----------------------------------------------------------------------------
#  Authors     : Christian Amstutz, Kaj Rosengren
#  Date        : 2022-09-20
# -----------------------------------------------------------------------------
#  Description :
# -----------------------------------------------------------------------------
#  Copyright (C) 2018 - 2021 European Spallation Source ERIC
# -----------------------------------------------------------------------------

import lowlevhw


address_width  = 20
register_width = 32

bank = lowlevhw.RegisterBank('address_mode_bank', address_width, register_width, element_addressing=False, error_reporting=True)
bank.base_address = 0x20000

# Basic read/write register with reset value
bank.add_register('register_1', address=0x0, reset_value=0x1122_3344, modes='RW', description="This is register_1. And this text is a test of how the formatting works.")

# Basic read/write register without reset value (default reset value)
bank.add_register('register_2', 0x4, modes='RW')

# Add read-only register at lower address
bank.add_register('system_id', 0x8, modes='R', description="System ID, this identifies the system.\n This text tries multiline comments\n The formatting is a bit annoying in python" )

# Add fixed-value register
bank.add_register('fw_id', 0xC, modes='B', reset_value=0xBEEF)

# register 10
bank.add_register('register_10', 0x010, modes='RW')
bank['register_10'].add_field('a_factor', 0, 8, description="A factor is the factor for the A\n. Its not the same factor as the B factor.")
bank['register_10'].add_field('b_factor', 16, 8, reset_value=0x44)
bank['register_10'].add_field('en', 30, 1)
bank['register_10'].reset_value = 0x0044_0000

bank.add_register('adc_value', 0x014)

# register 30
bank.add_register('register_30', 0x030)
bank['register_30'].add_field('reg30_a_factor', 0, 8)
bank['register_30'].add_field('reg30_b_factor', 16, 8, reset_value=0x44)
bank['register_30'].reset_value = 0x0044_0000

# param x
bank.add_register('param_x', 0x040, reset_value=0x0FFF_FFFF, modes='RW')
bank.add_register('param_x_set', 0x044, 'S')
bank['param_x_set'].add_linked_field('param_x_set', bank['param_x']['param_x'], 0)
bank.add_register('param_x_clr', 0x048, modes='CB')
bank['param_x_clr'].link_to_register(bank['param_x'], 'clr')

# strobe reg
bank.add_register('strobe_reg', 0x050, reset_value=0x0000_0000, modes='T')

# shadow registers
bank.add_register('shadow_1', address=0x60, modes='BW')
bank.add_register('shadow_2', address=0x64, modes='RW')
bank['shadow_2'].add_field('low_byte', 0, 8)
shdw_grp1 = bank.add_shadow_group('shadow_group_1')
shdw_grp1.add_element(bank['shadow_1'])
shdw_grp1.add_element(bank['shadow_2'])

# Standard BW register for write strobe tests
bank.add_register('strb_wide', 0x070, reset_value=0x0000_DEAD, modes='BW')

bank.add_register('strb_fields', 0x074, modes='BW')
bank['strb_fields'].add_field('en_0', 0, 1)
bank['strb_fields'].add_field('en_1', 1, 1)
bank['strb_fields'].add_field('en_2', 2, 1)
bank['strb_fields'].add_field('en_3', 3, 1)
bank['strb_fields'].add_field('wide4_0', 4, 4)
bank['strb_fields'].add_field('wide8_0', 8, 8)
bank['strb_fields'].add_field('wide4_1', 16, 4)
bank['strb_fields'].add_field('wide4_2', 20, 4)
bank['strb_fields'].add_field('wide8_1', 24, 8)

bank.add_register('CAPITAL_REG', 0x078, modes='BW', description="This register has capital letters in the definition")
bank['CAPITAL_REG'].add_field('ODD_SIZE', 0, 11, description="This register has size 11")
bank['CAPITAL_REG'].add_field('WEIRD_SIZE', 11, 7, description="This register has size 5")
bank['CAPITAL_REG'].add_field('FUNKY_SIZE', 18, 3, description="This register has size 3")

print()
print(str(bank))
print()


output_folder = '../../output/test_address_mode'

generator = lowlevhw.generators.AXIRegbankVHDLRecordGen(bank, output_folder)
generator.generate()

bank.name='address_mode_bank_slv'
generator = lowlevhw.generators.AXIRegbankVHDLslvGen(bank, output_folder)
generator.generate()

generator = lowlevhw.generators.RegbankHtmlDocGen(bank, output_folder)
generator.generate()

generator = lowlevhw.generators.RegbankMDDocGen(bank, output_folder)
generator.generate()
