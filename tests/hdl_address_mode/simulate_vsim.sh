#-------------------------------------------------------------------------------
# Project     : Automated Vivado Workflow
#-------------------------------------------------------------------------------
# File        : simulate_vsim.sh
# Authors     : Kaj Rosengren
# Created     : 2020-10-26
# Platform    : Questasim 10.7c
# Standard    :
#-------------------------------------------------------------------------------
# Description : Copy of simulate_xsim.sh for questasim instead
# Problems    :
#-------------------------------------------------------------------------------
# Copyright (c) 2020 - 2023 European Spallation Source ERIC
#-------------------------------------------------------------------------------

TB_DIR="."
TB_NAME="address_mode_bank_tb"
TOP_DESIGN="testbench"

GUI_OPT="-do vsim_gui.tcl"

# Remove previous simulation data
rm -r questa_lib
rm transcript
rm modelsim.ini
rm -r ../../output/test_address_mode

python3 address_mode_test_bank.py

# Sim lib from environment variable
if [ -z ${VSIM_LIB_DIR+x} ]
then
    echo "No xilinx VSIM_LIB_DIR found. Exiting"
    exit
else
    echo "Copying library from :" ${VSIM_LIB_DIR}
    cp ${VSIM_LIB_DIR}/modelsim.ini .
fi

# Run analysis for VHDL files if existing
if [ -e tb_vhdl.prj ]
then
    vsim -batch -do "do vsim_compile.tcl tb_vhdl.prj"
fi

# Elaborate design
XELAB_OPT=""
if [ -e xelab_opt.ini ]
then
    XELAB_OPT="-f xelab_opt.ini"
fi

SNAPSHOTS="${TB_NAME}_sim"
if [ -e tb_verilog.prj ]
then
    SNAPSHOTS="${SNAPSHOTS} xil_defaultlib.glbl"
fi
vopt +acc -suppress 8602 -work xil_defaultlib xil_defaultlib.$TOP_DESIGN $XELAB_OPT -o ${SNAPSHOTS}

echo $GUI_OPT
# Run simulation
vsim -work xil_defaultlib $GUI_OPT ${TB_NAME}_sim
