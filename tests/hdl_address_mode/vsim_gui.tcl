#-------------------------------------------------------------------------------
# Project     : ESS LLRF FPGA
#-------------------------------------------------------------------------------
# File        : xsim_gui.tcl
# Authors     : Christian Amstutz
# Created     : 2020-10-09
# Platform    : Xilinx Vivado 2020.1
# Standard    :
#-------------------------------------------------------------------------------
# Description : Questa simulation script for regisster generator test bench.
# Problems    :
#-------------------------------------------------------------------------------
# Copyright (c) 2021 European Spallation Source ERIC
#-------------------------------------------------------------------------------

run -all

exit
