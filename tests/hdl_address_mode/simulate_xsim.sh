#-------------------------------------------------------------------------------
# Project     : Automated Vivado Workflow
#-------------------------------------------------------------------------------
# File        : run_tb.sh
# Authors     : Christian Amstutz
# Created     : 2017-06-08
# Platform    : Xilinx Vivado 2017.1
# Standard    :
#-------------------------------------------------------------------------------
# Description : Executes the test bench in the Vivado simulator.
#-------------------------------------------------------------------------------
# Copyright (c) 2018 - 2023  European Spallation Source ERIC
#-------------------------------------------------------------------------------

TB_DIR="."
TB_NAME="address_mode_bank_tb"
TOP_DESIGN="testbench"
SIM_SCRIPT="${TB_DIR}/xsim_gui.tcl"
OUTPUT_DIR="../../output/test_address_mode"
WAV_OUTPUT_DIR="${OUTPUT_DIR}/wavedb"

# Remove previous simulation data
rm xsim.dir -r
rm ${OUTPUT_DIR} -r

python3 address_mode_test_bank.py

# if [ "$3" == "gui" ]
# then
#   # Run simualtion with GUI and script setting up the GUI
  GUI_OPT="-gui -tclbatch $SIM_SCRIPT"
# else
  # Run simulation without GUI and quit after execution
  # GUI_OPT="-R"
# fi

# Command line options
xvlog_opts="-m64 --relax"
xvhdl_opts="-m64 --relax"

# Run analysis for Verilog files if existing
if [ -e ${TB_DIR}/tb_verilog.prj ]
then
    xvlog $xvlog_opts -prj ${TB_DIR}/tb_verilog.prj
fi

# Run analysis for VHDL files if existing
if [ -e ${TB_DIR}/tb_vhdl.prj ]
then
    xvhdl $xvlog_opts -prj ${TB_DIR}/tb_vhdl.prj
fi

# Elaborate design
XELAB_OPT=""
if [ -e ${TB_DIR}/xelab_opt.ini ]
then
    XELAB_OPT="-f ${TB_DIR}/xelab_opt.ini"
fi

XSIM_INI=""
if [ -e ${TB_DIR}/xsim.ini ]
then
    XSIM_INI="-initfile ${TB_DIR}/xsim.ini"
fi

SNAPSHOTS="${TB_NAME}_sim"
if [ -e ${TB_DIR}/tb_verilog.prj ]
then
    SNAPSHOTS="${SNAPSHOTS} xil_defaultlib.glbl"
fi
xelab xil_defaultlib.$TOP_DESIGN -s ${SNAPSHOTS} -debug typical --relax --mt auto -L work -L unisims_ver -L unimacro_ver -L xilinxcorelib_ver -L secureip -L SIMPRIM_VER $XELAB_OPT $XSIM_INI

# Run simulation
mkdir ${WAV_OUTPUT_DIR} -p
xsim ${TB_NAME}_sim -wdb ${WAV_OUTPUT_DIR}/${TB_NAME}.wdb $GUI_OPT
