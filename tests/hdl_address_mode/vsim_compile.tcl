#-------------------------------------------------------------------------------
# Project     : ESS Framework
#-------------------------------------------------------------------------------
# File        : vsim_compile.tcl
# Authors     : Kaj Rosengren
# Created     : 2020-10-23
# Platform    : Questasim 10.7c
# Standard    :
#-------------------------------------------------------------------------------
# Description : This script will parse a given .prj file and compile 
#               the content. Note the vhdl files needs to be in order as 
#               questasim doesn't find dependencies automatically.
#
# Problems    :
#-------------------------------------------------------------------------------
# Copyright (c) 2020 European Spallation Source ERIC
#-------------------------------------------------------------------------------

# TODO Build path, now the script must be executed from the testbench directory

set filename $1
puts "The filename is \"$filename\""
# Check filename?

set fp [open $filename]

set file_data [read $fp]
set data [split $file_data "\n"]

set libs {}

# Director to collect the libraries (easier to clean)
file mkdir questa_lib

foreach line $data {
    ## Split lines, and assign the three rows to separate variables
    set fields [regexp -all -inline {\S+} $line]
    lassign $fields tag lib file_path

    ## Check if we have mapped this library, if not map it
    if {$tag in [list "vhdl" "vhdl2008" "verilog" "sv"]} {
        if {[lsearch -exact $libs $lib] == -1} {
            lappend libs $lib
            #puts "Library mapping : $lib"
            #puts "Libraries mapped : $libs"
            vlib questa_lib/$lib
            vmap $lib questa_lib/$lib
        }
    }

    # Compile according to the tag
    if {$tag == "vhdl"} {
        #puts "VHDL file $lib : $file_path"
        vcom -64 -93 -work $lib [string trim $file_path "\""]
    }
    if {$tag == "vhdl2008"} {
        #puts "VHDL 2008 file $lib : $file"
        vcom -64 -2008 -work $lib [string trim $file_path "\""]
    }
    if {$tag == "verilog"} {
        vlog -64 -work $lib [string trim $file_path "\""]
    }
    if {$tag == "sv"} {
        vlog -64 -sv -work $lib [string trim $file_path "\""]
    }
}

close $fp
exit
