from setuptools import setup

def readme() -> str:
    with open('README.md') as f:
        return f.read()

setup(name='lowlevhw',
      version='0.0.1',
      description="Library for low-level hardware access, e.g. for FPGA design.",
      long_description="see README.md",
      url="https://bitbucket.org/europeanspallationsource/lowlevhw",
      classifiers=[
        "Development Status :: 3 - Alpha",
        # "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.6",
        "Natural Language :: English",
        "Topic :: Scientific/Engineering :: Electronic Design Automation (EDA)",
      ],
      keywords="HDL FPGA hardware testing registers VHDL",
      author="Christian Amstutz",
      author_email="christian.amstutz@ess.eu",
      # license="MIT",
      package_dir={'': 'srcs'},
      packages=[
        'lowlevhw',
        'lowlevhw.driver',
        'lowlevhw.generators',
        'lowlevhw.helpers',
      ],
      include_package_data=True,
      install_requires=[
        'pytest',
        'scipy',
        'wavedrom; python_version>="3.7"',
        'cocotb',
      ],
      zip_safe=False)
