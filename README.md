# LowLevHW - Low-level hardware helpers for Python #

This package provides low-level access to hardware. This includes access to
registers and memory. This allows developers to use Python as the language for
software prototyping and testing of hardware through low-level access. It also
contains also a register bank generator which automatically generates register
banks in VHDL and documentation.

Planned:
  - C header file generation
  - Register description file format

## Installation and Development ##

The module can be installed locally by

    python3 -m pip install --user --editable .

This results that the module can be loaded with

    import lowlevhw

into Python files. Additionally, the source can be edited in the folder from
which LowLevHW has been installed and changes have an immediate effect on the
execution on all projects using the  module.

To run the tests of the module

    pytest


## Contact ##

Christian Amstutz (christian.amstutz@ess.eu)
