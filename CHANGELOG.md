# Changelog

## [unpublished]

 - sub-module helpers has been introduced where all functions used for the
   processing of digital data are are collected.
 - The device specification has been extracted from the Device class and is now
   handled by the DeviceDefinition class. The Device class is only responsible
   for the acccess of devices.
 - Device also needs a DeviceDefinition object as a first parameter.
 - renamed toggle() to toggle_bit() in DeviceRegister class
 - renamed is_multiple_fields() method of Register class to has_fields()
 - output_folder of generators is provided to the constructors instead of the
   generate() function
